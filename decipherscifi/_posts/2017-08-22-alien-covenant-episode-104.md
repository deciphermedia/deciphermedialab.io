---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2017-08-22 06:44:33+00:00
description: 'Alien Covenant science. Space settlement crew selection, population bombs, swole jesus, neutrino detection & instrument miniaturization, xenomorph evolution'
layout: podcast_post
permalink: /decipherscifi/alien-covenant-episode-104
redirect_from:
  - /decipherscifi/104
slug: alien-covenant-episode-104
title: 'Alien Covenant: neutrinos, weakly interacting massive particles, and swole Jesus aliens'
tags:
  - evolution
  - jesus
  - neutrinos
  - planet seeding
  - population bombs
  - science
  - science fiction
  - space settlement

# Episode particulars
image:
  path: assets/imgs/media/movies/alien_covenant_2017.jpg
  alt: Salivating Xenomorph Alien Covenant movie backdrop
links:
  - text: 'What is a Neutrino? by Minute Physics'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=lAAmAbJvvJg'
  - text: 'Quantum SHAPE-SHIFTING: Neutrino Oscillations by Minute Physics'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=7fgKBJDMO54'
  - text: 'Alien: Covenant | Prologue: Last Supper by 20th Century FOX'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=EkXgRlRao5I'
  - text: 'Alien: Covenant | Prologue: The Crossing | 20th Century FOX'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=XeMVrnYNwus'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/alien-covenant/id1231319469?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Alien-Covenant-Michael-Fassbender/dp/B071NLN3WB'
  title: Alien Covenant
  type: movie
  year: 2017
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/alien_covenant_2017.jpg
  number: 
  guid: 'https://decipherscifi.com/?p=3036'
  media:
    audio:
      audio/mpeg:
        content_length: 32843776
        duration: '39:05'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Alien_Covenant_--_--_decipherSciFi.mp3'
---
#### Crew Selection

Human pairing in space crews. Sex selection for different purposes, and in particular _settlement_. The [Voices from L5 episode on Gender in Space](http://thisorbitallife.com/podcast/voices-l5-gender-space/). If humans still need to gestate the old-fashioned way, you're gonna need a lot of human wombs.

#### Colonizing

The numbers game of space settlement. The possibility of gestating the next human generation on the target planet from frozen embryo: i.e. population bomb.

#### Neutrinos

_Weakly_ interacting. How to detect a "neutrino burst" in space and not be vaporized by a supernova. Tools to grant perspective on the topic.  Neutrino detectors large and small. "[World's Smallest neutrino detector.](https://news.uchicago.edu/story/worlds-smallest-neutrino-detector-observes-elusive-interactions-particles)"

{% responsive_image path: assets/imgs/misc/japanese_neutrino_detector.jpg alt: "Japanese neutrino detector model" caption: "A model of the Japanese neutrino detector" attr_url: "https://commons.wikimedia.org/wiki/File:Kamiokande89.JPG" attr_text: "日:Jnn" %}

#### Swole Jesus

More [details from Ridley Scott](http://www.indiewire.com/2012/06/did-ridley-scott-just-ruin-the-mystery-of-prometheus-kill-its-sequel-109273/) about the connection between Jesus and the engineers. The suggestion made in Prometheus made more explicit. [Swole Korean Jesus](https://www.google.com/search?q=swole+korean+jesus&tbm=isch&source=lnt&tbs=isz:l&sa=X&ved=0ahUKEwj0gICA4ZXkAhXsm-AKHd4FBmEQpwUIIg&biw=1592&bih=730&dpr=2). [Swole white Jesus](https://www.reddit.com/r/swoleacceptance/comments/1blzfv/swole_jesus_for_those_who_worship_in_the_iron/).

#### Alien Evolution

Parasitoid developments. Design vs natural selection. Indiscriminate resource consumption.

#### How to break out of a chest

[These guys](https://theconversation.com/from-breaking-glass-to-chest-bursting-the-scientists-review-of-alien-covenant-77150) made some assumptions and ran with them. Mouth-mouth saw-saws and their use in chestbursting.

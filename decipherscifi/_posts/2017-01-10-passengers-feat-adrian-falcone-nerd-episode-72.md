---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2017-01-10 07:35:54+00:00 
description: 'Adrian Falcone is back again, for Passengers! Making sense of time and distance. Gravity, fake or otherwise, and the ceasing thereof. Ethics (a little).' 
layout: podcast_post
permalink: /decipherscifi/passengers-feat-adrian-falcone-nerd-episode-72
redirect_from:
  - /decipherscifi/72
slug: passengers-feat-adrian-falcone-nerd-episode-72 
title: 'Passengers: helical ship designs, the real Arcturus star, and ethics  w/ Adrian Falcone' 
tags:
  - science fiction
  - science
  - gravity
  - love
  - space stations
  - artificial gravity
  - space settlement

# Episode particulars
image:
  path: assets/imgs/media/movies/passengers_2016.jpg
  alt: Chir sPratt and Jennifer Lawrence gazing through a slit Passengers movie backdrop
links:
  - text: Seveneves
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/seveneves/id901493995?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Seveneves-Novel-Neal-Stephenson-ebook/dp/B00LZWV8JO'
  - text: '"How did the Avalon (seemingly) get to Arcturus so quickly if they are only traveling at .5 of lightspeed?"'
    urls:
      - text: Scifi & Fantasy Stack Exchange
        url: 'http://scifi.stackexchange.com/q/148855/60780'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/passengers/id1179581806?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Passengers-Jennifer-Lawrence/dp/B01N2TC6OJ'
  title: Passengers
  type: movie
  year: 2017
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: adrian_falcone
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/passengers_2016.jpg
  number: 72
  guid: 'http://www.decipherscifi.com/?p=1262'
  media:
    audio:
      audio/mpeg:
        content_length: 35187778
        duration: '48:52'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Passengers_--_Adrian_Falcone_--_decipherSciFi.mp3'
---
#### Ship design

Segmented helix. Simulated/artificial gravity. Fusion reactors and constant acceleration. 0.5c after 30 years.

#### Arcturus

A red dwarf, 37 lightyears away from earth. Could the ship have made is to that point with the time and speed given? ([no](http://scifi.stackexchange.com/q/148855/60780)) This is the meat right here. Meaningless slingshots. Red Dwarves. Physics!

#### Stasis

Generation ships vs stasis pods. Stasis as a resource-saving measure. IRL research in "stasis" and muscle-mass conservation. Acceptable levels of brain deterioration. "Minor" brain damage.

#### Anti-collision measures

Kinda like magic. Atomizing rocks in space at 0.5c.

#### Gravity and the ceasing thereof

How simulated gravity works on a giant spinning torus (or, in this case, a segmented helix). How it ceases, and the physics necessary in order to create the effect seen in the film. Swimming out of floating water.

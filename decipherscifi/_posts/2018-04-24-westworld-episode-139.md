---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2018-04-24 07:45:52+00:00
description: 'Westworld science! Robots! The "wild west" and how long the period lasted. Comparisons with Disney parks. Blaming the Lighthill for the "AI winter" in the 1970s. Comparing the movie and the HBO series. Modular neural net functionality versus artifical intelligence. Robotics. Computer viruses. More! 🤖'
layout: podcast_post
permalink: /decipherscifi/westworld-episode-139
redirect_from:
  - /decipherscifi/139
slug: westworld-episode-139
title: 'Westworld: extreme theme parks, first computer virus, and sensory processing'
tags:
  - ai winter
  - android
  - artificial intelligence
  - computer viruses
  - disney
  - machine consciousness
  - robotics
  - science
  - science fiction
  - sensory processing
  - wild west

# Episode particulars
image:
  path: assets/imgs/media/movies/westworld_1978.jpg
  alt: Robot Yule Brenner and computer vision of some dudes
links:
  - text: The Lighthill Debate on Artificial Intelligence [1973]
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=03p2CADwGF8'
  - text: Westworld (the series)
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/tv-season/westworld-season-1/id1182074045?mt=4&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/The-Original/dp/B01N05UD06'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/westworld/id294015994?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Westworld-Yul-Brynner/dp/B000LJ9ZWE'
  title: Westworld
  type: movie
  year: 1978
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/westworld_1978.jpg
  number: 139
  guid: 'https://decipherscifi.com/?p=6180'
  media:
    audio:
      audio/mpeg:
        content_length: 35595694
        duration: '42:21'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Westworld_1978_--_--_Decipher_SciFi.mp3'
---
#### Disney

Westworld is some Disneyworld-level theme park logistics. Star Wars Galaxy's Edge. Westworld ticket prices.

#### The Wild West

The mythos. Cowboys shootin' and rootin- tootin'. The time period. The expansion of settlement exceeding governmental reach and the perception of lawlessness.

#### TV vs Movie

Deep questions about consciousness and self and where they really really _aren't_.

#### AI in the 70s

Depictions of artificial general intelligence in the period and how awesome 2001: A Space Odyssey was/is. The "AI Winter." The Lighthill Debate on Artificial Intelligence. Heuristics. Teaching our machines to learn. Neural net black boxes. Machines created by other machines.

#### Smart AI/dumb AI

Collections of very convincing modules, sans consciousness, vs AGI. In what way can a computer system "understand" language?

#### Sensory processing

How long it takes humans and machines to adapt to moving in their environments. The speed of artificial neural networks in learning and human evolution of millennia. Computer vision in 1973! Face tracking. Human attention to human attention.

#### Robotics!

The difficulty in developing human-like robots. Our subconscious alertness to any oddness in our human-to--human interactions. The people-watching your brain does when you're not watching.

#### Computer viruses

Coinage. Von Nuemann's "self-replicating automata."

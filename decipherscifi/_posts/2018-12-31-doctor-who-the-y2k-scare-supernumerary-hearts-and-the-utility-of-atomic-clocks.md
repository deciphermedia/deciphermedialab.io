---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2018-12-31 10:00:41+00:00
description: 'Our introduction to Doctor Who! Millenium pedantry. The Y2K bug. Counting time from the beginning of the universe. Atomic clocks and their role in GPS.'
layout: podcast_post
permalink: /decipherscifi/doctor-who-the-y2k-scare-supernumerary-hearts-and-the-utility-of-atomic-clocks
redirect_from:
  - /decipherscifi/174
slug: doctor-who-the-y2k-scare-supernumerary-hearts-and-the-utility-of-atomic-clocks
title: 'Doctor Who: the Y2K scare, supernumerary hearts, and the utility of atomic clocks'
tags:
  - atomic clocks
  - biology
  - computing
  - science
  - science fiction
  - timekeeping

# Episode particulars
image:
  path: assets/imgs/media/movies/doctor_who_1996.jpg
  alt: One guy, and then another guy. And a clock!
links:
media:
  links:
    - text: Amazon
      url: 'https://www.amazon.com/Doctor-Who-Special-Paul-McGann/dp/B0049S1NYG'
  title: Doctor Who The Movie
  type: movie
  year: 1996
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/doctor_who_1996.jpg
  number: 174
  guid: 'https://decipherscifi.com/?p=9698'
  media:
    audio:
      audio/mpeg:
        content_length: '26078038'
        duration: '31:02'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Doctor_Who_--_--_Decipher_SciFi.mp3'
---
#### Doctor Who

Not a lot of Doctor Who experience at the table here. Recognition of the Moffat era. Finding a single, self-contained Who unit to use as an entrée. James McAvoy.

#### Millenium

Pedantic note: the millenium wouldn't really start until _2001_. Colbert gets to be the jerk for once.

#### Y2K

Numeronyms. Alternate terms: CDC, FADL. How we got here (there, then?). Limits on data storage and memory over time that make two-digit date storage actually seem kinda reasonable. Y2K38, aka the Unix millnium bug. The horrors of datetime in software and can you imagine the pain of writing this code for _time travel_. Standard datetime epochs: unix epoch, planet epoch, and time-since-the-big-bang epoch. We're gonna need a lot more memory.

#### Multiple hearts

Doctor Who has got a lot of _heart_. But also he has literally two hearts. Animals irl on Earth with multiple "hearts." Octopus (3), hagfish (4). The role of auxiliary "hearts" in these animals. Human heart grafting in heterotopic aka "piggy-back" heart transplants.

#### Atomic clocks

The history of time-measurement accuracy. From sundials to Hugens' pendulum to modern atomic clocks. Appreciating GPS and understanding how atomic clocks are integral to their function.

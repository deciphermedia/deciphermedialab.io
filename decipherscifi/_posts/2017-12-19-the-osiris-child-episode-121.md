---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2017-12-19 06:58:17+00:00
description: 'Indie scifi love. Floating cities. Conversational personal assistants. Terraforming. Monsters, Ninja Turtles, Goombas. Nuclear meltdowns. Rule 34. Dr Moreau'
layout: podcast_post
permalink: /decipherscifi/the-osiris-child-episode-121
redirect_from:
  - /decipherscifi/121
slug: the-osiris-child-episode-121
title: 'The Osiris Child: floating cities, terraforming, and the reverse Moreau maneuver'
tags:
  - dr moreau
  - genetic modification
  - indie film
  - nuclear reactors
  - science
  - science fiction
  - space settlement
  - terraforming

# Episode particulars
image:
  path: assets/imgs/media/movies/the_osiris_child_2016.jpg
  alt: A cool guy and a little kid and some spaaaaaaaaaaace
links:
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/the-osiris-child-science-fiction-volume-one/id1282789699?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Osiris-Child-Science-Fiction-One/dp/B075KFYGW6'
  title: The Osiris Child
  type: movie
  year: 2016
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/the_osiris_child_2016.jpg
  number: 121
  guid: 'https://decipherscifi.com/?p=4791'
  media:
    audio:
      audio/mpeg:
        content_length: 30978048
        duration: '36:52'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/The_Osiris_Child_--_--_decipherSciFi.mp3'
---
#### Indie scale

Scifi adventure throwback. Corporations are always evil. Ambition in scope of indie scifi.

{% youtube "https://www.youtube.com/watch?v=v3XR9VNcaxA" %}

#### Cloud city

Floating cities and air bases. Energy requirements. Floating cities on Venus, where balloons of an atmosphere of Earth-like density can float safely and happily in the high clouds. Hamster ball pleasure cruses. IRL "floating aircraft carriers." Realizing that all aircraft carriers are "floating."

#### Space settlement light and dark

The apparent necessity of slave labour. Western expansion analogies. Naval analogies. Australian analogies. Hoping that space settlement becomes mundane.

#### Backwards Dr Moreau

{% responsive_image path: assets/imgs/misc/osiris_child_ragged_comparison.jpg alt: "Osiris Child 'ragged' monster comparison with Ninja Turtles and Goombas" caption: "A comparison" attr_text: "Individual picture elements property of their respective owners" %}

"Raggeds." Reverse mode on [Dr. Moreau]({% link decipherscifi/_posts/2016-11-01-island-of-dr-moreau-jeo-ruppel-movie-podcast-episode-62.md %}). Echoes of evil ninja turtles and goombas. Send the gorillas to get the mongoose.

{% youtube "https://www.youtube.com/watch?v=P9yruQM1ggc" %}

#### "Terraforming"

Maybe "ecoforming?" Ecological manipulation. Seeding new ecology vs tearing down existing ecology. NASA sterilization practices and caution in avoiding contamination vs throwing turtle monsters at it.

#### Self-destruct cleanup mode

[Rule 34](https://en.wikipedia.org/wiki/Rule_34_(Internet_meme)? Nuclear reactors can explode, but they don't explode _like a nuclear bomb_. Chernobyl as example. Hot water and pressure reactive with metal and exploding. Metldown and the elephant foot.

#### Taking risks

Character elimination and happy fairy tale endings.

{% youtube "https://www.youtube.com/watch?v=YpXPtAVdMIY" %}

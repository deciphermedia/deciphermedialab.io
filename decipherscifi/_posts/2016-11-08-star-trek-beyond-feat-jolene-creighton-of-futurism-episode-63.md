---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2016-11-08 08:18:19+00:00 
description: 'Star Trek Beyond science stuff with Jolene from Futurism.com. Teleporters, gravity, EXPLOSIVE DECOMPRESSION, translators universal and otherwise, more' 
layout: podcast_post
permalink: /decipherscifi/star-trek-beyond-feat-jolene-creighton-of-futurism-episode-63
redirect_from:
  - /decipherscifi/63
slug: star-trek-beyond-feat-jolene-creighton-of-futurism-episode-63
title: 'Star Trek Beyond: drone swarms, teleporters, and decompression w/ Jolene Creighton' 
tags:
  - science fiction
  - science
  - star trek
  - explosive decompression
  - language
  - translation
  - drones
  - teleporters

# Episode particulars
image:
  path: assets/imgs/media/movies/star_trek_beyond_2016.jpg
  alt: "Some Star Trek characters' faces and a Starship Enterprise"
links:
  - text: The Trouble with Teleporters by CGP Grey
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=nQHBAdShgYI'
  - text: Deep Space Nine
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/tv-season/star-trek-deep-space-nine/id257255214?mt=4&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Emissary/dp/B000V5Y1SQ'
  - text: The United Federation of 'Hold my beer, I got this'
    urls:
      - text: Imgur
        url: 'http://imgur.com/gallery/wpZ4w'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/star-trek-beyond/id1128456722?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'https://www.amazon.com/Star-Trek-Beyond-John-Cho/dp/B01IG0H9JU'
  title: Star Trek Beyond
  type: movie
  year: 2016
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: jolene_creighton
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/star_trek_beyond_2016.jpg
  number: 63
  guid: 'http://www.decipherscifi.com/?p=1113'
  media:
    audio:
      audio/mpeg:
        content_length: 36054221
        duration: '50:04'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Star_Trek_Beyond_Jolene_Creighton_decipherSciFi.mp3'
---
#### Lament

Our love for Star Trek and how this does or does not fit.

#### Drone Swarm

Drone swarm tactics. Realizing they're robots.

#### Explosive Decompression

It is all manners of unpleasant.

#### Universal Translator

Christopher's revelation. IRL real-time translation software. Skype Translator.

#### Security Systems

And bad design.

#### Teleporters

Breaking phsyics. Weaponization.

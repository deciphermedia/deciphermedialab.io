---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2017-05-09 07:45:12+00:00
description: "Guardians! A fun movie with great characters. Tree science and how Groot might work. Animal intelligence and Rocket's enhancement. Language and metaphor."
layout: podcast_post
permalink: /decipherscifi/guardians-of-the-galaxy-episode-89
redirect_from:
  - /decipherscifi/89
slug: guardians-of-the-galaxy-episode-89
title: 'Guardians of the Galaxy: Groot language, intelligent animals, and where do trees get their mass?'
tags:
  - science fiction
  - science
  - space
  - intergalactic civilization
  - exobiology
  - trees
  - language
  - linguistics
  - analog
  - racoons

# Episode particulars
image:
  path: assets/imgs/media/movies/guardians_of_the_galaxy_2014.jpg
  alt: Guardians of the Galaxy team picture movie backdrop
links:
  - text: Why Do Marvel's Movies Look Kind of Ugly? by Patrick Willems
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=hpWYtXtmEFQ'
  - text: Guardians of the Galaxy 2
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/movie/guardians-of-the-galaxy-vol-2/id1225848804?mt=6&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Guardians-Galaxy-Vol-Bonus-Features/dp/B0718Z9G5W'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/guardians-of-the-galaxy/id899347364?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Guardians-Galaxy-Theatrical-Chris-Pratt/dp/B00QROH0QK'
  title: Guardians of the Galaxy
  type: movie
  year: 2014
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/guardians_of_the_galaxy_2014.jpg
  number: 89
  guid: 'http://www.decipherscifi.com/?p=1524'
  media:
    audio:
      audio/mpeg:
        content_length: 34747475
        duration: '41:21'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Guardians_of_the_Galaxy_--_--_decipherSciFi.mp3'
---
#### A bit of review

Because we really enjoyed this movie.

#### The team

A human, a green humanoid, an enhanced raccoon, Drax, and a hypermobile hyperintelligent tree.

#### Rocket Raccoon

Cybernetic skeletons. Animal intelligence. Cognitive enhancement and different types of intelligence.

#### Drax

Kinda like a grey hulk. Putting our lack of comics expertise on display.

#### Groot

How plants work. Nitrogen from the air making 50% of tree biomass. Plant regeneration and communication.

#### Language considerations! Yay!

Possible layers of complexity under "I am Groot." The different ways that could or could not represent language. The complexity of metaphors and similar linguistic mechanisms, and how Drax's people could lack understanding. Thermians. Tamarians.

#### Old Terran technology

Chris Pratt's cassette tapes. Batteries. Audio cassette technology. Wearing oxides and tape-stretching.

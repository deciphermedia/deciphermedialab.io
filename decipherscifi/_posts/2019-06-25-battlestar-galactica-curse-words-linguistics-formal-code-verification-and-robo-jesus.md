---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - tv
date: 2019-06-25 07:45:06+00:00
description: 'Ronald Moore worldbuilding. Frak and fracking and the linguistic gravity of swear words. Anti-AI security. code proofing and modularization. Robot theism. Etc.'
layout: podcast_post
permalink: /decipherscifi/battlestar-galactica-curse-words-linguistics-formal-code-verification-and-robo-jesus
redirect_from:
  - /decipherscifi/199
slug: battlestar-galactica-curse-words-linguistics-formal-code-verification-and-robo-jesus
title: 'Battlestar Galactica: curse words linguistics, formal code verification, and robo Jesus'
tags:
  - artificial intelligence
  - cyborgs
  - humanity
  - language
  - linguistics
  - religion
  - robots
  - science
  - science fiction
  - star trek
  - swearing

# Episode particulars
image:
  path: assets/imgs/media/tv/battlestar_galactica_2004.jpg
  alt: 'Battlestar Galactica cast in Last-Supper mode'
links:
  - text: 'Diaspora, The Free BSG Space-Sim based on Freespace 2'
    urls:
      - text: 'Hard-Light.net'
        url: 'http://diaspora.hard-light.net/'
  - text: Caprica
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/tv-season/caprica-season-1/id345695317?mt=4&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Caprica-Pilot-Part-1/dp/B0031UNWWE'
media:
  links:
    - text: iTunes
      url: 'https://itunes.apple.com/gb/tv-season/bsg-the-complete-collection/id946709572?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/33/dp/B000UU2YKE'
  title: Battlestar Galactica
  type: tv
  year: 2004
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/tv/battlestar_galactica_2004.jpg
  number: 199
  guid: 'https://decipherscifi.com/?p=10447'
  media:
    audio:
      audio/mpeg:
        content_length: 28435438
        duration: '33:50'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/battlestar_galactica_--_--_decipher_scifi.mp3'

---
#### Review

Battlestar was so good! The importance of Ronald D Moore's work on Deep Space Nine. The genesis of the "gritty" scifi drama juxtaposed against Star Trek's utopia. Judging the best scifi captains: Adama? Picard?

#### Frak!

The difficulties with "frak." "Fraking" vs "fracking." Changes of spelling. Designing the curse words of scifi futures. Appreciating [Nick Farmer]({% link _people/nick_farmer.md %})'s "Belter." The nature of "swear words." Comparing the sounds of made-up brand names to test linguistic form/function clustering.

Anti-AI security

Computers are good at computers. Fully securing your "hello world" program. Code testing and integration. Formal verification to mathematically guarantee computer code, and the incredible expense thereof.

#### Robo Jesus

Creatures coming into sapience in a world with the scientific method and body of knowledge and still turning to "God."

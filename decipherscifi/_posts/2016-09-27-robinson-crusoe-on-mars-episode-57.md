---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2016-09-27 07:48:43+00:00
description: 'Robinson Crusoe on Mars gets us talking about all kinds of Mars science and the history of our knowledge. Water, atmosphere, underwater sausage plants, more'
layout: podcast_post
permalink: /decipherscifi/robinson-crusoe-on-mars-episode-57
redirect_from:
  - /decipherscifi/57
slug: robinson-crusoe-on-mars-episode-57
title: 'Robinson Crusoe on Mars: Mars history, golden age space, and scientific accuracy'
tags:
  - albido
  - atmosphere
  - colonialism
  - columbus
  - mars
  - martian canals
  - schiaparelli
  - science
  - science
  - science fiction
  - space exploration

# Episode particulars
image:
  path: assets/imgs/media/movies/robinson_crusoe_on_mars_1964.jpg
  alt: Robinson Crusoe on Mars in a spacesuit helping a 'native' man. Also there are flying saucers? Robinson Crusoe on Mars movie backdrop.
links:
  - text: The Martian
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/movie/the-martian/id1039586890?at=1001l7hP&mt=6'
      - text: Amazon
        url: 'https://www.amazon.com/Martian-Michael-Pe%C3%B1a/dp/B018HIZSIA'
  - text: The Martian on Decipher SciFi
    urls:
      - text: Decipher SciFi
        url: 'https://www.decipherscifi.com/the-martian-podcast-episode-12/'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/robinson-crusoe-on-mars/id298711900?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'https://www.amazon.com/Robinson-Crusoe-Mars-Paul-Mantee/dp/B001YX2Q2C/ref=sr_1_1?ie=UTF8&qid=1474937045&sr=8-1&keywords=robinson+crusoe+on+mars'
  title: Robinson Crusoe on Mars
  type: movie
  year: 1964
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/robinson_crusoe_on_mars_1964.jpg
  number: 57
  guid: 'http://www.decipherscifi.com/?p=1030'
  media:
    audio:
      audio/mpeg:
        content_length: 23365983
        duration: '38:56'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Robinson_Crusoe_on__Mars_decipherSciFi.mp3'
---
#### Steven Seagal

Adam West, pre-Batman. An impressive entrance and an early, unfortunate exit.

#### Scientific Accuracy

Surprisingly reasonable space tech and portrayal of mars! For the time.

#### The Martian

Commonalities. Scientific accuracy with the knowledge of the time.

#### Mars

Bowser fireballs. Atmosphere.

#### History of our Knowledge of Mars

Galileo, albido mapping, canali, Percivall Lowell, modern missions.

#### Planetary Exploration

How to explore a new planet

1. Open mask
2. Breathe deeply
3. Cross your fingers

#### Martian Resources and Features

Water, underwater sausage plants, red vines, air, atmosphere. Ice caps, aurora, vulcanism.

#### Columbus on Mars

Exploration and enslaving people. Friday, Friday. Gotta get down on Friday. Culture and intelligence.

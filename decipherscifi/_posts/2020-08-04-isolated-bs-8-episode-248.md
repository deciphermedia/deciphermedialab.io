---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - conversation
date: 2020-08-04T02:52:08-04:00
description: 'Innovative smartphone camera VFX. Crossing fingers for revolutions in creative software. Machine learning the weather. Doom on Win95 in Minecraft. Game feel. And game programming for kids!'
layout: podcast_post
redirect_from:
  - /decipherscifi/248
slug: isolated-bs-3d-tracking-doom-everything-game-juice
title: 'Isolated BS: 3D tracking, Doom on EVERYTHING, and game juice'
tags:
  - children
  - computing
  - doom
  - game design
  - machine learning
  - programming
  - science
  - science fiction
  - smartphones
  - video games

# Episode particulars
image:
  path: assets/imgs/media/misc/isolated_bs_8_doom.jpg
  alt: Decipher Media brain Doom guy, presumably in Hell
links:
  - text: 'CamTrackAR'
    urls:
      - text: Apple App Store
        url: 'https://apps.apple.com/us/app/camtrackar/id1502545167'
  - text: DecipherMedia Site Search
    urls:
      - text: DecipherMedia.tv
        url: 'https://deciphermedia.tv/search'
  - text: 'You Can Now Play DOOM on a Windows 95 PC Inside Minecraft'
    urls:
      - text: InterestingEngineering.com
        url: 'https://interestingengineering.com/you-can-now-play-doom-on-a-windows-95-pc-inside-minecraft'
  - text: Game Feel
    urls:
      - text: GameFeel.com
        url: 'http://www.game-feel.com/'
  - text: "Vlambeer - 'The art of screenshake'"
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=AJdEqssNZ-U'
  - text: 'Game Feel: Why Your Death Animation Sucks'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=pmSAG51BybY'
  - text: 'Scratch - Imagine, Program, Share'
    urls:
      - text: MIT.edu
        url: 'https://scratch.mit.edu/'
media:
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/misc/isolated_bs_8_doom.jpg
  number: 248
  guid: isolated-bs-3d-tracking-doom-everything-game-juice
  media:
    audio:
      audio/mpeg:
        content_length: 45823505
        duration: '54:30'
        explicit: false
        url: 'https://traffic.libsyn.com/secure/decipherscifi/bs8.final.mp3'

---
# 3D tracking

Removing the [manual work of 3d tracking](https://www.youtube.com/watch?v=InIuTtt7W3E) in video production. [CamTrackAR](https://apps.apple.com/us/app/camtrackar/id1502545167). Smartphone camera software processing innovations. Making creative tools more widely available. Grousing about Adobe's overwhelming market-share and user-hostile subscription model. Hoping for a shift.

# Weather AI

Dark Sky, the honestly best of all weather apps, [was bought by Apple](https://blog.darksky.net/) and have shut down their Android app. It is a sad day. Open government weather data APIs enabling innovation. Machine learning weather models. Hyper-local extrapolation and push notifications. Weather forecasting/reporting as a demonstration of probabilities.

# Website search

We took the bullet for our website visitors and implemented a [site search on DecipherMedia.tv]({% link search/index.html %}) in JavaScript! The result: we still do not enjoy JavaScript. But we do have search! Future functionality improvements. Here is our [internal documentation](https://gitlab.com/deciphermedia/deciphermedia.gitlab.io/-/blob/master/_docs/search.md). And a reminder that our whole site is open source and available [on GitLab](https://gitlab.com/deciphermedia/deciphermedia.gitlab.io)!

# Doom on everything

Our continuing fascination in seeing Doom on more things. And the less it makes sense, the better! [Doom on Windows 95 in Minecraft](You Can Now Play DOOM on a Windows 95 PC Inside Minecraft), because *why not*.

# Game feel

Game feel [the book](http://www.game-feel.com/). Creating a satisfying input/response loop when all you're *really* doing is mashing buttons. Vlambeer on mobile game "juice." The relative difficulty of providing good "feel" on mobile games with a uniformly smooth display as an input device. Putting overtime into the design of basic repetitive game mechanics, e.g. Mario or God of War. New frontiers in game feel.

# Game programming for kids

A wonderful, simple, directly productive introduction to core concepts: [Scratch](https://scratch.mit.edu/)! Proud parent moments.

# Jesus news

Apparently The Passion is getting a sequel? The wackiness of ecclesiastical minutiae. Discovering more schisms in Christianity.

---
redirect_to: https://patreon.com/decipherscifi
redirect_from:
  - /decipherhistory/supporttheshow
  - /decipherscifi/supporttheshow
  - /support
  - /supporttheshows
author: Christopher Peterson
link: https://deciphermedia.tv/supporttheshow/
slug: supporttheshow
title: Support The Show!
---

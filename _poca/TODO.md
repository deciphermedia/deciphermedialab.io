* remove subscribe button when on a non-podcats subsite. replace with link to a general subscribe page...?
* Figure out how to include chaptermakrs or timestamps or whatever and pull those from the time file in audacity. there are multiple potential chapter formats! either in rss or in shownotes and i need to see which are most popular. here are some:
  - podlove simple chapters (in rss). supported by spotify: https://podlove.org/simple-chapters/
  - id3 chapters: eyed3 might support this? might be multiple standards, not sure.. :/
  - just typing `(hh::mm:ss)` into show notes seems to be supported without a standard by several clients including spotify https://podcasters.spotify.com/faq#can-i-link-to-particular-points-of-my-podcast. "You can also use a combination to express a range: (hh:mm:ss-hh:mm:ss)."
* in tests: make sure the link to the episode image and the mp3 show up in the final page
* actually point to the blog, somehwere somehow
* podcast plugin support itunes-new-feed tag from _config
* make zardon img have correct aspect ratio
* style lists, ordered and unordered. they are bad rn.
* test: all media urls are https
* test: all required podcats fields are present in `_config`
* inform the podcast plugin of yet more difft desintations and allow the configuration to pick which ones get listed in the subscribe buttons. add a field like `button` ?
* pull ep summmaries fot feed from orig wp db
* turn on frontpage people display once it is random or something
* turn on frontpage "latest episodes"
* make header image responsive
* paginate people list eventually
* Crsate tests fir podcadt plugin reqyired fielfd
* loop add links from episode frontmatter into podcast post body
* remove the wp-content directory with the old podcast logo after the new feed takes over
* make sure links in episode page body include our highlight coloring in a resting state
* add the "irl" or "meetup" page witha schedule of upcoming events. like movies together in nyc, or *conventions*
* link to person pages from individual episode posts
* make a point in the about/opensource/etc about accessibility and our attempt at it, and our openness to improvement. PS: how to properly hide a longdesc div http://www.d.umn.edu/itss/training/online/moodle_downloads/accessibility_104/hidingcontent.html
* randomize or otherwise make the features guests frontpage section not always be the same? add a weighting value to the people and sort so ted chiang comes out on top etc?
* open source and creative commons resource page AND footer
* update itunes logo to a new path, but ensure the old path serves it as well for a while
* create paginated category and tag pages
* consider the prettylinks we have on the existing site that go to *outside* targets. maintain them? do away?
* use fa home icon in the breadcrumb toplevel
* linkify author name if present in podcats post page
* use norobots headers on pages that hurt seo e.g. duplicated content under different urls
* enable site search and unhide the searchbar
* html5 subscribe buttons with FA icons. vartical stacked bottom, horizontal list top.
* investigate the in-place list filtering situation with the post list
* switch the episode/date on the show listing to black, make the title links hover blue
* ensure header image/logo/etc from the theme is all non-huge
* create a 'patreon' color style for the bottom social buttons and the share buttons as per the others
* fix the facebook "share" button used in episode posts
* css/js randomize newsletter bg images between selfies
* figure out how to update font-awesome and do so in order to get the patreon symbols and probably others
* the category filter in the podcast pages: work out the liquid to select the top n categories or tags to filter by?
* the home > subsite line under the header should programmatically link to its parents
* templatize the style.css (just changing hover and primary colors, i think) per site/subsite
* hide top searchbox until clicked? https://www.solodev.com/blog/web-design/creating-a-toggled-search-bar.stml
* realtime search narrowing on people page
* use the existing post list narrowing but for the people list by show?
* ensure accessibility with proper tagging of everything pls. 
  - bootstrap breadrcumbs recommend:
      > Accessibility
      > Since breadcrumbs provide a navigation, it’s a good idea to add a meaningful label such as aria-label="breadcrumb" to describe the type of navigation provided in the <nav> element, as well as applying an aria-current="page" to the last item of the set to indicate that it represents the current page.

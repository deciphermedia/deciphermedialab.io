---
layout: person_page
person_data:
  name: James Peterson
  data_name: james_peterson
  image_path: assets/imgs/people/james_peterson.jpg
  title: Chemical engineer
  bio: 'Petroleum engineering smartypants'
  social:
  roles:
    - show: decipherscifi
      role: answerer
---

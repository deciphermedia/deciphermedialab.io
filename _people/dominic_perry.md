---
layout: person_page
person_data:
  name: Dominic Perry
  data_name: dominic_perry
  image_path: assets/imgs/people/dominic_perry.png
  title: Egyptologist
  bio: 'Prolific history/archaeology podcaster. His area of concentration was ancient Egyptian economics, but you might be surprised how well it fits into a sci-fi podcast.'
  websites:
    - name: The History of Egypt Podcast
      url: 'https://egyptianhistorypodcast.com/'
  social:
    - name: twitter
      url: https://www.twitter.com/egyptianpodcast
    - name: instagram
      url: https://www.instagram.com/egyptpodcast
    - facebook: egyptianpodcast
  roles:
    - show: decipherscifi
      role: guest_cohost
---

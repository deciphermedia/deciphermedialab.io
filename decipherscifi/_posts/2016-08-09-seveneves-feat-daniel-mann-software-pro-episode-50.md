---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - book
date: 2016-08-09 07:22:12+00:00
description: 'Covering a book this week: Seveneves, by Neal Stephenson. Meteorites, Tunguska, Chelyabinsk, apocalypse, jumping down wiki-holes, space tech, gliding, more'
layout: podcast_post
permalink: /decipherscifi/seveneves-feat-daniel-mann-software-pro-episode-50
redirect_from:
  - /decipherscifi/50
slug: seveneves-feat-daniel-mann-software-pro-episode-50
title: 'Seveneves: impactors, guided evolution, and radiation shields w/ Daniel Mann'
tags:
  - science fiction
  - science
  - moon
  - space settlement
  - space station
  - apocalypse
  - evolution

# Episode particulars
image:
  path: assets/imgs/media/books/seveneves_2015.jpg
  alt: Starburst lens flare on an eyeball Seveneves book backdrop
links:
  - text: The Day The Moon Blew Up by Bill Gates
    urls:
      - text: GatesNotes
        url: 'https://www.gatesnotes.com/Books/Seveneves'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/book/seveneves/id901493995?mt=11&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Seveneves-Novel-Neal-Stephenson-ebook/dp/B00LZWV8JO'
  title: Seveneves
  type: book
  year: 2015
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: daniel_mann
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/books/seveneves_2015.jpg
  number: 50
  guid: 'http://www.decipherscifi.com/?p=948'
  media:
    audio:
      audio/mpeg:
        content_length: 39197768
        duration: '01:05:19'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Seveneves_Dan_Mann_decipherSciFi.mp3'
---
#### Read the book

Our attempt to convince folks to read the book. Do it!

> The moon blew up without warning and for no apparent reason. It was waxing, only one day short of full. The time was 05:03:12 UTC. Later it would be designated A+0.0.0, or simply Zero.
> - <cite>The opening paragraph of Seveneves</cite>

#### Neal Stephenson

Big old nerd. Chris expresses appreciation for his deep-dives.

#### Meteor Strikes

Tunguska. Chelyabinsk. Measuring in five dollar footlongs and blue whales. Who knows how many fathoms?

#### Shielding!

Humans and computers are both susceptible to radiation.

#### Choosing Our Evolution

Deaf parents choosing to have deaf babies. Culture.

#### Technology in Space

Difficulty in producing microchips. Dan's experience in chemicals and computing. Lost knowledge.

#### Gliding

Thermals and ridge lifts and whatnot. Terrifying!

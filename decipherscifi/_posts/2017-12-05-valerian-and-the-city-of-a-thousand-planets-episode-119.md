---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2017-12-05 07:45:21+00:00
description: 'Valerian! crazy big world-building. Future space stations, aliens and bipedalism, space city modularity, space battles and planetary proximity, more!'
layout: podcast_post
permalink: /decipherscifi/valerian-and-the-city-of-a-thousand-planets-episode-119
redirect_from:
  - /decipherscifi/119
slug: valerian-and-the-city-of-a-thousand-planets-episode-119
title: 'Valerian and the City of a Thousand Planets: alien evolution, intelligence exaptation, and layered universes'
tags:
  - aliens
  - evolution
  - intelligence
  - science
  - science fiction
  - space
  - space battles
  - space stations
  - universes

# Episode particulars
image:
  path: assets/imgs/media/movies/valerian_and_the_city_of_a_thousand_planets_2017.jpg
  alt: Lots of character heads and colors
links:
  - text: "The Fifth Element: space turtles, magic stones, and LeeLoo Dallas multipass"
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/34'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/valerian-and-the-city-of-a-thousand-planets/id1254728550?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Valerian-City-Thousand-Planets-DeHaan/dp/B0748PJSBJ'
  title: Valerian and the City of a Thousand Planets
  type: movie
  year: 119
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/valerian_and_the_city_of_a_thousand_planets_2017.jpg
  number: 119
  guid: 'https://decipherscifi.com/?p=4458'
  media:
    audio:
      audio/mpeg:
        content_length: 30339072
        duration: '36:06'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Valerian_and_the_City_of_a_Thousand_Planets_--_--_decipherSciFi.mp3'
---
#### International Space Station

Contemporary space station size comparisons. ISS transformation over time.

#### Alien life

Bipedalism. Bilateral symmetry. The assumption of humanoid structure in the "world." Duck-billed Ferengi information brokers and neurological RAID modes.

#### Alpha Station

"[Critical mass](https://en.wikipedia.org/wiki/Critical_mass)." Vital stats. Population, demographics. Modular growth over time. Racial job specialization.

#### Alpha Station location and travel

FTL(?) via "exospace." The plan to exit the solar system. The issues re planetary orbits when you build a brand new moon around earth with tons and tons of foreign (to the solar system) mass. Languages! 5,000+ languages are spoken on the station, not even including "computer languages."

#### Mül and the Pearls

Beach-dwelling N'avi (with no tails). Math and science. Intellectual ability and exaptation of base rational abilities to science and math.

#### Space battles

Should they happen so near planets? Travel and planned space battle. Don't fight _on_ a planet because you're vulnerable to Christopher's favorite: the Brannigan maneuver.

---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - book
date: 2020-04-21T02:45:59-04:00
description: 'Chinese sci-fi context. Scale-accurate alien communications. The *ACTUAL* three body problem. Simple models and complex results. Creative civilizational sabotage. Math!'
permalink: /decipherscifi/the-three-body-problem-mathy-math-math-and-first-contact-game-theory-w-adrian
layout: podcast_post
redirect_from:
  - /decipherscifi/235
slug: the-three-body-problem-mathy-math-math-and-first-contact-game-theory-w-adrian
title: 'The Three Body Problem: mathy math math and first contact game theory w/ Adrian'
tags:
  - aliens
  - first contact
  - mathematics
  - physics
  - science
  - science fiction
  - space
  - space communications
  - space travel
  - technology

# Episode particulars
image:
  path: assets/imgs/media/books/three_body_problem_2008.jpg
  alt: "The 'Three Body Problem' pyramid art"
links:
  - text: "The Wandering Earth: altered goldilocks zones, Earth's heat budget, and smuggling pickles in your pants"
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/182'
  - text: 'Three Body Problem the Minecraft animated series'
    urls:
      - text: 'Season 2 @ YouTube'
        url: 'https://www.youtube.com/watch?v=TusUoA1emI4&list=PLJ1-YFDG-WNqa3YQBK1m3PGzqok6m2LZV'
      - text: 'Season 3 @ YouTube'
        url: 'https://www.youtube.com/watch?v=blaMSFtFiKI&list=PLuKJTzk2Qbb5BLcXy2UIHyU8W6A2_c1WH'
  - text: 'Division by Zero by Ted Chiang'
    urls:
      - text: Fantastic Metropolis
        url: 'https://fantasticmetropolis.com/i/division'
media:
  links:
    - text: iTunes
      url: 'https://books.apple.com/us/book/the-three-body-problem/id856893409?mt=11&app=itunes&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Three-Body-Problem-Remembrance-Earths-Past-ebook/dp/B00IQO403K'
  title: The Three Body Problem
  type: book
  year: 2008
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: adrian_falcone
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/books/three_body_problem_2008.jpg
  number: 235
  guid: 'https://decipherscifi.com/?p=10977'
  media:
    audio:
      audio/mpeg:
        content_length: 49970704
        duration: '59:26'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/three_body_problem.final.mp3'

---
#### Books!

A larger time investment, generally... but high idea-load! That [one time]({% link decipherscifi/_posts/2016-08-09-seveneves-feat-daniel-mann-software-pro-episode-50.md %}) we did a book in the past. And that [next one](https://www.rifters.com/real/Blindsight.htm) we'll probably do in the future.

#### Context

Chinese science fiction. This book as the first Asian Hugo Award-winner. The difficulty of translating highly technical, *crunchy* hard scifi. Translation footnotes or the lack thereof in the audiobook. Prior movie adaptation from the same author on Decipher SciFi: [The Wandering Earth]({% link decipherscifi/_posts/2019-02-26-the-wandering-earth-altered-goldilocks-zones-earths-heat-budget-and-smuggling-pickles-in-your-pants.md %}). Adaptions, failed or otherwise. Minecraft engine magic.

#### Alien Communications

Inverse square law and power and focus difficulties. Reaching out way above our stellar paygrade and reaping the consequences. Scale-accurate "first contact" scenarios.

> DO NOT REPLY
>
> DO NOT REPLY
>
> DO NOT REPLY
>
>
> -An alien

#### The Three Body Problem

The frustation of having no closed-form solution to your planet's gravitational situtation. The value of the scientific method. Recognizing that *there is always a model*. The complexity of a model may not be veyr connected with the complexity of the results it yields.

#### Civilizational advancement

Targeting the loci of technological advancement. Figuring out how to *prevent* that advancement in opposing far-away civilizations.

#### Math!

What if math... [didn't work anymore]({% link decipherscifi/_posts/2018-01-09-nothing-but-gifs-qa-1-episode-124.md %})? Adrian's initial frustration with this book. How *totally exciting* it is to discover math is broken when you're not being tortured by aliens.

> I am the best mathematician that has ever existed.
> There is no one better.
> Put my name on a mountain!
> I did it.
>
> [Adrian Falcone]({% link _people/adrian_falcone.md %})

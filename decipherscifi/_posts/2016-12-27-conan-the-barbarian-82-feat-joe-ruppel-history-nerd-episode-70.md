---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2016-12-27 08:35:10+00:00 
description: 'Joe Ruppel is back with more historical perspective for the original Conan the Barbarian. Swordmaking, making a sword from the blood of your enemies, more' 
layout: podcast_post
permalink: /decipherscifi/conan-the-barbarian-82-feat-joe-ruppel-history-nerd-episode-70
redirect_from:
  - /decipherscifi/70
slug: conan-the-barbarian-82-feat-joe-ruppel-history-nerd-episode-70 
title: 'Conan the Barbarian: antedeluvian mythology, bloodsmithing, and the riddle of steel  w/ Joe Ruppel' 
tags:
  - science fiction
  - science
  - metallurgy
  - arnold
  - history
  - fantasy
  - blood

# Episode particulars
image:
  path: assets/imgs/media/movies/conan_the_barbarian_1982.jpg
  alt: Conan the Barbarian standing triumphant with a hot 80s fantasy lady
links:
  - text: Conan the Barbarian the Musical
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=OBGOQ7SsJrw'
  - text: Science Fiction Film Podcast - Conan the Barbarian
    urls:
      - text: LSGMedia
        url: 'https://www.libertystreetgeek.net/conan-barbarian-82-bounty-hunters-club/'
  - text: 'Total Recall: My Unbelievably True Life Story'
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/audiobook/total-recall-my-unbelievably/id566563624?mt=3&app=itunes&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Total-Recall-Unbelievably-True-Story/dp/B009JY6WEQ'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/conan-the-barbarian/id311306408?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Conan-Barbarian-Arnold-Schwarzenegger/dp/B0026IYLS6'
  title: Conan the Barbarian
  type: movie
  year: 1982
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: joe_ruppel
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/conan_the_barbarian_1982.jpg
  number: 70
  guid: 'http://www.decipherscifi.com/?p=1231'
  media:
    audio:
      audio/mpeg:
        content_length: 44642972
        duration: '01:01:59'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Conan_the_Barbarian_--_Joe_Ruppel_--_decipherSciFi.mp3'
---
#### Conan the Bookbarian

Robert E. Howard and pulp fantasy.  His life  and death.

#### Arnold is amazing


Arnold Schwarzenegger appreciation hour. Training. Work ethic. Hustle. The American dream. Grizzled old beardy [Conan the Conqueror](http://www.cosmicbooknews.com/content/conan-the-conqueror). Arnold's blooper with the wolves:

{% youtube "https://www.youtube.com/watch?v=d98KJNnOoak" %}

#### Conan's world and mythology

"Antedeluvian" history. Chroniclers keeping record of the exploits of historical figures. Genghis Khan.

#### Swordmaking

Iron, steel, forge technology. [How not to make or use a sword](http://www.cracked.com/personal-experiences-1238-6-things-movies-get-wrong-about-swords-an-inside-look.html).

#### Steppe peoples

Horsemastery. Wind vs slave power. More Genghis Khan.

#### Doom

Cannibal cults. Human cookery.

#### The riddle of steel

Steel, flesh, [swords made from the blood of one's enemies](https://www.reddit.com/r/theydidthemath/comments/27na8y/request_how_many_men_would_you_need_to_kill_to/). [Our Warrior Dash video](https://youtu.be/dRxAR--65hk).

---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2017-04-04 07:45:47+00:00
description: "Aliens made of murder-muscle. Near-future space research. Panspermia. Universal biology. NASA twin studies. Orbital mechanics and escaping earth's gravity."
layout: podcast_post
permalink: /decipherscifi/life-episode-84
redirect_from:
  - /decipherscifi/84
slug: life-episode-84
title: 'Life: time in space, ISS safety, and literal firewalls'
tags:
  - science fiction
  - science
  - aliens
  - biology
  - evolution
  - space
  - space stations
  - horror
  - monsters

# Episode particulars
image:
  path: assets/imgs/media/movies/life_2017.jpg
  alt: Pretty people in spacesuits 😎
links:
  - text: Alien
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/movie/alien-the-directors-cut/id361991162?mt=6&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Alien-Sigourney-Weaver/dp/B003GXJ072'
  - text: What if... orbital speed?
    urls:
      - text: What If
        url: 'https://what-if.xkcd.com/58/'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/life/id1207067167?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Life-Ryan-Reynolds/dp/B06XT9L2R8'
  title: Life
  type: movie
  year: 2017
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/life_2017.jpg
  number: 84
  guid: 'http://www.decipherscifi.com/?p=1474'
  media:
    audio:
      audio/mpeg:
        content_length: 41920512
        duration: '49:53'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Life_--_--_decipherSciFi.mp3'
---
#### The International Space Station

Crew of six. Highly trained to do space stuff and... study aliens? Crazy crane-arms.

#### Is this the future?

Near-future? Referencing the challenger explosion. A survey of the evidence and the answer: basically this must be taking place now. World population: 8 billion!

#### Origins of alien life

Looked rather similar to life on our own planet. Common origins. [Panspermia](https://en.wikipedia.org/wiki/Panspermia). Or is life typical in the universe, and life as we understand it is a somewhat typical example?

#### Time in space

In the movie Jake Gyllenhaal claims 473 days in space. That's a lot! [NASA Twin study](https://www.nasa.gov/feature/how-stressful-will-a-trip-to-mars-be-on-the-human-body-we-now-have-a-peek-into-what-the-nasa).

#### _This_ alien

Wow what a bastard this alien is. "Just trying to survive." Pluripotency and cell specialization.  A brain made of muscles. "A giant piece of murder muscle." [A jet plane made of biceps](https://youtu.be/t-3qncy5Qfk?t=51). One cell

#### Kill it with fire!

Literal firewalls. Temperatures of incinerators. Could we burn it up by tossing it down into the atmosphere (hot!).

#### Kill it with space!

Give that alien some space. Aliens love space.

{% responsive_image path: assets/imgs/misc/bitches_love_space.jpg alt: "Bitches love space meme" %}

Plan B: Screw it! Push it and they and everything into space! But is the math in the ballpark of reasonable? Escape velocities from earth's surface vs the ISS.

{% youtube "https://www.youtube.com/watch?v=tbNlMtqrYS0" %}

---
author: christopher_peterson
categories:
  - blog
date: 2016-10-03 02:23:55+00:00
layout: post
slug: awesome-halloween-illustration-2016
title: 'Decipher SciFi Halloween Illustration'
image:
  path: assets/imgs/misc/decipherscifi-halloween.png
  alt: 'Decipher SciFi Halloween poster by Caelum Rale'
tags:
  - art
  - selfie

---
Artist-in-residence Cae has provided us with this wonderful illustration of us kicking some ass in a horror setting:

Thanks Cae! We've produced a big print of it, you'll see it on the wall in the studio. :)

* Illustration © [Caelum Rale](https://instagram.com/inclosedspaces), used with permission

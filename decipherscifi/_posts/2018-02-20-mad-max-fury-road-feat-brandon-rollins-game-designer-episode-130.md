---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2018-02-20 07:45:19+00:00
description: 'Mad Max Fury Road science. First: roads themselves! Then deserts, salt flats, parabiosis and adrenalie tranfer. Teeth and human co-evolution with our mouth bacteria. Chroming, car stuff with DJ Moffett, bards that melt your face off. And more!'
layout: podcast_post
permalink: /decipherscifi/mad-max-fury-road-feat-brandon-rollins-game-designer-episode-130
redirect_from:
  - /decipherscifi/130
slug: mad-max-fury-road-feat-brandon-rollins-game-designer-episode-130
title: 'Mad Max Fury Road: salt flats, parabiosis, and the Namib desert w/ Brandon Rollins'
tags:
  -
  - blood
  - cars
  - deserts
  - parabiosis
  - roads
  - salt flats
  - science
  - science fiction

# Episode particulars
image:
  path: assets/imgs/media/movies/mad_max_fury_road_2015.jpg
  alt: Furiosa glaring Fury Road move backdrop
links:
  - text: Highways and Byways a game from Brandon Rollins
    urls:
      - text: Highways & Byways
        url: 'http://bywaysgame.com/'
  - text: Brandon the Game Dev
    urls:
      - text: Brandon the Game Dev
        url: 'http://brandonthegamedev.com/'
  - text: 'Making-of Fury Road: Cars'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=9L67BiENzYs'
  - text: 'Making-of Fury Road: Tools'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=k52rhn2w1VM'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/mad-max-fury-road/id990549112?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Mad-Max-Fury-Tom-Hardy/dp/B00XOX9QCS'
  title: 'Mad Max: Fury Road'
  type: movie
  year: 2015
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: brandon_rollins
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/mad_max_fury_road_2015.jpg
  number: 130
  guid: 'https://decipherscifi.com/?p=5172'
  media:
    audio:
      audio/mpeg:
        content_length: 34301952
        duration: '40:49'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Mad_Max_Fury_Road_--_Brandon_Rollins_--_Decipher_SciFi.mp3'
---
#### Wow this is pretty good

Chris' varied experiences of this film over time. Color grading and the Black & Chrome edition.

#### Roads!

Roads are awesome. The US highway system is awesome. Eisenhower's road trip. Road trips are awesome, too.

#### Deserts!

Lack of roads in the outback. Accidental un-deserting. The Namib desert. The Namib dessert is 55 million years old!

#### The Great Salt

The oceans? No way. Red salt planet in [The Last Jedi]({% link decipherscifi/_posts/2017-12-26-star-wars-the-last-jedi-episode-122.md %}). Evaporating the oceans.

#### Blood bags

War Boys stealing blood from "healthy" donors. Lack of red blood cell production from blood cancer and the need for transfusion in the late stage. Blood loss limits. Adrenaline transfer.

#### Parabiosis

Science vampires! Callbacks to [Daybreakers]({% link decipherscifi/_posts/2017-10-17-daybreakers-episode-112.md %}). Combining mice. Kidnapping [Wim Hof](https://en.wikipedia.org/wiki/Wim_Hof) for his blood.

#### Teeth!

Malnutrition, radiation poisoning, the oral bacteriome. Dental hygiece pre- vs post-agrciulture. Beneficial dental plaques. The evolution of the human lower jaw.

#### Chroming

Berserker mushroom mode. Signalling. Low framerate editing effects.

#### Wtf cars

Car stuff with **DJ Moffett**! Superchargers, turbochargers, maintenance and machining.

#### Music

Guitar dude with his hundred-amp truck. Energy consumption of flameguy vs a literal drummer boy. Mobile FM transmitting stations. A bard that can melt your face.

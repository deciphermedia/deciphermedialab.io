---
# General post frontmatter
categories:
  - episode
  - rpg
date: 2018-09-12 07:45:40+00:00
description: 'Emporor Colbert dresses down and bodyguard Chris rediscovers the joy of the bidet and the German shelf toilet. Then: daring aerial maneuvers!'
layout: podcast_post
permalink: /decipherscifi/decipher-rpg-mirror-pilot-2
redirect_from:
  - /decipherscifi/rpg2
slug: decipher-rpg-mirror-pilot-2
title: 'Decipher RPG - Mirror Pilot #2'
tags:
  - actual play
  - fantasy
  - rpg

# Episode particulars
image:
  path: assets/imgs/media/rpg/mirror02.jpg
  alt: 'Mirror RPG character illustrations for episode 1 of 3'
links:
media:
  links:
    - text: Itch.io
      url: 'https://sandypuggames.itch.io/mirror-a-micro-rpg'
  title: Mirror
  type: rpg
  year: 2018
people:
  - data_name: christopher_peterson
    roles:
      - player
  - data_name: lee_colbert
    roles:
      - player
  - data_name: liam_ginty
    roles:
      - game_master
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/rpg/mirror02.jpg
  guid: 'https://decipherscifi.com/?p=7685'
  media:
    audio:
      audio/mpeg:
        content_length: 55128064
        duration: '01:05:27'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/rpg.01.mirror.ep02.final.mp3'
---
Emperor Colbert dresses down and bodyguard Chris rediscovers the joy of the bidet and the German shelf toilet. Then: daring aerial maneuvers!

(This podcast is the second part of our pilot series for Decipher RPG where we play [Mirror](http://www.sandypuggames.com/sandy-pug-orginals/) by Sandy Pug Games with [Liam Ginty]({% link _people/liam_ginty.md %}). Previously: [Part 1]({% link decipherscifi/_posts/2018-09-11-decipher-rpg-mirror-test-1.md %}))

---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2017-07-25 07:32:02+00:00
description: 'The ecology of Kong Skull Island. Also long-lived storms on Jupiter, nondestructive geological imaging techniques, square cube law, giant monster evolution'
layout: podcast_post
permalink: /decipherscifi/kong-skull-island-episode-100
redirect_from:
  - /decipherscifi/100
slug: kong-skull-island-episode-100
title: 'Kong Skull Island: geological mapping, combat-ready herbivores, and Kong size over time'
tags:
  - biological scale
  - evolution
  - geology
  - godzilla
  - herbivores
  - jupiter
  - king kong
  - satellites
  - science
  - science fiction
  - storms
  - weaponry

# Episode particulars
image:
  path: assets/imgs/media/movies/kong_skull_island_2017.jpg
  alt: "Kong silhouette approaching soldiers and helicopters and it's beautiful, movie backdrop"
links:
  - text: Why Do Venomous Animals Live In Warm Climates? by Veritasium
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=myh94hpFmJY'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/kong-skull-island/id1209705645?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Kong-Skull-Island-Tom-Hiddleston/dp/B06XGDDMF3'
  title: Kong Skull Island
  type: movie
  year: 2017
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/kong_skull_island_2017.jpg
  number: 100
  guid: 'https://decipherscifi.com/?p=2673'
  media:
    audio:
      audio/mpeg:
        content_length: 33779712
        duration: '40:12'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Kong_Skull_Island_--_--_decipherSciFi.mp3'
---
#### Hiding islands

Perpetual storms. Jupiter's storm and other non-Kongish Jupiter appreciation.

#### Spy Satellites

Early spy satellites. Stability-spins and film-return techniques.

#### Geological Imaging

"Seismic charges" i.e. bombs to irritate aggressive megafauna. Earthquakes, real or otherwise, and using them to understand the inside of the earth. The "hollow earth theory" is dumb garbage.

#### Kong

King Kong size inconsistencies over time and even within films. [Godzilla]({% link decipherscifi/_posts/2016-07-19-godzilla-miles-greb-movie-podcast-episode-47.md %}) comparisons.

#### Combat-ready herbivores

Gorillas! When herbivores have sharp fangs. The economics of displays of aggression instead of fighting.

#### Giant Creatures Evolution

Pygmification of island species irl. Limitations on resources. Keystone species in ecosystems.

#### The dangers of the tropics

Venomous creatures and their prevalence in warmer climes. [Why Do Venomous Animals Live In Warm Climates?](https://www.youtube.com/watch?v=myh94hpFmJY)

#### Medieval weaponry...

And how this relates to King Kong..? Was the flail a 'real' weapon?

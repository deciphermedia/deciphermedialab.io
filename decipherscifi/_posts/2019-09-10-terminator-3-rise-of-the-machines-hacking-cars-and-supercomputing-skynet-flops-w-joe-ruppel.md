---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2019-09-10 07:45:14+00:00
description: 'Terminator 3 science! Nanotech. Data collection and cell phone ubiquity. Hacking cars wirelessly. Hydrogen fuel cells as mini nukes. Skynet supercomputing!'
layout: podcast_post
permalink: /decipherscifi/terminator-3-rise-of-the-machines-hacking-cars-and-supercomputing-skynet-flops-w-joe-ruppel
redirect_from:
  - /decipherscifi/209
slug: terminator-3-rise-of-the-machines-hacking-cars-and-supercomputing-skynet-flops-w-joe-ruppel
title: 'Terminator 3 Rise of the Machines: hacking cars and supercomputing Skynet FLOPS w/ Joe Ruppel'
tags:
  - arnold schwarzenegger
  - cars
  - computing
  - james cameron
  - magnetic resonance imaging
  - man versus machine
  - science
  - science fiction
  - skynet
  - technology
  - terminators
  - time travel

# Episode particulars
image:
  path: assets/imgs/media/movies/terminator_3_2003.jpg
  alt: 'Scratched-up Arnold and a younger murder-bot'
links:
  - text: Decipher History
    urls:
      - text: Decipher Media
        url: 'https://deciphermedia.tv/decipherhistory'
  - text: 'Hackers Remotely Kill a Jeep on a Highway by WIRED'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=MK0SrxBC1xs'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/terminator-3-rise-of-the-machines/id283619485?mt=6'
    - text: Amazon
      url: 'https://www.amazon.com/Terminator-3-Machines-Claire-Danes/dp/B001AT4Y84'
  title: 'Terminator 3: Rise of the Machines'
  type: movie
  year: 2003
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: joe_ruppel
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/terminator_3_2003.jpg
  number: 209
  guid: 'https://decipherscifi.com/?p=10774'
  media:
    audio:
      audio/mpeg:
        content_length: 37040746
        duration: '43:59'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/terminator_3_2003.mp3'

---
#### Meta

James Cameron blank checks and record-breaking budgets and payouts.

#### Arnold

Getting re-jacked to your original shape at _56 years old_. Screening your movie at Saddam's palace.

#### Terminators

The broadness of the term "nanotechnology." Upgrading your liquid-metal nanotechss32 robots.

#### Data Collection

Placing this movie in the data collection timeline: after the dawn of big data digital government spying, but _before_ smartphones when we willingly began to give up _all_ the data. But there's a Terminator movie for that too!

#### Hacking cars

Direct linkages in car systems. Driving by-wire. Hacking automobile computer systems. Wired hacking and modern wireless car hacks.

#### Technology progression

Finally, a Terminator in the age of the ubiquitous cell phone! Still not smartphones yet, but it's a start! Hydrogen fuel cell failure modes and the danger of tiny hydrogen bombs. [MRI dangers](https://www.youtube.com/watch?v=plvIEf7JsKo).

#### Skynet

AI escape scenarios. Supercomputing at "60 teraFLOPS" and wth is a "[teraFLOPS](https://en.wikipedia.org/wiki/Teraflop)" anyway? Computing power  measurement and FLOP precisions. Recognizing the actual utility of supercomputers only becoms apparent with very paralellizable tasks.

---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2016-04-12 07:00:40+00:00
description: 'With Man of Steel we talk Superman, Kryptonian politics, Zod was the good guy (on Krypton), alien power armor, superpowers, sensory adaptation, more'
layout: podcast_post
permalink: /decipherscifi/superman-man-of-steel-movie-podcast-episode-33
redirect_from:
  - /decipherscifi/33
slug: superman-man-of-steel-movie-podcast-episode-33
title: 'Man of Steel: burly Superman, Kryptonian prison rehab, and yellow sun superpowers'
tags:
  - science fiction
  - science
  - comics
  - comic books
  - saving the world
  - superheroes
  - superman

# Episode particulars
image:
  path: assets/imgs/media/movies/man_of_steel_2013.jpg
  alt: Superman Man of Steel backdrop
links:
  - text: Superman (1978)
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/movie/superman/id272810065?at=1001l7hP&mt=6'
      - text: Amazon
        url: 'http://www.amazon.com/Superman-Movie-Marlon-Brando/dp/B0012QVJXS'
  - text: Fleisher 1940s Superman Cartoons
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=u1-PjY5tcOI'
  - text: All-Star-Superman
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/all-star-superman/id563134688?mt=11&at=1001l7hP'
      - text: Amazon
      - url: 'http://www.amazon.com/All-Star-Superman-Grant-Morrison-ebook/dp/B0064W66MO'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/man-of-steel-2013/id684580963?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'http://www.amazon.com/Man-Steel-Henry-Cavill/dp/B00GH4R95C'
  title: Man of Steel
  type: movie
  year: 2013
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/man_of_steel_2013.jpg
  number: 33
  guid: 'http://www.decipherscifi.com/?p=713'
  media:
    audio:
      audio/mpeg:
        content_length: 29160715
        duration: '48:35'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Man_of_Steel_decipherSciFi.mp3'
---
#### Krypton is Imploding


No refuge! Except for all the different refuge options. Harvesting the planet's core something something oh no it's collapsing!


#### Zod is the Good Guy


On Krypton at least. Sure, he sounded a little bit like Hitler, but look what happened when they didn't listen to him - no more Krypton! And then


#### Kryptonian Prison Rehab


They seem to put some serious resources into their criminal rehab system that barely gets used. Mundane black hole tech. Dildo butt plug space ships.


#### The Codex and the Genesis Chamber


Kal first natural birth in centuries. Clearly Kryptonians were multistellar once upon a time. Wonder what happened.


#### Super Awakening


Lessons in how _not_ to handle a kid having a panic attack. Humans actually do pretty well adapting to new sensory input. Clark's compensation and self control.


#### Yellow Sun Superpowers


Disagreement over the exact function of Kryptonian power armor and the nature of the yellow sun/superpower relationship.


#### Simulated Consciousness


Russel Crowe hologram simulation. So the Kryptonians reached the singularity basically, but don't seem to be impressed enough.


#### Defeating Zod


Super strength, super neck bones, super despondent military leaders with nothing to protect.

---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2016-06-14 07:20:41+00:00
description: 'With Serenity on the podcast we talk Firefly, languge evolution, cyberpunk vs scifi western, Joss Whedon, reaver origins, religion, lotsa callbacks, more'
layout: podcast_post
permalink: /decipherscifi/serenity-firefly-movie-podcast-episode-42
redirect_from:
  - /decipherscifi/42
slug: serenity-firefly-movie-podcast-episode-42
title: 'Senenity: Firefly, reavers, and space settlement sans FTL'
tags:
  - science fiction
  - science
  - space
  - western
  - space settlement
  - weapons
  - religion
  - language

# Episode particulars
image:
  path: assets/imgs/media/movies/serenity_2005.jpg
  alt: Serenity/Firefly crew movie backdrop
links:
  - text: Firefly
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/tv-season/firefly-the-complete-series/id151801083?at=1001l7hP&mt=4'
      - text: Amazon
      - url: 'https://www.amazon.com/Firefly-Season-1/dp/B004XUMMFE'
  - text: Serenity on Science Fiction Film Podcast
    urls:
      - text: LSG Media
        url: 'http://www.libertystreetgeek.net/serenity/'
  - text: Bad Character by Ted Chiang
    urls:
      - text: The New Yorker
        url: 'http://www.newyorker.com/magazine/2016/05/16/if-chinese-were-phonetic'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/serenity/id292625148?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'https://www.amazon.com/Serenity-Nathan-Fillion/dp/B001HBYHFA'
  title: Serenity
  type: movie
  year: 2005
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/serenity_2005.jpg
  number: 42
  guid: 'http://www.decipherscifi.com/?p=822'
  media:
    audio:
      audio/mpeg:
        content_length: 35076363
        duration: '41:45'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Serenity_--_--_Decipher_SciFi.mp3'
---
#### Joss Whedon & Firefly

[Johhhhhhn Cena prank call](https://www.youtube.com/watch?v=wRRsXxE1KVY). Fox's predilection for cancelling the most-loved shows. Viewing orders.

#### Hard and soft scifi

Joss Whedon doesn't care about the science. Star system design without FTL. A call-in from [Adrian Falcone]({% link _people/adrian_falcone.md %}).

#### Sci-Fi western

From cyberpunk to westerns. Frontier living.

#### Culture and language

Language evolution on generation ships. Creoles. Joss' poetic Rialogue.

#### Reavers

The mythology and the more reasonable backstory. Samuel Mumby calls in to express his appreciation for the former. "Who does the dishes?"

#### Religion

Sci-fi treatments of religion by Whedon and others.

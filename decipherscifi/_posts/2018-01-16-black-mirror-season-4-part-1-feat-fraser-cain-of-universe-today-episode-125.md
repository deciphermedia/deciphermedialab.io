---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - tv
date: 2018-01-16 07:45:37+00:00
description: 'Star Trek. Posthumous digital simulations and the optimal data sources. Parenting. The risk of the cloud, Computer vision and input filtering. Fallibility of human testimony. Memory crowdsourcing. Memory obstruction strategies. More!'
layout: podcast_post
permalink: /decipherscifi/black-mirror-season-4-part-1-feat-fraser-cain-of-universe-today-episode-125
redirect_from:
  - /decipherscifi/125
slug: black-mirror-season-4-part-1-feat-fraser-cain-of-universe-today-episode-125
title: 'Black Mirror Season 4 Part 1: AI control, surveillance, and lotsa murder w/ Fraser Cain'
tags:
  - artificial intelligence
  - brain-machine interface
  - cloud
  - computer vision
  - dna
  - memory
  - parenting
  - science
  - science fiction
  - star trek
  - virtual reality

# Episode particulars
image:
  path: assets/imgs/media/tv/black_mirror_2011_season_4_part_1.jpg
  alt: Black Mirror Season 4 three-episode backdrop collage
links:
  - text: "Fraser’s things"
    urls:
      - text: Universe Today
        url: 'https://www.universetoday.com'
      - text: Astronomy Cast
        url: 'http://www.astronomycast.com'
      - text: Fraser Cain @ YouTube
        url: 'https://www.youtube.com/user/universetoday'
media:
  links:
    - text: Netflix
      url: 'https://www.netflix.com/title/70264888'
  title: Black Mirror
  type: tv
  year: 2011
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: fraser_cain
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/tv/black_mirror_2011_season_4_part_1.jpg
  number: 125
  guid: 'https://decipherscifi.com/?p=4967'
  media:
    audio:
      audio/mpeg:
        content_length: 41719808
        duration: '49:39'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Black_Mirror_Season_4_Part_1_--_Fraser_Cain_--_decipherSciFi.mp3'
---
#### Episode 1 - USS Callister

The one with the Star Trek. Protecting the new kinds of consciousnesses we will eventually create. Sleeping through the AI apocalypse. What our solo video game habits say about us. Posthumous digital simulations. Data sources for creating a digital simulacrum and how DNA is one of the least useful. Designing your brain-machine interface to not possibly ever lock someone in ever.

#### Episode 2 - Arkangel

The one with the helicopter parenting. Mundane parenting concerns writ large in tech. Becoming luddites and finding your breaking point. But then you die and it doesn't matter. The danger of cloud services. Computer vision and visual input filtering.

#### Episode 3 - Crocodile

The one with the murders. Would you like a little more murder on your murder sandwich? The genius of the automatic, unmanned pizza truck. The pointlessness of human testimony. Crowdsourcing accurate memories. Memory obstruction strategies.

---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2018-06-19 07:45:43+00:00
description: 'Cyborg technologies. Remote control insect drones. Pain insensitivity as performance enhancement. The best places in your body to put holes (not the heart!)'
layout: podcast_post
permalink: /decipherscifi/universal-soldier-face-kicking-cyborg-technologies-and-micro-drone-insects
redirect_from:
  - /decipherscifi/147
slug: universal-soldier-face-kicking-cyborg-technologies-and-micro-drone-insects
title: 'Universal Soldier: face-kicking cyborg technologies and micro drone insects'
tags:
  - cyborgs
  - government conspiracies
  - implants
  - memory
  - pain
  - science
  - science fiction
  - thermoregulation
  - transhumanism

# Episode particulars
image:
  path: assets/imgs/media/movies/universal_soldier_1992.jpg
  alt: Jean-Claude with cyborg headgear and a 500-yard stare
links:
  - text: Universal Soldier alternate ending
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=x4SSdx1czfE'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/universal-soldier/id252616845?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Universal-Soldier-Dolph-Lundgren/dp/B000IHLG58'
  title: Universal Soldier
  type: movie
  year: 1992
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/universal_soldier_1992.jpg
  number: 147
  guid: 'https://decipherscifi.com/?p=6684'
  media:
    audio:
      audio/mpeg:
        content_length: 35995780
        duration: '42:50'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Universal_soldier_--_--_Decipher_SciFi.mp3'
---
#### Cyborgification

Remote-controlled cockroaches. Engineering cyborg cats with tape and servos. Search and rescue rats. What counts as a cyborg? Insects with eyebrows.

#### Non-cyborgification

Whirring servos and motors. Wearable technology and where to draw the line on cyborgs. The cyborg olympics. Determining the limits of human performance.

#### Thermoregulation

Mammalian heat regulation. Humans are the best distance runners AND car-pushers.

#### Pain

Pain response and learning. Pain and athletic performance. A PSA about injecting things int your heart - _you don't want a hole there_. The best places in your body to randomly put holes.

#### Cyborg naiveté

But what _is_ a penis anyway? Memory inhibition. Competitive eating. Binging and stomach elasticity. Competitive eating training.

#### Supersoldier Programs

Was it a success? Drugs make you a superhero.

---
layout: person_page
person_data:
  name: Miles Greb
  data_name: miles_greb
  image_path: assets/imgs/people/miles_greb.png
  title: Comic creator
  bio: 'The man behind a number of really special comic book and webcomic projects. His main book is the love letter to science *After the Gold Rush*, but please go check out all of his other projects as well! Don’t ask him about Star Wars. It’s dangerous! Like don’t-feed-them-after-midnight type dangerous.'
  websites:
    - name: Goldrush Comics
      url: 'http://www.afterthegoldrush.space/'
  social:
    - name: twitter
      url: https://www.twitter.com/goldrushcomic
    - name: instagram
      url: https://www.instagram.com/milesgreb
    - facebook: Afterthegoldrushcomic
  roles:
    - show: decipherscifi
      role: guest_cohost
---

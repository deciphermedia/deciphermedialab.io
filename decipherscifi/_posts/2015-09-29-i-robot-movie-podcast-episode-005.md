---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: '2015-09-29 03:45:32+00:00'
description: 'With I Robot we talk artificial intelligence, robots, the three laws, Isaac Asimov, robots with emotions, economics of robot workers, Google deep dreaming'
layout: podcast_post
permalink: /decipherscifi/i-robot-movie-podcast-episode-005
redirect_from:
  - /decipherscifi/5
slug: i-robot-movie-podcast-episode-005
title: 'I Robot: the three laws of robotics are inadequate'
tags:
  - asimov
  - consciousness
  - future
  - google
  - intelligence testing
  - man versus machine
  - robot
  - science fiction
  - the future of work
  - the three laws of robotics

# Episode particulars
image:
  path: assets/imgs/media/movies/i_robot_2004.jpg
  alt: 'I Robot Will Smith backdrop'
links:
  - text: Google Deep Dreaming 
    urls:
      - text: Blog post
        url: 'http://googleresearch.blogspot.com/2015/06/inceptionism-going-deeper-into-neural.html'
      - text: Make your own
        url: 'http://deepdreamgenerator.com/'
  - text: 'I, Robot by Isaac Asimov'
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/i-robot/id419950877?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'http://www.amazon.com/I-Robot-Isaac-Asimov/dp/055338256X'
  - text: 'Superintelligence: Paths, Dangers, Strategies - by Nick Bostrom'
    urls:
      -  text: iTunes
         url: 'https://geo.itunes.apple.com/us/book/superintelligence-paths-dangers/id899568056?mt=11&at=1001l7hP'
      -  text: Amazon
         url: 'http://www.amazon.com/Superintelligence-Dangers-Strategies-Nick-Bostrom-ebook/dp/B00LOOCGB2'
  - text: 'Our Final Invention - by James Barrat'
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/our-final-invention/id647685260?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'http://www.amazon.com/Our-Final-Invention-Artificial-Intelligence-ebook/dp/B00CQYAWRY'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/i-robot/id271873831?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'https://www.amazon.com/gp/video/detail/B003MXEGCK'
  title: 'I Robot'
  type: movie
  year: 2004
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: 
  number: 5
  guid: 'http://www.decipherscifi.com/?p=232'
  media:
    audio:
      audio/mpeg:
        content_length: 20507387
        duration: '34:10'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/I_Robot_decipherSciFi.mp3'
---
#### The Script Started Out Much Different

What began as a science fiction version of an Agatha Christie mystery grew into a Will Smith blockbuster. And then the studio acquired the rights to Asimov.

#### The Three Laws
	
  * Law Zero: Robots must not harm humanity.

	
  * Law One: Robots cannot harm a human.

	
  * Law Two: Robots must obey humans unless it conflicts with 1

	
  * Law Three: Rzobots must avoid harm to themselves except where it conflicts with 2 or 3

#### Reasoning Around the Three Laws

Wouldn't want any rogue states or terrorist groups getting their hands on a VIKI. VIKI reasons her way around the laws.

#### Tests For Intelligence

Art? Emotions? People always moving the goalposts. Domain-specific masteries.

#### Robot Workers

Taking our jerbs. [Amazing Amazon warehouse robots](https://www.youtube.com/watch?v=quWFjS3Ci7A). Teachers. Service industry. Creative work the most protected, for now. How to handle the fallout from the robot worker class? Guaranteed minimum income [experiment in Holland](http://thinkprogress.org/economy/2015/07/01/3676097/utrecht-universal-basic-income/).

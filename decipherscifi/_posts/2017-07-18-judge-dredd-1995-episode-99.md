---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2017-07-18 08:00:53+00:00
description: 'LAWWWWWWW and Judge Dredd. And robots and eugenic cloning and dystopia and aerodynamics and recycling food from waste and surviving the apocalypse and more!'
layout: podcast_post
permalink: /decipherscifi/judge-dredd-1995-episode-99
redirect_from:
  - /decipherscifi/99
slug: judge-dredd-1995-episode-99
title: 'Judge Dredd: combat robots, mega cities, and food recycling'
tags:
  - aerodynamics
  - cities
  - genetic engineering
  - incubation
  - oxygen cycle
  - robots
  - science
  - science fiction

# Episode particulars
image:
  path: assets/imgs/media/movies/judge_dredd_1995.jpg
  alt: Grumpy-Gus Judge Dredd stallone close-up movie backdrop
links:
  - text: You Betrayed the Law
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=glUf4PQ-vuU'
  - text: Dredd (2012)
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/movie/dredd/id570852344?mt=6&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Dredd-Karl-Urban/dp/B00ARF93AQ'
  - text: 'Demolition Man: cryogenic criminal justice and three seashells'
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/2'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/judge-dredd/id444656214?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Judge-Dredd-Sylvester-Stallone/dp/B004IDG97S'
  title: Judge Dredd
  type: movie
  year: 1995
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/judge_dredd_1995.jpg
  number: 99
  guid: 'https://decipherscifi.com/?p=2220'
  media:
    audio:
      audio/mpeg:
        content_length: 30279680
        duration: '36:02'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Judge_Dredd_--_--_decipherSciFi.mp3'
---
#### Mega Cities

Big. Tall. How they can be good, and how this one is bad.

#### Breaking the oxygen cycle

The environmental calamity in the movie that broke the earth is not detailed, but we try to consider [how long humanity has](https://scifi.stackexchange.com/questions/125877/how-do-people-breathe-in-the-dredd-universe/125890#125890) if it doesn't fix it somehow.

#### Recycled Food

Reclaimed food waste? OR... [reclaiming synthetic food from human waste](http://www.sciencealert.com/nasa-s-received-a-200-000-grant-to-turn-human-poop-into-food). Unjustified [psychological aversions](https://ww2.kqed.org/science/2016/10/06/how-california-is-learning-to-love-drinking-recycled-water/).

#### Ground Effect in aerodynamics

"In fixed-wing aircraft, ground effect is the increased lift (force) and decreased aerodynamic drag that an aircraft's wings generate when they are close to a fixed surface. When landing, ground effect can give the pilot the feeling that the aircraft is 'floating'." -Wikipedia

#### Warbots

Awesome bang-for-buck robot design. Chris adds "[Warbot head model](http://goldenarmor.com/abc-warrior/)" to his list of things he needs even more than air.

#### The Janus Project

The practical impossibility of incubating neurologically-adult humans. Babies in sacks and the benefits thereof.

_LAWWWWWWW_

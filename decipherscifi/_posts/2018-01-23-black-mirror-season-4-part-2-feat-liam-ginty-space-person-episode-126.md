---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - tv
date: 2018-01-23 06:22:08+00:00
description: 'Black Mirror season 4! Guest co-host Liam Ginty. The happy endings are the least happy ones. Hit the button. AI techniques. Boston Dynamics. Creepy sex doctor. Pain, pleasure, and salience. Pleasure buttons. Coma communication. Split brians. More!'
layout: podcast_post
permalink: /decipherscifi/black-mirror-season-4-part-2-feat-liam-ginty-space-person-episode-126
redirect_from:
  - /decipherscifi/126
slug: black-mirror-season-4-part-2-feat-liam-ginty-space-person-episode-126
title: 'Black Mirror Season 4 Part 2: dating, robots, and pain w/ Liam Ginty'
tags:
  - brain
  - drones
  - mind
  - pain
  - science
  - science fiction
  - sex

# Episode particulars
image:
  path: assets/imgs/media/tv/black_mirror_2011_season_4_part_2.jpg
  alt: Black Mirror Season 4 three-episode backdrop collage
links:
  - text: Voices from L5
    urls:
      - text: Voices From L5
        url: 'https://www.patreon.com/VoicesFromL5/posts'
  - text: Problems with Mind Uploading by exurb1a
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=APlw4xCl0xI'
  - text: You Are Two by CGP Grey
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=wfYbgdo8e-8'
  - text: Blindsight by Peter Watts
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/blindsight/id385975082?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Blindsight-Firefall-Peter-Watts-ebook/dp/B003K15EKM'
media:
  links:
    - text: Netflix
      url: 'https://www.netflix.com/title/70264888'
  title: Black Mirror
  type: tv
  year: 2011
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: liam_ginty
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/tv/black_mirror_2011_season_4_part_2.jpg
  number: 126
  guid: 'https://decipherscifi.com/?p=5035'
  media:
    audio:
      audio/mpeg:
        content_length: 45133824
        duration: '53:43'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Black_Mirror_Season_4_Part_2_--_Liam_Ginty_--_decipherSciFi.mp3'
---
#### Episode 4 - Hang the DJ

The one with the dating. Like [The button](https://www.youtube.com/watch?v=LJQ-LZYAMBQ), but instead of one person it's thousands. And instead of a million dollars you get a date. And still only maybe. Calculating Black Mirror horribleness for comparison between episodes. Massive data-crunching techniques.

{% youtube "https://www.youtube.com/watch?v=LJQ-LZYAMBQ" %}

#### Episode 5 - Metalhead

The one with the robot thing. Comparison with the new SpotMini.

{% youtube "https://www.youtube.com/watch?v=kgaO45SyaO4" %}

#### Episode 6 - Black Museum

The one with the museum. Weird sex stuff. Don't touch my nipples! The [Schmidt Pain Index](https://en.wikipedia.org/wiki/Schmidt_sting_pain_index). The value of actually doing and recording research. Black Mirror trashy romance novels. [Pleasure buttons](https://io9.gizmodo.com/whats-it-really-like-to-have-your-pleasure-center-stimu-1716449443). Sensory mapping. Consciousness partitioning. Liam was on Turn with this dude from this episode! Diffuse vs localized brain injury. [Communicating with locked-in patients](https://www.vox.com/2014/10/28/7078105/vegetative-state-coma-communicate-mind-reading-MRI-EEG-Owen). Split brains! [You Are Two](https://www.youtube.com/watch?v=wfYbgdo8e-8)! Waiting for law to catch up with dystopian technology.

{% responsive_image path: assets/imgs/misc/liam_turn.jpg caption: "Liam's appearance on &quot;Turn: Washington's Spies&quot;" alt: "Liam Ginty as he appeared on the AMC series &quot;Turn: Washington's Spies&quot;" attr_text: "AMC STudios ARR" %}

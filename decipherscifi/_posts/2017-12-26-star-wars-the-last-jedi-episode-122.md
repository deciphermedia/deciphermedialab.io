---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2017-12-26 07:45:49+00:00
description: 'The SCIENCE of Star Wars episode VIII. Explosive decompression, salt planets, hydrofoils and alasofoils, hyperspace telefragging, explosions in space, nipple testing, more!'
layout: podcast_post
permalink: /decipherscifi/star-wars-the-last-jedi-episode-122
redirect_from:
  - /decipherscifi/122
slug: star-wars-the-last-jedi-episode-122
title: 'The Last Jedi: FTL ramming, salt geology, and real green milk'
tags:
  - atmospheric pressure
  - explosions in space
  - ftl
  - geology
  - milk
  - science
  - science fiction
  - space combat
  - spaceships
  - star wars

# Episode particulars
image:
  path: assets/imgs/media/movies/star_wars_the_last_jedi_2017.jpg
  alt: A pile of Sgtar Wars heads
links:
  - text: 'Star Wars Meta: How to Star Wars, machete order, and the best Star Wars fan edit'
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/23'
  - text: 'Star Wars: the death star, planet science, and appreciating the original Star Wars trilogy'
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/24'
  - text: The Last Jedi - Science Fiction Film Podcast #220
    urls:
      - text: LSG Media
        url: 'https://www.libertystreetgeek.net/last-jedi/'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/star-wars-the-last-jedi/id1316280891?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Star-Wars-Last-Bonus-Content/dp/B0784YGPNS'
  title: "Star Wars: The Last Jedi"
  type: movie
  year: 2017
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/star_wars_the_last_jedi_2017.jpg
  number: 122
  guid: 'https://decipherscifi.com/?p=4829'
  media:
    audio:
      audio/mpeg:
        content_length: 35485696
        duration: '42:13'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Star_Wars_The_Last_Jedi_--_--_decipherSciFi.mp3'
---
#### Space combat

Naval analogies and a callback to a similar discussion in our [Valerian episode]({% link decipherscifi/_posts/2017-12-05-valerian-and-the-city-of-a-thousand-planets-episode-119.md %}). Magnetic (fused) torpedoes!

#### (FTL) Ramming speed!

Ramming vs telefragging. Kamikaze fleets, or attaching telefrag Warp Drives to asteroids and the like. Energy levels with ramming operations at lightspeed. Ruining Star Wars combat by introducing telfragging.

#### Explosions in space

Explosions in space shouldn't make noise, because there isn't (noise). Not across the near-vacuum, at least. Production of light and shrapnel. Starbursts!

#### Explosive decompression

Teleporting Colbert to his doom. For science! Multimodal attacks. Explosive decompression. Sausage people and asphyxiation. Freezing in space and how thew force disables all bets. Sausage people and elasticity of human tissues. Consciousness vs useful consciousness.

#### Skimmer ships

Hydrofoils cum "alasofoils." Flight engineering and "dragging sticks."

{% responsive_image path: assets/imgs/misc/hydrofoil_boat.jpg alt: "Hydrofoil boat" caption: "hydrofoil boat" attr_text: "U.S. Navy CC-0" attr_url: "https://commons.wikimedia.org/wiki/File:Carl_XCH-4.jpg" %}

#### Salt planet

"Salthoth." "Hey, it's salt."  How salt flats form on earth. Salt rising and drying from seasonal flooding. Salt-friendly bacteria and weird oxidation.

#### Green milk

Yum! Domesticating mammals for their milk. Mailing baboons. Milk's macronutrient ratios and the micronutrients that color it with the bulk removed e.g. for cheese). How it's kinda green irl too. Domesticating the aurochs and testing the nipples.

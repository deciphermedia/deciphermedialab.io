---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - qa
date: 2019-05-07 09:45:57+00:00
description: 'Guerilla space warfare. Saltwater toilets. Alkaline fermentation and smelly foods. Cute aggression. The call of the void. Playing Descent in the 90s. Etc.'
layout: podcast_post
permalink: /decipherscifi/questions-addenda-5-fermenting-your-pee-cute-aggression-and-centricities
redirect_from:
  - /decipherscifi/192
slug: questions-addenda-5-fermenting-your-pee-cute-aggression-and-centricities
title: 'Questions & Addenda #5: fermenting your pee, cute aggression, and centricities'
tags:
  - artificial intelligence
  - cyborg
  - economics
  - planetary centricities
  - science
  - science fiction
  - star wars
  - transhumanism
  - unicode
  - virtual reality
  - waste disposal

# Episode particulars
image:
  path: assets/imgs/media/misc/qa_05.jpg
  alt: 'Q&A word cloud'
links:
  - text: 'The Art of Fermentation by Sandor Katz'
    urls:
      - text: Amazon
        url: 'https://www.amazon.com/Art-Fermentation-Depth-Exploration-Essential/dp/160358286X'
  - text: 'Media used: Heavy Metal Riff by tgfcoder'
    urls:
      - text: 
        url: 'https://freesound.org/people/tgfcoder/sounds/114025/'
      - text: 'CC-BY'
        url: 'https://creativecommons.org/licenses/by/2.0/'
  - text: 'Media used:Retro Robot Thinking Sound by orginaljun'
    urls:
      - text: FreeSound
        url: 'https://freesound.org/people/orginaljun/sounds/402032/'
      - text: 'CC-BY'
        url: 'https://creativecommons.org/licenses/by/2.0/'
media:
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/misc/qa_05.jpg
  number: 192
  guid: 'https://decipherscifi.com/?p=10397'
  media:
    audio:
      audio/mpeg:
        content_length: 31092344
        duration: '37:00'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/qa_5--_--_decipher_scifi.mp3'

---
We got so much feedback over time from [supporter]({% link supporttheshow/index.md %}) Jeremy that we're dedicating a whole Questions & Addenda episode to covering them. Just like [that one time]({% link decipherscifi/_posts/2018-07-10-qa-3-glacial-surge-myth-vs-legend-and-underground-nukes.md %}).


#### Star Wars: The Last Jedi [[episode page]({% link decipherscifi/_posts/2017-12-26-star-wars-the-last-jedi-episode-122.md %})]

Guerilla space warfare. The Star Wars Geneva Convention. Chinese gun powder weaponization. Resources and impoetus in the development of killing technologies.

#### Ready Player One [[episode page]({% link decipherscifi/_posts/2018-07-31-ready-player-one-virtual-reality-haptics-digital-economies-and-optical-trickery-w-jolene-creighton.md %})]

Speed-of-light internet latency. EVE online strategies for fun and profit. "Sharding" as first-named in [Ultima](https://www.youtube.com/watch?v=KFNxJVTJleE) as a technical _and_ storytelling solution.

#### Wall-E [[episode page]({% link decipherscifi/_posts/2018-08-07-wall-e-biodegradable-plastics-waste-management-and-space-cattle.md %})]

Wasting potable water. Saltwater pipe corrosion. Graywater toilets. Fermenting your pee and eating rotten shark that smells like pee.

#### Upgrade [[episode page]({% link decipherscifi/_posts/2018-09-25-upgrade-biohacking-ai-containment-and-paralyzation-workarounds.md %})]

Automation in combat. "[Cute aggression](https://en.wikipedia.org/wiki/Cute_aggression)." The "[call of the void](https://en.wikipedia.org/wiki/Suicidal_ideation)," and the fear of the call of the void.

#### Doom [[episode page]({% link decipherscifi/_posts/2018-10-02-doom-moons-of-mars-space-marines-and-telefragging.md %})]

Could you really spoil Doom, though? Navigating by the ecliptic in space. Playing [Descent](https://www.youtube.com/watch?v=Ed_78HTkRbQ).

#### The Predator [[episode page]({% link decipherscifi/_posts/2019-01-08-the-predator-glistening-biceps-savant-syndrome-and-first-contact-w-josh-effengee.md %})]

Savantism before the invention and eventual ubiquity of writing.

#### 2036 Origin Unknown [[episode page]({% link decipherscifi/_posts/2019-01-22-2036-origin-unknown-ion-propulsion-dust-storms-and-how-to-speak-greek-in-orbit.md %})]

The wonder of centricities and the Greek pantheon. Using the wrong words for things. ASCII vs Unicode.

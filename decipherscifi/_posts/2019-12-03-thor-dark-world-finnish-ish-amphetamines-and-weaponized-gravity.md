---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2019-12-03 07:45:19+00:00
description: 'Dark World science! Bringing laser guns to a sword fight. Constructed languages. Viking trivia. Magic amphetamines. Dealing with magic. More'
layout: podcast_post
permalink: /decipherscifi/thor-dark-world-finnish-ish-amphetamines-and-weaponized-gravity
redirect_from:
  - /decipherscifi/219
slug: thor-dark-world-finnish-ish-amphetamines-and-weaponized-gravity
title: 'Thor Dark World: Finnish-ish, amphetamines, and weaponized gravity'
tags:
  - comics
  - constructed languages
  - drugs
  - language
  - magic
  - science
  - science fiction
  - space
  - vikings

# Episode particulars
image:
  path: assets/imgs/media/movies/thor_the_dark_world_2013.jpg
  alt: 'Piles of people and heads with Thor and Natalie Portman in front'
links:
  - text: 'Guardians of the Galaxy'
    urls:
      - text: 'Decipher SciFi'
        url: 'https://deciphermedia.tv/decipherscifi/89'
  - text: 'Guardians of the Galaxy 2'
    urls:
      - text: 'Decipher SciFi'
        url: 'https://deciphermedia.tv/decipherscifi/108'
  - text: 'Avengers Infinity War'
    urls:
      - text: 'Decipher SciFi'
        url: 'https://deciphermedia.tv/decipherscifi/142'
  - text: 'Black Panther'
    urls:
      - text: 'Decipher SciFi'
        url: 'https://deciphermedia.tv/decipherscifi/159'
  - text: 'Avengers Endgame'
    urls:
      - text: 'Decipher SciFi'
        url: 'https://deciphermedia.tv/decipherscifi/191'
media:
  links:
    - text: 'iTunes'
      url: 'https://geo.itunes.apple.com/us/movie/thor-the-dark-world/id731796731?mt=6&at=1001l7hP'
    - text: 'Amazon'
      url: 'https://www.amazon.com/Thor-Dark-World-Chris-Hemsworth/dp/B00IMYSVY8'
  title: 'Thor: The Dark World'
  type: movie
  year: 2013
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/thor_the_dark_world_2013.jpg
  number: 219
  guid: 'https://decipherscifi.com/?p=10875'
  media:
    audio:
      audio/mpeg:
        content_length: 25418319
        duration: '30:09'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/thor_the_dark_world.final.mp3'

---
#### Background

Looking back after the culmination of all these threads in [Endgame]({% link decipherscifi/_posts/2019-04-30-avengers-endgame-appreciating-the-enormity-of-the-mcu.md %}). Queen Amidala's midichlorian magical umbrella.

#### Language

Thor knowing All-Tongue, emitting pheromones, and doing the bee dance. The difficutly of universal language or language understanding. Finnishish conlanging. The cultural and linguistic juxtaposition between Scandinavia and Finland.

#### Alignment of worlds

Astrology is bunkum nonsense gobbledegook. The miniscule and generally unimportant effects when bodies in space are "in alignment."

#### "Magic"

Our difficulties with magical realism. Asgardian "Smagic." Apparent technology levels and technology so advanced it looks like magic. Bringing swords and shields to a lasergun fight.

#### Battle stimulation

Dosing for war. Uppers are very popular. The [Finnish soldier](https://en.wikipedia.org/wiki/Aimo_Koivunen) who was the "first documented case of a soldier over overdosing on methamphetamines during combat."

#### Black holes etc

"Gravity grenades." The possibility of man-made miniature black holes.

#### Vikings

"Viking burials" but not like you thought. [Tumuli/barrows/kurgans](https://en.wikipedia.org/wiki/Tumulus). Horned helmets.

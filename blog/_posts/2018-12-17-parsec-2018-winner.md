---
# General post frontmatter
author: christopher_peterson
categories:
  - awards
date: 2018-12-17T07:40:06+-05:00
layout: post
slug: parsec-2018-winner
title: '2018 Parsec Awards winners announced!' 
image:
  path: assets/imgs/misc/parsec_awards_2018_winner.jpg
  alt: 'Astronomy Cast 2018 Parsec Awards winner'

---
Hey the Parsec winners are up....

and we lost! 😞

but then ours friend won! 😀

congrats [Fraser Cain]({% link _people/fraser_cain.md %}) and Dr Pamela L Gay! <3 [http://www.astronomycast.com/](http://www.astronomycast.com/)

---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - conversation
date: 2020-09-22T02:43:27-04:00
description: 'Chicken and waffles. VR accessibility and AR convergence. Working from home on the toilet. Forest arachnic attack. Life on Venus maybe?!?!  Oceanic data centers. Etc.'
layout: podcast_post
redirect_from:
  - /decipherscifi/253
slug: stank-waffles-vr-development-venusian-cloud-datacenters
title: 'Stank waffles, VR development, and Venusian cloud datacenters'
tags:
  - augmented reality
  - computing
  - cooking
  - science
  - science fiction
  - space exploration
  - venus
  - virtual reality
  - waffles

# Episode particulars
image:
  path: assets/imgs/media/misc/isolated_bs_11_clouds_venus.jpg
  alt: Jar Brain and History Brain in the clouds of Venus
links:
  - text: 'Review: We do not recommend the $299 Oculus Quest 2 as your next VR system'
    urls:
      - text: Ars Technica
        url: 'https://arstechnica.com/gaming/2020/09/review-we-do-not-recommend-the-299-oculus-quest-2-as-your-next-vr-system/'
  - text: Oculus Quest 2
    urls:
      - text: Oculus.com
        url: 'https://www.oculus.com/blog/introducing-oculus-quest-2-the-next-generation-of-all-in-one-vr-gaming/'
  - text: 'Phosphine gas in the cloud decks of Venus'
    urls:
      - text: Nature Astronomy
        url: 'https://www.nature.com/articles/s41550-020-1174-4'
  - text: 'Chemical that shouldn’t be there spotted in Venus’ atmosphere'
    urls:
      - text: Ars Technica
        url: 'https://arstechnica.com/science/2020/09/unexpected-chemical-found-in-venus-upper-atmosphere/'
  - text: 'Microsoft declares its underwater data center test was a success'
    urls:
      - text: Ars Technica
        url: 'https://arstechnica.com/information-technology/2020/09/microsoft-declares-its-underwater-data-center-test-was-a-success/'
media:
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/misc/isolated_bs_11_clouds_venus.jpg
  number: 253
  guid: stank-waffles-vr-development-venusian-cloud-datacenters
  media:
    audio:
      audio/mpeg:
        content_length: 32424832
        duration: '38:33'
        explicit: false
        url: 'https://traffic.libsyn.com/secure/decipherscifi/bs11.final.mp3'

---
<sub><sup>
Clouds in episode image from "Storm Clouds" by Albert Bierstadt/The White House Historical Association <a href="https://commons.wikimedia.org/wiki/File:Albert_Bierstadt_-_Storm_Clouds_(c.1880).jpg">CC-O @ Wikipedia</a>
</sup></sub>

# Waffles

{% responsive_image_block %}
  path: 'assets/imgs/misc/belgian_waffle.jpg'
  alt: 'Belgian Waffle formula'
  caption: 'Belgian Waffle formula'
{% endresponsive_image_block %}

Tasty waffles! Stank waffles! With lots of syrup! Solving food problems with casseroles. *Gravy*!

{% youtube "https://www.youtube.com/watch?v=fm2mlT7q7mY" %}

# VR

Marveling at the Oculus Quest two 25% price drop from the last generation. Hating Facebook. Realizing the separateness of AR and VR development, even within companies working on both at the same time (Facebook). Can/will AR and VR converge and become ubiquitous?

# WFH

Working from home. Surprisingly long hikes and arachnid attacks. Tiny child legs and tiny child wills.

# Venus

Life on venus? The dense, deep, permanent nature of the Venusian cloud layer. The possible effect on drake equation. Looking forward to balloon probes in the Venusian atmosphere.

# Ocean data

Ocean-floor data centers. The cost of infrastructure and cooling vs the cost of real estate. The value of removing humans from the environment because we're so loud and clumsy and *moist*.

---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2017-09-12 08:24:49+00:00
description: 'The Time Machine! H.G. Wells and the awesome adaptability of his scifi. Time travel, paradoxes, human evolution, avoiding the apocalypse, more'
layout: podcast_post
permalink: /decipherscifi/the-time-machine-feat-joe-ruppel-episode-107
redirect_from:
  - /decipherscifi/107
slug: the-time-machine-feat-joe-ruppel-episode-107
title: 'The Time Machine: HG Wells, geological time scales, and time travel self-consistency w/ Joe Ruppel'
tags:
  - apocalypse
  - evolution
  - geologic time
  - hg wells
  - nuclear war
  - paradoxes
  - science
  - science fiction
  - time travel

# Episode particulars
image:
  path: assets/imgs/media/movies/the_time_machine_1960.jpg
  alt: 
links:
  - text: The Time Machine by H.G. Wells
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/the-time-machine/id434215319?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Time-Machine-Enriched-Classics-ebook/dp/B004XVQ73G'
  - text: The Time Machine (2002)
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/movie/the-time-machine/id643194944?mt=6&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Time-Machine-Guy-Pearce/dp/B074JKK68Z8'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/the-time-machine-1960/id302453009?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Time-Machine-Rod-Taylor/dp/B000LJAVUO'
  title: The Time Machine (1960)
  type: movie
  year: 1960
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: joe_ruppel
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/the_time_machine_1960.jpg
  number: 107
  guid: 'https://decipherscifi.com/?p=3247'
  media:
    audio:
      audio/mpeg:
        content_length: 45260800
        duration: '53:52'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/The_Time_Machine_--_Joe_Ruppel_--_decipherSciFi.mp3'
---
#### H.G. Wells

The book, his prescience, and his coinage of the term "time machine." His scientific literacy and the ideas in the physics and mathematics of his time. [Other]({% link decipherscifi/_posts/2016-03-29-war-of-the-worlds-joe-ruppel-movie-podcast-episode-31.md %}) [times]({% link decipherscifi/_posts/2016-11-01-island-of-dr-moreau-jeo-ruppel-movie-podcast-episode-62.md %}) he was on the show.

#### Moving through time

Dimensions and physics and moving through the invisible dimension of time.

#### Humanity and war

Time traveling between wars. The inevitability of global conflict in the late 19th century.  The inevitability of nuclear apocalypse in 1960. The inevitability of humanity overreaching in manipulation of the natural world in 2002. Really large explosions, nuclear and otherwise. [Krakatoa](https://en.wikipedia.org/wiki/Krakatoa) vs [Tsar Bomba](https://en.wikipedia.org/wiki/Tsar_Bomba). The [year without a summer]({% link decipherscifi/_posts/2016-10-04-bram-stoker-dracula-joe-ruppel-episode-58.md %}).

#### Geological scales

Time traveling across geological time scales. Tracking in relative space and over miles across the planet's surface. "Time and space machine." Staccato time travel.

#### Eloi and Morlocks

Human evolution and selective pressures. Humans as cattle. Inefficiency in turning sunlight into steak. Maintaining 19th-century English over millennia.

#### Time travel paradoxes

Single-timeline self-consistency. Time travel victory strategies.

#### Books

What books would you bring with you to repair future society?

Weenas

---
layout: person_page
person_data:
  name: 'Jillian & Meredith'
  data_name: jillian_meredith
  image_path: assets/imgs/people/jillian_meredith.png
  title: ''
  bio: 'They are attached at the hip as far as we can tell. This is why they get their own combined guest page here.'
  websites:
    - name: 'MerideathX @ Etsy'
      url: 'https://www.etsy.com/se-en/shop/MerideathX'
  roles:
    - show: decipherscifi
      role: guest_cohost
---

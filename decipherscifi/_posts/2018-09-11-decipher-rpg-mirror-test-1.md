---
# General post frontmatter
categories:
  - episode
  - rpg
date: 2018-09-11 07:45:43+00:00
description: "Emperor Colbert's coronation is interrupted by a sudden attack. His bodyguard Chris rushes him to the tunnels, but safety is uncertain!"
layout: podcast_post
permalink: /decipherscifi/decipher-rpg-mirror-test-1
redirect_from:
  - /decipherscifi/rpg1
slug: decipher-rpg-mirror-test-1
title: 'Decipher RPG - Mirror Pilot #1'
tags:
  - actual play
  - fantasy
  - rpg

# Episode particulars
image:
  path: assets/imgs/media/rpg/mirror01.jpg
  alt: 'Mirror RPG character illustrations for episode 1 of 3'
links:
media:
  links:
    - text: Itch.io
      url: 'https://sandypuggames.itch.io/mirror-a-micro-rpg'
  title: Mirror
  type: rpg
  year: 2018
people:
  - data_name: christopher_peterson
    roles:
      - player
  - data_name: lee_colbert
    roles:
      - player
  - data_name: liam_ginty
    roles:
      - game_master
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/rpg/mirror01.jpg
  guid: 'https://decipherscifi.com/?p=7672'
  media:
    audio:
      audio/mpeg:
        content_length: 46358528
        duration: '55:07'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Decipher_RPG_-_mirror_-_01.mp3'
---
This podcast is the first of three episodes of our pilot series for Decipher RPG where we play [Mirror](http://www.sandypuggames.com/sandy-pug-orginals/) by Sandy Pug Games with [Liam Ginty]({% link _people/liam_ginty.md %}). Here is [part 2]({% link decipherscifi/_posts/2018-09-12-decipher-rpg-mirror-pilot-2.md %})!

After some particular fun with character creation, Emperor Colbert's coronation is interrupted by a sudden attack. With RPG-Colbert still entranced in his ceremonial mask, his bodyguard Chris rushes him to safety, but... airships!

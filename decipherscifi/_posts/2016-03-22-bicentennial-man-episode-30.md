---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2016-03-22 08:45:48+00:00
description: 'With Bicentennial Man we talk emergence sentience, the invisible robot slave class, skinjobs and passing as human. Turing tests. Isaac Asimov and his laws.'
layout: podcast_post
permalink: /decipherscifi/bicentennial-man-episode-30
redirect_from:
  - /decipherscifi/30
slug: bicentennial-man-episode-30
title: 'Bicentennial Man: laws of robotics, biorobots, and natural language processing'
tags:
  - science fiction
  - science
  - love
  - robot
  - ai takeoff
  - cyborg

# Episode particulars
image:
  path: assets/imgs/media/movies/bicentennial_man_1999.jpg
  alt: Bicentennial Man Robin Williams movie backdrop
links:
  - text: Positronic Man by Isaac Asmiov
    urls:
      - text: Amazon
        url: 'http://www.amazon.com/Positronic-Man-Isaac-Asimov/dp/0385263422/ref=sr_1_1?ie=UTF8&qid=1458447214&sr=8-1&keywords=positronic+man'
  - text: Humans
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/tv-season/humans/id1006961139?at=1001l7hP&mt=4'
      - text: Amazon
        url: 'http://www.amazon.com/Episode-1/dp/B010O44BSS'
  - text: Robin Williams in Motion by Every Frame a Painting
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=I8lQKLjmoWI'
  - text: Uncertainty Principle Podcast by Daniel James Barker
    urls:
      - text: Soundcloud
        url: 'https://soundcloud.com/uncertaintyprinciple'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/bicentennial-man/id188705997?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'http://www.amazon.com/Bicentennial-Man-Robin-Williams/dp/B004IEIO66'
  title: 
  type: movie
  year: 
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: 
  number: 30
  guid: 'http://www.decipherscifi.com/?p=679'
  media:
    audio:
      audio/mpeg:
        content_length: 22550694
        duration: '37:34'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Bicentennial_Man_decipherSciFi.mp3'
---
#### Asimov's Three Laws

Enumerated in spectacular fashion in the film in order to get it out of the way and never speak of it again.

#### Natural Language Processing

It's really hard! Errors in humor and social expectations. Infinite conversational loops.

#### Robot Personification

A nurturing environment maybe contributed to Robo Williams' sentience. Art as awakening. Acting human. Emergence.

#### Robot Slave Class

Why don't we see them?

#### Uncanney Valley

Turing tests. Robots, androids, biorobots. Robo Williams' synthetic organs.

#### What is Life?

Actually kind of hard to define.

#### Immortality

"Death is an engineering problem."

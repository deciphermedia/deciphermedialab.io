---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: '2015-11-03 04:45:54+00:00'
description: 'With Lost in Space on the podcast we cover climate disaster, interstellar travel, colonizing other worlds, gifted children, great men being terrible fathers'
layout: podcast_post
permalink: /decipherscifi/lost-in-space-episode-010
redirect_from:
  - /decipherscifi/10
slug: lost-in-space-episode-010
title: 'Lost in Space: gifted children, future vlogging, and colonizing space'
tags:
  - climate
  - cryo sleep
  - family
  - future
  - gravity
  - intelligence
  - science fiction
  - space
  - space settlement
  - stargate
  - time travel
  - volumetric projections

# Episode particulars
image:
  path: assets/imgs/media/movies/lost_in_space_1998.jpg
  alt: 'Lost in Space movie backdrop 1998'
links:
  - text: 'The Newsroom - climate change interview'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=M1cMnM-UJ5U'
  - text: Lost in Space (the original television series)
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/tv-season/lost-in-space-season-1/id1034534739?at=1001l7hP&mt=4'
      - text: Amazon
        url: 'https://www.amazon.com/Reluctant-Stowaway-The/dp/B000H3VBEE'
  - text: Revelation Space - by Alastair Reynolds
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/revelation-space/id357921849?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'http://www.amazon.com/Revelation-Space-Alastair-Reynolds-ebook/dp/B001QL5MAA'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/lost-in-space/id311321383?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'https://www.amazon.com/Lost-Space-William-Hurt/dp/B002PYW26Y'
  title: Lost in Space
  type: movie
  year: 1998
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: 
  number: 10
  guid: 'http://www.decipherscifi.com/?p=314'
  media:
    audio:
      audio/mpeg:
        content_length: 36256983
        duration: '50:21'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Lost_in_Space_decipherSciFi.mp3'
---
#### Climate Disaster

Colbert talks about it a bunch. Chris goes all Pollyanna. The Newsroom Climate episode (embedded above)

#### Traveling From Earth

Stargates. Gravity wells. Luck they didn't wind up inside a sun.

#### Space Colonies

Space X! Mars One! Christopher gushes about Elon Musk for thirty minutes.

#### Gifted Children And Rubbish Fathers

Will Robinson. Ahmed Mohamed.  Steve Jobs and Steve Wozniak. Ghengis Khan? Elon Musk?

#### Future Tech

Cryo sleep. Holographic communications. Vlogging via watch.

#### Video Communications and Holography

[youtube]Y-P1zZAcPuw[/youtube]

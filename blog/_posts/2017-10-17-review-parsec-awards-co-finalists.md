---
# General post frontmatter
author: christopher_peterson
categories:
  - awards
date: 2017-10-17 13:46:28+00:00 
layout: post
slug: review-parsec-awards-co-finalists 
title: 'A review of our 2017 Parsec Awards co-finalists' 

---
[As we mentioned]({% link blog/_posts/2017-10-17-parsec-2017-finalists.md %}), we're very happy to be 2017 Parsec Awards finalists. We also mentioned how happy we were to be lumped in with the other really impressive shows in the category. So here is a conversion of my reviews of them from [Twitter](http://twitter.com/decipherscifi). Basically, you're observing in real-time my realization of all our inadequacies in the face of their talents.

### [Meta Treks](http://trek.fm/meta-treks/)


Meta Treks by [**Zachary Fruhling**](https://twitter.com/zacharyfruhling) and [**C. Michael Morrison**](https://twitter.com/cmichael1701) is a great combo of scifi and philosophy! It makes our own philosophical scifi incompetence that much more glaring.

You can check it out at [Trek.fm](http://trek.fm/meta-treks/).

### [Talk Nerdy](http://carasantamaria.com/podcast/)

Talk Nerdy is [Cara Santa Maria](https://twitter.com/CaraSantaMaria)'s science interview podcast. Great people on great topics and she is an excellent science communicator. I was already a fan of hers from [The Skeptics Guide](http://www.theskepticsguide.org) so it was easy to add this to my list.

You can check it out at [here](http://carasantamaria.com/podcast/).

### [Guide to Space by universe today](https://www.youtube.com/playlist?list=PLbJ42wpShvml6Eg22WjWAR-6QUufHFh2v)

Fans of the show will no doubt already be familiar with [Fraser Cain](https://decipherscifi.com/about/fraser-cain-space-person/), Decipher SciFi guest-cohost and all-around prolific space person. He's a role-model for how to be awesome in media on the internet and I hope to be half as good one day.

You can check out Guide to Space in either [audio](https://t.co/9FFtgobSbj) or [video](https://www.youtube.com/playlist?list=PLbJ42wpShvml6Eg22WjWAR-6QUufHFh2v) form.

### [Planetary radio](http://planetary.org/multimedia/planetary-radio/)

This show from the Planetary Society has a great host in [Mat Kaplan](https://twitter.com/PlanRad) and great content and guests via its association with the Planetary Society. The planetary society, btw, is a really worthwhile organization  that you might consider also supporting.

You can check it out [here](http://planetary.org/multimedia/planetary-radio/).

<br>
<br>
So in the end I'm not even sure how we are in a category with shows of this calibre, but it's an incredibly satisfying indication that what we're doing works. Thanks so much!

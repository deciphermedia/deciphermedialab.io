---
layout: person_page
person_data:
  name: Caelum Rale
  data_name: caelum_rale
  image_path: assets/imgs/people/caelum_rale.png
  title: Artist and Musician
  bio: 'Makes art and music. He prefers to work in the shadows, though we drag him out into the light now and again.'
  websites:
    - name: 'Cae @ Instagram'
      url: 'https://www.instagram.com/inclosedspaces/'
  social:
    - name: instagram
      url: https://www.instagram.com/inclosedspaces
  roles:
    - show: decipherscifi
      role: guest_cohost
---

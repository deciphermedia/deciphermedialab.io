---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2016-11-22 07:40:05+00:00
description: 'Author Eliot Peper helps us tackle Minority Report. Machine learning and AI influencing policing, advertising, policy. Civil liberties. Thought crimes. more' 
layout: podcast_post
permalink: /decipherscifi/minority-report-eliot-peper-author-episode-65
redirect_from:
  - /decipherscifi/65
slug: minority-report-eliot-peper-author-episode-65 
title: 'Minority Report: Eliot Peper on freedom and algorithmic precognition' 
tags:
  - science fiction
  - science
  - social justice
  - artificial intelligence
  - police state
  - dystopia
  - utopia
  - surveillance
  - philip k dick
  - futurism

# Episode particulars
image:
  path: assets/imgs/media/movies/minority_report_2002.jpg
  alt: Tom Cruise using a gestural interface Minority Report backdrop
links:
  - text: Cumulus by Eliot Peper
    urls:
      - text: Amazon
        url: 'https://www.amazon.com/Cumulus-Eliot-Peper-ebook/dp/B01E4L5L6S'
  - text: Make it So by Chris Noessel
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/make-it-so/id564005891?mt=11'
      - text: Amazon
        url: 'https://www.amazon.com/dp/B009EGPJCU'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/minority-report/id573718569?mt=6'
    - text: Amazon
      url: 'https://www.amazon.com/Minority-Report-Tom-Cruise/dp/B00A2FSXHK'
  title: Minority Report
  type: movie
  year: 2002
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: eliot_peper
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/minority_report_2002.jpg
  number: 65
  guid: 'http://www.decipherscifi.com/?p=1145'
  media:
    audio:
      audio/mpeg:
        content_length: 45493837
        duration: '01:15:49'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Minority_Report_Eliot_Peper_decipherSciFi.mp3'
---

{% responsive_image path: assets/imgs/selfies/minority_report.jpg alt: "Decipher SciFi hosts re-enacting minority report" %}

#### Futurists

Eliot shares how Kevin Kelly, Stuart Brand, et al advised the filmmakers on how the future ought to look. The movie's prescience makes a lot more sense now.

#### Privacy Intrusion

Intrusion creep. Giving up your privacy knowingly. Testing users.

#### Thought Crimes

Is it a thought crime? Or just determinism?

#### Big Data/Precogs

Artificial intelligence. Machine learning. Policing, advertising. Oakland and other cities.

#### Utopia/Dystopia

The rather rosy picture painted by Minority Report and its realism. How it seems more reasonable now than it may have at the time.

#### Gestural Interface

Feedback cycle between our fiction and real life.

#### Dismantling The System

Could such a system be dismantled! It's hard to just part with a murder rate of zero. Eliminating murder vs civil liberties.

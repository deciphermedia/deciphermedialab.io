---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - tv
date: 2020-06-23T02:45:20-04:00
description: 'The Tulsa Massacre. Graphic novel/film adaptations. Super-genius narcissism. Chemical-based VR Nostalgia. The Dr Manhattan process. Etc.'
layout: podcast_post
redirect_from:
  - /decipherscifi/243
slug: watchmen-tulsa-nostalgia-victorian-spacesuits
title: 'Watchmen - Tulsa, nostalgia, and victorian spacesuits'
tags:
  - history
  - science
  - science fiction
  - space travel
  - time travel

# Episode particulars
image:
  path: assets/imgs/media/tv/watchmen_2019.jpg
  alt: 'Sister Night in front of a yellow clockface, Watchmen backdrop'
links:
  - text: 'Watchmen - Adapting The Unadaptable by kaptainkristian'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=5oltd-Jsi2I'
  - text: 'Watchmen (2009 Film)'
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/movie/watchmen/id308856387?mt=6&at=1001l7hP'
      - text: Prime Video
        url: 'https://www.amazon.com/Watchmen-Malin-Akerman/dp/B0028R765A'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/tv-season/watchmen-season-1/id1483959881?mt=4'
    - text: Prime Video
      url: 'https://www.amazon.com/An-Almost-Religious-Awe/dp/B07RM7Y5W5'
  title: Watchmen
  type: tv
  year: 2019
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/tv/watchmen_2019.jpg
  number: 243
  guid: watchmen-tulsa-nostalgia-victorian-spacesuits
  media:
    audio:
      audio/mpeg:
        content_length: 29628200
        duration: '35:13'
        explicit: false
        url: 'https://traffic.libsyn.com/secure/decipherscifi/watchmen.final.mp3'

---
# Tulsa Massacre

The [Black Wall Street Massacre](https://en.wikipedia.org/wiki/Tulsa_race_massacre). [Detecting buried remains with ground-penetrating radar](https://www.ncjrs.gov/pdffiles1/nij/grants/238275.pdf).

# Ozymandias

Doing splodey blue-balls on a city vs dropping a squid. "Saving" humanity. Narcissism. Super-genius space-prison escape plans. Spaceships vs submarines. Catapults, trebuchets, and escape velocity.

# Dr Manhattan

Ol' bluey. Creating life. Exploring the universe when you can be everywhere and everywhen at once. How and why would anyone want to blue themselves this way? The Dr Manhattan formula from human to complete lack of concern for humanity.

# Nostalgia

Chemical VR. An alternative route to Alzheimer's treatment.

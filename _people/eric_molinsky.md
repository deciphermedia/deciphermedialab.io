---
layout: person_page
person_data:
  name: Eric Molinsky
  data_name: eric_molinsky
  image_path: assets/imgs/people/eric_molinsky.png
  title: Radio Producer
  bio: 'Produces great audio. Shows where you might have heard his work include 99% Invisible, Studio 360, and a bunch of NPR shows'
  websites:
    - name: Eric Monlinsky
      url: 'https://www.ericmolinsky.com/'
    - name: Imaginary Worlds
      url: 'https://www.imaginaryworldspodcast.org/'
  social:
    - name: twitter
      url: https://www.twitter.com/emolinsky
    - name: instagram
      url: https://www.instagram.com/ericmolinsky
  roles:
    - show: decipherscifi
      role: guest_cohost
---

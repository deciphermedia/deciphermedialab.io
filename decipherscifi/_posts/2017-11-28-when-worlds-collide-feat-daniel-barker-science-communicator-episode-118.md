---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2017-11-28 07:45:58+00:00
description: "When Worlds Collide brings our disaster month to spaaaace. How to jump to a new planet, who to bring with you, and how you'll die anyway, Galaxies & stars"
layout: podcast_post
permalink: /decipherscifi/when-worlds-collide-feat-daniel-barker-science-communicator-episode-118
redirect_from:
  - /decipherscifi/118
slug: when-worlds-collide-feat-daniel-barker-science-communicator-episode-118
title: 'When Worlds Collide: observable universe, 20th century astonomy, and modern space industry w/ Daniel James Barker'
tags:
  - planets
  - rockets
  - science
  - science fiction
  - space
  - space industry
  - space settlement
  - stars
  - universe
  - uranus

# Episode particulars
image:
  path: assets/imgs/media/movies/when_worlds_collide_1951.jpg
  alt: 1950-s style painting of people running away from an exploding city
links:
  - text: When Worlds Collide by Powerman 5000
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=lsV500W4BHU'
  - text: 2,000,000,000,000
    urls:
      - text: Uncertainty Principle the Podcast
        url: 'https://soundcloud.com/uncertaintyprinciple/2000000000000a'
  - text: Hubble Deep Field by Vox
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=95Tc0Rk2cNg'
  - text: And another about the Hubble "Ultra" Deep Field by Deep Astronomy
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=oAVjF_7ensg'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/when-worlds-collide/id276048393?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/When-Worlds-Collide-Richard-Derr/dp/B00170I7LG'
  title: When Worlds Collide
  type: movie
  year: 1951
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: daniel_barker
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/when_worlds_collide_1951.jpg
  number: 118
  guid: 'https://decipherscifi.com/?p=4384'
  media:
    audio:
      audio/mpeg:
        content_length: 31543296
        duration: '37:32'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/When_Worlds_Collide_--_Daniel_James_Barker_--_decipherSciFi.mp3'
---
#### The "observable" universe

The part of the universe that we can, in principle, capture light from as a function of the speed of light and the expansion of the universe.

#### Galactic perspectives

Learning more about the breadth and contents of the universe since the 1950s. Hubble and "[island universes](https://en.wikipedia.org/wiki/Galaxy#Etymology)." Figuring out there are other galaxies and past and present ideas of just how many there are.

#### The modern space industry

Appreciation for where we are finally. Space travel becoming cheaper and cheaper and the legit settlement of other planets on the horizon.

#### When worlds actually collide

Spitzer detecting the aftermath of what seems to have been a planetary collision. Other planetary collisions in the history of our own solar system, like the one that produced our moon.

{% responsive_image path: assets/imgs/misc/planertary_collision_artists_concept.jpg alt: "Artist's conception of a collision around the star NGC 2547-ID8" caption: "Artist's conception of a collision around the star NGC 2547-ID8" attr_text: "NASA/JPL-Caltech" attr_url: "https://www.nasa.gov/jpl/spitzer/pia18469" %}

#### The incoming star: Bellus

Only 12x the size of earth. Luminosity and distance. What type of star could be in this range of size and luminosity?

{% responsive_image path: assets/imgs/misc/hertzsprung_russel_stardata.jpg alt: "Herzprung-Russel Star Diagram" caption: "Herzprung-Russel Star Diagram of star surface temperature and luminosity" attr_text: "ESO CC-A-4.0" attr_url: "https://www.eso.org/public/images/eso0728c/" %}

#### Uranus

Somehow we got to this topic. And now here is some history!

{% youtube "https://www.youtube.com/watch?v=h3ppbbYXMxE" %}

#### Effect on the solar system

Oh no! Will the planet be able to find a stable orbit in our solar system after presumably affecting the fairly stable orbits of the other planets? Star system escape velocities.

#### Landing on a new world

And not even having time to remark upon the clearly engineered structures on the surface before your whole planet is flung from its sun and everything freezes and you die. :cry:

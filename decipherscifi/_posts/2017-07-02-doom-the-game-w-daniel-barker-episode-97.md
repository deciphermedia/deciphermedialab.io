---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - video game
date: 2017-07-02 01:18:33+00:00
description: 'Our first video game episode. Nostalgia for Doom 1993. Running Doom on all technology. 3d holograms versus AR. AI. Cyborg geniuses. Rip and Tear!'
layout: podcast_post
permalink: /decipherscifi/doom-the-game-w-daniel-barker-episode-97
redirect_from:
  - /decipherscifi/97
slug: doom-the-game-w-daniel-barker-episode-97
title: 'Doom: PC games nostalgia, intelligent supercomputers, and interdimensional beings w/ Daniel James Barker'
tags:
  - artificial intelligence
  - body transplants
  - doom
  - hell
  - nostalgia
  - science
  - science fiction
  - video games

# Episode particulars
image:
  path: assets/imgs/media/videogames/doom_2016.jpg
  alt: Doom Guy kicking demon butts, Doom game backdrop
links:
  - text: Doom (The original game)
    urls:
      - text: GoG
        url: 'https://www.gog.com/game/the_ultimate_doom'
      - text: Steam
        url: 'http://store.steampowered.com/sub/18397/'
      - text: Android
        url: 'http://www.beloko.com/?page=game_doom'
  - text: Doom 2016 Soundtrack
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=Jm932Sqwf5E'
  - text: Masters of Doom: How Two Guys Created an Empire and Transformed Pop Culture by David Kushner
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/audiobook/masters-doom-how-two-guys-created-empire-transformed/id544540217?mt=3&app=itunes&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Masters-Doom-Created-Transformed-Culture/dp/B008KGXM6A'
media:
  links:
    - text: Steam
      url: 'http://store.steampowered.com/app/379720/DOOM/'
    - text: Amazon
      url: 'https://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=doom%202016'
  title: Doom
  type: video_game
  year: 2016
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: daniel_barker
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/videogames/doom_2016.jpg
  number: 97
  guid: 'http://www.decipherscifi.com/?p=1617'
  media:
    audio:
      audio/mpeg:
        content_length: 30414848
        duration: '36:11'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Doom_2016_video_game_--_Daniel_James_Barker_--_decipherSciFi.mp3'
---
#### Doom

Doom was a really awesome important thing in 1993. History lessons because Dan is too young to understand but we are old. Nostalgia, for better or for worse. Modularity and the development of game modding. How to play a modern game without playing it.

#### The true test of technology

The true test of any technology: can it run Doom? Doom [running on a Macbook Pro Touch Bar](https://www.youtube.com/watch?v=GD0L46y3IqI]).

#### Volumetric Projections

"3D holograms." As a narrative/historical record playback device. As seen in: [Prometheus]({% link decipherscifi/_posts/2017-05-16-prometheus-episode-90.md %}). Difficulties versus augmented reality.

#### Health

Measuring "life" as a life meter. Micromorts and actuarial considerations. Safety gear in health packs.

#### Hell Universe

Tapping into unlimited energy; who could resist?

#### Vega the supercomputer

Turing tests. Regrets. "Absolute zero" cooling systems and marketing speak. Backups analagous to DNA cloning.

#### Samuel Hayden

Body transplants vs brain transplants. Where does the self reside? Cyborg geniuses. Life goals.

RIP AND TEAR

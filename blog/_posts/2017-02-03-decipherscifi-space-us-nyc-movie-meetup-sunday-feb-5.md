---
author: christopher_peterson
categories:
  - irl
date: 2017-02-03 19:55:40+00:00
layout: post
slug: decipherscifi-space-us-nyc-movie-meetup-sunday-feb-5
title: 'Decipher SciFi The Space Between Us NYC movie meetup (Sunday, Feb 5)'
image:
  path: assets/imgs/misc/meetup_space_between_us.jpg
  alt: "The Space Between Us NYC movie meetup announcement image"

---

Hey, people! Remember our [last meetup]({% link blog/_posts/2016-11-14-arrival-meetup-nyc.md %})? That was fun!

Our friend [Chris Noessel]({% link _people/chris_noessel.md %}) is in town this weekend, so we're setting up something real quick to get together and watch a movie with MARS and SPACE and love or something: [The Space Between Us](http://www.imdb.com/title/tt3922818/?ref_=fn_al_tt_1)!

Wanna come? Please go to the [Facebook event page](https://www.facebook.com/events/1302970839741111/) and confirm your attendence if you're in the area. There will be a good handful of Decipher SciFi-type people there, and we'll go drink and eat afterwards.

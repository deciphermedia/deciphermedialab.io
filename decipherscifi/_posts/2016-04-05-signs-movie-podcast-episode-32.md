---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2016-04-05 08:00:20+00:00
description: "Shyamalan's Signs! Alien invasion, crop circle hoaxes, cognitive biases, a deterministic universe, and the very best explanation for the film."
layout: podcast_post
permalink: /decipherscifi/signs-movie-podcast-episode-32
redirect_from:
  - /decipherscifi/32
slug: signs-movie-podcast-episode-32
title: 'Signs: the ending, religious determinism, and how not to invade a watery planet'
tags:
  - science fiction
  - science
  - aliens
  - alien invasion
  - religion
  - crop circles

# Episode particulars
image:
  path: assets/imgs/media/movies/signs_2002.jpg
  alt: Signs Shiyamalan movie backdrop
links:
  - text: The Signs Demon Theory by ZorroMeansFox
    urls:
      - text: Reddit
        url: 'https://www.reddit.com/r/AskReddit/comments/ubaqq/what_fan_theories_have_blown_your_mind_with_their/c4ubvmy'
  - text: "Joaquin Phoenix's Forehead Face"
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=Q9UDVyUzJ1g'
  - text: You Are Not So Smart
    urls:
      - text: The Podcast
        url: 'http://youarenotsosmart.com/podcast/'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/signs/id188765169?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'http://www.amazon.com/Signs-Mel-Gibson/dp/B0060D18UM'
  title: Signs
  type: movie
  year: 2002
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: 
  number: 32
  guid: 'http://www.decipherscifi.com/?p=707'
  media:
    audio:
      audio/mpeg:
        content_length: 24288871
        duration: '40:28'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Signs_decipherSciFi.mp3'
---
#### Shyamalan

Christopher loved his direction in this movie. "Twists."  Appears in all his own films.

#### Crop Circles

Crop circle hoaxes. Not a great method of communication.

#### Two Groups of People

People who think there is meaning, and people think the universe is deterministic and unconcerned with us as individuals. Benjamin Franklin's pragmatism re religion. Our own perspective.

#### The Aliens

A list of unreasonable presumptions from the movie about alien life. Recon forces. Encryption. Spoilers.

---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2020-09-01T02:20:19-04:00
description: 'Making judgements. Pre-WW1 classism. Intercontinental travel in the the age of the steamship ocean-liner. Inquiries and the contemporary picture of the causes of the disaster.'
layout: podcast_post
redirect_from:
  - /decipherhistory/9
slug: titanic-rich-people-bad-luck-ever-increasing-hugeness
title: 'Titanic: rich people, bad luck, and ever-increasing hugenes w/ Joe'
tags:
  - capitalism
  - classism
  - engineering
  - history
  - ship construction
  - travel

# Episode particulars
image:
  path: assets/imgs/media/movies/titanic_1997.jpg
  alt:  'Jack and Rose and a boat'
links:
  - text: '[4K,60Fps,Colorized] Titanic, First and Last Voyage, April 1912 AI Recovery, added sound'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=SNbfmRbA0c8'
  - text: 'RMS Titanic Survivors True Accounts of The Sinking'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=Zfn1ZQFJcHw'
  - text: 'RMS Titanic: Fascinating Engineering Facts by the Engingeering Guy'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=fHmgF4ibmuk'
  - text: 'Titanic - Alternate Ending'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=9uXa1R2e4a8'
media:
  links:
    - text: iTunes
      url: ''
    - text: Amazon
      url: ''
  title: Titanic
  type: movie
  year: 1997
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: joe_ruppel
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/titanic_1997.jpg
  number: 9
  guid: titanic-rich-people-bad-luck-ever-increasing-hugeness
  media:
    audio:
      audio/mpeg:
        content_length: 83956564
        duration: '1:39:51'
        explicit: false
        url: 'https://traffic.libsyn.com/secure/decipherscifi/titanic.final.mp3'

---
# Production

Costly! The costs of vising the wreck *twelve times* for research and footage, not to mention the cost of the film production overall. The extreme profitability of this film and James Cameron in general.

# Classism

First, second, and third class on the same ship. Titans of industry. Runaway capitalism. The class divide and point of view before The Great War. The modern "royalty" by virtue of their wealth.

# Technology

The pace of technological development in industries, especially *intercontinental travel* by steamship. Comparing the early 1900s to other periods of technological progress like the 1980s and 1990. Noting the nature of the automobiles in the film: literally the design of a "horseless carriage." The cutting-edge wireless technology aboard the ship.

# The ship

Three engines! Of two varieties. Steam-driven piston engines and steam-driven a turbine. Note: *you cannot reverse a turbine*. Peak power: 46,000 horsepower. 600 tons of coal *daily*. An anecdote about Mr. Diesel.

# A collection of small problems

The nature of many disasters, probably including Titanic: a series of small problems, adding up to a catastrophic failure under the rigt (wrong) circumstances. Rich people hogging the Marconi wireless. Out of date lifeboat regulations. Loading lifeboats (badly). The reality of "women and children first." Avoiding chaos until it's too late.

# Judgements

The inquiries from shortly after the event, and the great surprise you may experience when realizing that the company men consistently pushed blame back up the chain of command. Modern evidence changing our conception over time. Actually ultimately coming to the consensus that this particular ship on this particular voyage was mostly operated in a way that was totally normal in almost all respects and simply got *very* unlucky.

> All told, it does seem that Titanic was really really *well*-engineered and did remarkably well under the circumstances.

---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: '2015-09-15 05:01:33+00:00'
description: 'For this episode of the podcast, come watch Titan AE with us and then discuss the most interesting science fiction themes and idea from the film.'
layout: podcast_post
permalink: /decipherscifi/titan_ae_movie_podcast_episode_001
redirect_from:
  - /decipherscifi/1
slug: titan_ae_movie_podcast_episode_001
title: 'Titan AE: Space exploration and Donald Bluth love'
tags:
  - 2d animation
  - 3d animation
  - 90s
  - aliens
  - explosive decompression
  - humanity
  - population bomb
  - population bottleneck
  - science fiction
  - seed vault
  - space
  - space travel

# Episode particulars
image:
  path: assets/imgs/media/movies/titan_ae_2000.jpg
  alt: 'Titan AE backdrop'
links:
  - text: 'Paperman'
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/movie/paperman/id601669104?at=1001l7hP&mt=6'
      - text: Amazon
        url: 'https://www.amazon.com/gp/video/detail/B00BENM4JC/'
  - text: 'Paperman and the Future of 2D Animation'
    url: 'https://www.youtube.com/watch?v=TZJLtujW6FY'
  - text: "Dragon's Lair"
    urls:
      - text: iTunes App Store
        url: 'https://geo.itunes.apple.com/us/app/dragons-lair-30th-anniversary/id688402750?mt=8&at=1001l7hP'
      - text: Google Play Store
        url: 'https://play.google.com/store/apps/details?id=com.digitalleisure.dragonslair&hl=en'
  - text: 'The Land Before Time'
    url: 'https://www.amazon.com/gp/video/detail/B00GGPNV68'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/titan-a.e./id270551266?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'http://www.amazon.com/gp/product/B002BSNOPM'
  title: 'Titan AE'
  type: movie
  year: 2000
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: 
  number: 1
  guid: 'http://www.decipherscifi.com/?p=89'
  media:
    audio:
      audio/mpeg:
        content_length: 19817756
        duration: '33:01'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/001_Titan_AE_decipherSciFi.mp3'
---
#### Quotes


Cale: “_Everyday I wake up and it's still the present. The same grimy, boring present. I don't think this "future" thing of yours exists._ “
How mundane the miracles of technology seem once they become ubiquitous. Wikipedia. Butt-washing machines.
Korso: "_That the human race is outta gas. It's circling the drain. It's finished! The only thing that matters is grabbing whatcha can before somebody else beats ya to it._"
The desperation of desperate people in desperate times. FYGM, rational self-interest. Sci-fi's common dichotomy between high and low life with high tech.


#### Director


Donald Bluth directed. also made _Dragon's Lair_, _Space Ace_ video games. And the movies _An American Tale_ and _The Land Before Time_. Fox Animation Studios closed after _Titan_ did badly in theaters.


#### CG/2D


Space is beautiful in CG. Christopher loves the 2d hand-drawn cell animation on top of CG environments. 2.5d, perhaps.


#### 90s


The music of the 90s. 90s fashion. Mat Damon's hair. Powerman 5000. Spaceship repair montage.


#### Alien Creatures


Cochroach chef was almost Jar Jar Binks. Straight-up murder in a cartoon surprised us. Alien kangaroo person's hundred-jointed legs are of questionable utility. Energy beings made less than enough sense.


#### Low Human Population


Humanity was down to 10,000 people millennia ago, that was close. Now here they are spread out across space. Space racism.


#### Titan Device


Like [Norwegian Seed Vault](https://en.wikipedia.org/wiki/Svalbard_Global_Seed_Vault), but in space. Sort of. Adam and Eve. New Earth _or_ Planet Bob. Joss Whedon.

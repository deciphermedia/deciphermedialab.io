---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2017-03-28 07:12:25+00:00
description: 'Daniel James Barker brings the physics to this one. Giant asteroids, neutron stars, gravity assist slingshots, how not to do simulated gravity, more'
layout: podcast_post
permalink: /decipherscifi/armageddon-feat-daniel-barker-science-communicator-episode-83
redirect_from:
  - /decipherscifi/83
slug: armageddon-feat-daniel-barker-science-communicator-episode-83
title: 'Armageddon: explosions, impactor tracking, and density of the asteroid belt  w/ Daniel James Barker'
tags:
  - science fiction
  - science
  - space
  - impactors
  - apocalypse
  - astonomy


# Episode particulars
image:
  path: assets/imgs/media/movies/armageddon_1998.jpg
  alt: Heroic American heads and a shuttle, Armageddon movie backdrop
links:
  - text: 'Deep Impact: citizen scientists, extinction events, and avoidance'
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/54'
  - text: Phil Plait's Review of Armageddon
    urls:
      - text: Bad Astronomer
        url: 'http://www.badastronomy.com/bad/movies/armpitageddon.html'
  - text: Uncertainty Principle the Podcast
    urls:
      - text: Uncertainty Principle
        url: 'https://uncertaintyprinciplethepodcast.com/'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/armageddon/id250581279?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Armageddon-Bruce-Willis/dp/B0060LYL30'
  title: Armageddon
  type: movie
  year: 1998
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: daniel_barker
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/armageddon_1998.jpg
  number: 
  guid: 'http://www.decipherscifi.com/?p=1460'
  media:
    audio:
      audio/mpeg:
        content_length: 50122126
        duration: '59:40'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Armageddon_--_Daniel_James_Barker_--_decipherSciFi.mp3'
---
#### 💥*explosion noises*💥

Michael Bay blows things up. Layers of subtlety.

#### BIG AS TEXAS

Just how very very large and unavoidably catastrophic the impactor in this movie is. Size vs Deep Impact. Size vs the dinosaur impactor.

#### Visibility of this asteroid in the sky

Angular size of this asteroid. Calculations from size, speed and distance suggested in the film (14 arcseconds). Using [Ceres](https://en.wikipedia.org/wiki/Ceres_(dwarf_planet)) as a baseline for figuring out how bright this asteroid would be in our sky.

#### Tracking potential impactors

We do this irl! It helps when they're on our solar plane. Comets can come in all sideways, which can make thing difficult. But we do okay tracking a lot of those too. [Apophis](https://en.wikipedia.org/wiki/99942_Apophis).

#### Asteroid belts etc

Common misconceptions about the density of objects in the belt. It's really diffuse in general! You won't need to fly your shuttle like a jet in not-asmosphere.

#### Things hit earth

[Lots](https://en.wikipedia.org/wiki/Vredefort_crater) [of](https://en.wikipedia.org/wiki/Sudbury_Basin) [past](https://en.wikipedia.org/wiki/Chesapeake_Bay_impact_crater) [impactors](https://en.wikipedia.org/wiki/Chicxulub_crater). Micrometeorites. Modern impacts like Chelyabinsk or Tunguska. Russia is basically a meteor magnet.

{% youtube "https://www.youtube.com/watch?v=OgCLMI3fgn0" %}

{% youtube "https://www.youtube.com/watch?v=CiLNxZbpP20" %}

#### Astronauts

Drilling and art vs science. Astronaut training and why it's not that big a deal and you can pretty much skip most of it and hop on an asteroid. 😎

#### Misc Physics

Nonsensical simulated gravity. Slingshot maneuvers. Steeling energy from moons and planets.

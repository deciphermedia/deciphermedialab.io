---
layout: person_page
person_data:
  name: Daniel Abella
  data_name: daniel_abella
  image_path: assets/imgs/people/daniel_abella.png
  title: PKD Film Festival Founder
  bio: 'Founder and Director of the International Philip K Dick Film Festival'
  websites:
    - name: The Philip K DickFilm Festival
      url: 'http://www.thephilipkdickfilmfestival.com/'
  social:
    - name: twitter
      url: https://www.twitter.com/philipkdickfest
    - facebook: thephilipkdickfilmfestival
  roles:
    - show: decipherscifi
      role: guest
---

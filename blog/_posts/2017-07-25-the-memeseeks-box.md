---
# General post frontmatter
author: christopher_peterson
date: 2017-07-25 16:41:55+00:00 
layout: post
redirect_from:
  - /decipherscifi/the-memeseeks-box
slug: the-memeseeks-box 
title: 'Launching today: The Memeseeks Box!'
image:
  path: assets/imgs/misc/us-memeseeks.png
  alt: 'Memeseeks Christopher and Colbert, illustrated by Nick Lowe, used with permission all other rights reserved'

---
***UPDATE***: Three days after we launched this, the people from Frinkiac launched [theirs](https://masterofallscience.com/) and immediately got all of the attention and traffic. There was really no point for us after that, so this no longer exists. 😢

The original post follows:

----------------------------

Just in time for the new season of Rick & Morty (as well as our own episode on the matter), we just released this thing we've been working on:


~~[The Memeseeks Box](nolongerthere://www.memeseeks.com)~~

It's a Rick & Morty memer and giffer (pronounced like the peanut butter ?). We've been saying we were working on a few things behind the scenes, and this was one of them. I'm glad to finally have it out there. :)

So meme away, and I hope you like it, and make sure you're [subscribed](https://decipherscifi.com/subscribe/) for our Rick & Morty episode in a few days!

<sup><sub>* Christopher & Colbert Meseeks illustration by [Nick Low](https://www.instagram.com/gentlemanlowe), used with permission</sub></sup>

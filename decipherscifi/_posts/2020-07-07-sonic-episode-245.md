---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2020-07-07T02:23:41-04:00
description: '8-bit nostalgia. Sanic. The console wars and the birth of Sonic as Sega mascot. The VFX re-do. Physics issues with high speeds. Hedgehogs, poop-eating, and spikey defenses.'
layout: podcast_post
redirect_from:
  - /decipherscifi/245
slug: sonic-console-wars-allocoprophagy-tiny-blue-godzilla
title: 'Sonic: the console wars, allocoprophagy, and tiny blue Godzilla'
tags:
  - marketing
  - nostalgia
  - physics
  - retro gaming
  - science
  - science fiction
  - sound barrier
  - speed
  - video games

# Episode particulars
image:
  path: assets/imgs/media/movies/sonic_the_hedgehog_2020.jpg
  alt: 'Sonic the Hedgehog running presumably very quickly with a ring and some missiles, move backdrop'
links:
  - text: 'Sonic the Hedgehog: The Animated Series Theme Song'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=fMa9Iot1Ves'
  - text: 'Human Loop the Loop with Damien Walters'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=dSDb9oKMCRc'
  - text: 'Console Wars by Blake J. Harris'
    urls:
      - text: Amazon
        url: 'https://www.amazon.com/Console-Wars-Nintendo-Defined-Generation-ebook/dp/B00FJ379XE'
      - text: iTunes
        url: 'https://books.apple.com/us/book/console-wars/id718597648?mt=11&app=itunes&at=1001l7hP'
  - text: 'SCIENTIFICALLY ACCURATE ™: SONIC THE HEDGEHOG'
    urls:
      - text: YouTube
        url: 'https://youtu.be/TvNEZ4WWQIk'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/sonic-the-hedgehog/id1496058930?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Sonic-Hedgehog-James-Marsden/dp/B084MLJYVB'
  title: Sonic the Hedgehog
  type: movie
  year: 2020
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/sonic_the_hedgehog_2020.jpg
  number: 245
  guid: sonic-console-wars-allocoprophagy-tiny-blue-godzilla
  media:
    audio:
      audio/mpeg:
        content_length: 28965908
        duration: '47:59'
        explicit: false
        url: 'https://traffic.libsyn.com/secure/decipherscifi/sonic.final.mp3'

---
# Cashing in

[Sanic](https://en.wikipedia.org/wiki/Sonic_the_Hedgehog_%28character%29#Sanic). 8-bit nostalgia. Remember the [Mario movie](https://www.themoviedb.org/movie/9607-super-mario-bros?language=en)? Imagining other possible Genesis-era ports. Chris is eagerly awaiting the film adaptation of [Pigskin Footbrawl](https://en.wikipedia.org/wiki/Pigskin_621_A.D.).

# Console wars

Sega licensing sports for title recognizability. Sega finding a mascot in Sonic and dominating the console space, momentarily.

> Sega does what Nintendon't
>
> - Sega, an intellectual

# Sonic

[Unused Sega mascot candidates](https://www.dkoldies.com/blog/teddy-roosevelt-and-7-other-rejected-sega-mascots/), including extra-mustachey Teddy Roosevelt! Sonic character design history. Consistency in appearance and attitude over *decades* and how to undo it all in your first draft of your movie. Don't forget [creepy-ass original movie sonic](https://en.wikipedia.org/wiki/Sonic_the_Hedgehog_%28film%29#Marketing) with his creepy human teeth. VFX responsibilities and character re-renderings. Tiny blue Godzilla.

> Release the butthole cut!
>
> -[Christopher]({% link _people/christopher_peterson.md %})

# Speed

Sonic's smagic speed powers. Comic book power levels where they expand to fit the plot. The speed of sound and relationship with atmospheric density. Creating "frozen time" action scenes. Setting the bar at Quicksilver. Sometimes, you can only care about the science as much as the movie does. Supersonic vs hypersonic vs ultrasonic and the mixing of and Latin. Running loops.

{% youtube "https://www.youtube.com/watch?v=T9GFyZ5LREQ" %}

# Hedgehogs

A long-term human domestication collaborator. The lovable version of a porcupine. Spikey defense mechanisms. Covering yourself in other animals' poop for fun and profit. Allocoprophagy.

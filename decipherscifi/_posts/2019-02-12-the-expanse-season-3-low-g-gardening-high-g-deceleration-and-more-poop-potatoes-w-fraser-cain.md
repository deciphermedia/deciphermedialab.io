---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - tv
date: 2019-02-12 06:00:38+00:00
description: 'Surprises about growing plants in space. Superman snowshoes and relativistic baseballs. The Fermi Paradox. Clever mechanisms for high-g maneuvering, etc'
layout: podcast_post
permalink: /decipherscifi/the-expanse-season-3-low-g-gardening-high-g-deceleration-and-more-poop-potatoes-w-fraser-cain
redirect_from:
  - /decipherscifi/180
slug: the-expanse-season-3-low-g-gardening-high-g-deceleration-and-more-poop-potatoes-w-fraser-cain
title: 'The Expanse Season 3: low-g gardening, high-g deceleration, and more poop potatoes w/ Fraser Cain'
tags:
  - acceleration
  - interstellar travel
  - kinetic weapons
  - microgravity
  - poop
  - relativity
  - science
  - science fiction
  - space
  - space settlement

# Episode particulars
image:
  path: assets/imgs/media/tv/the_expanse_2015_season_3.jpg
  alt: 'The crew of the Rocinante posing'
links:
  - text: 'The Universe Today Ultimate Guide to Viewing The Cosmos: Everything You Need to Know to Become an Amateur Astronomer by David Dickinson & Fraser Cain'
    urls:
      - text: Amazon
        url: 'https://www.amazon.com/gp/product/1624145442'
  - text: 'The Expanse Season 3'
    urls:
      - text: Amazon
        url: 'https://www.amazon.com/dp/B07BNVJK3R'
  - text: 'Guide to Space by Fraser Cain'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/user/universetoday'
media:
  links:
    - text: Amazon
      url: 'https://www.amazon.com/dp/B07BNVJK3R'
  title: The Expanse
  type: tv
  year: 2015
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: fraser_cain
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/tv/the_expanse_2015_season_3.jpg
  number: 180
  guid: 'https://decipherscifi.com/?p=10011'
  media:
    audio:
      audio/mpeg:
        content_length: 45625848
        duration: '54:18'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/The_Expanse_Season_3_--_Fraser_Cain_--_Decipher_SciFi.mp3'

---
#### Past coverage

We like this show lots! Thus, our continuing coverage:

* [The Expanse: Nick Farmer on conlanging and the intersection of language and scifi]({% link decipherscifi/_posts/2016-07-12-language-expanse-feat-nick-farmer-episode-46.md  %})
* [The Expanse VFX: an interview with Cailin Munroe, VFX Producer]({% link decipherscifi/_posts/2017-01-27-decipherscifi-is-people-01-the-expanse-vfx-w-cailin-munroe.md %})
* [The Expanse Season 2: falling into the sun, martian gravity, and human g-force limits w/ Fraser Cain]({% link decipherscifi/_posts/2017-06-06-the-expanse-season-2-feat-fraser-cain-episode-93.md  %})

#### Difficulties

The oft-overlooked actual difficulties of space settlement. Not technology and logistics, but like, how hard it will likely be for the humans doing the job at first. And second. And then for a while after, too.

#### Plants in spaaaaaaaace

Growing fresh food in space habitats. Poop-potatoes. Wide-spectrum LEDs. Self-contained hydroponics/aeroponics.

#### Low-g situations

People in spaaaaaaaaace. Dealing with microgravity. Bro, do you even lift [in space](https://www.youtube.com/watch?v=05oOst9kZXQ)? Eyeball un-squishing. Figuring out [how to burp](https://www.youtube.com/watch?v=lcvEhqQ8_O0). Fluid-pooling and major injury.

#### High-g situations.

Turning people into jelly. Differing human sensitivity to sustained g-forces in different directions. The wonderful engineering concept of the ship with omnidirectional seating that reconfigures on-the-fly for optimal g-resistance ([The Razorback](https://expanse.fandom.com/wiki/Razorback)).

#### High-speed space impact

Turning into jelly and then having your jelly explode your ship, because physics. [Relativistic baseballs](https://what-if.xkcd.com/1/). Long-haul space-racing and how to be your own butt-wiping pit crew.

#### Kinetic weapon diplomacy

Nuclear non-proliferation treaties in space. Orbital kinetic weapons and cloaked nuke-ships playing the role of the submarine as deterrent.

#### Interstellar highway program

Minimum viable star system bootstrap answering machine. The Fermi paradox. The odds that most of the signs of alien civilization that we might discover would be past.

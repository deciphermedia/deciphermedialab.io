---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2019-07-23 07:45:27+00:00
description: "Sports science and the 'big bang of body types' in athletics. Punch forces. Brain injury. Jelly brains. Reducing momentary forces on the brain. Etc."
layout: podcast_post
permalink: /decipherscifi/rocky-iv-sports-science-brain-injury-and-horsepower-punches
redirect_from:
  - /decipherscifi/203
slug: rocky-iv-sports-science-brain-injury-and-horsepower-punches
title: 'Rocky IV: sports science, brain injury, and horsepower punches'
tags:
  - brain injury
  - science
  - science fiction
  - sports

# Episode particulars
image:
  path: assets/imgs/media/movies/rocky_iv_1985.jpg
  alt: 'Rocky, morally victorious, draped in the American flag'
links:
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/rocky-iv/id278077103?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Rocky-IV-Sylvester-Stallone/dp/B001A5KRXE'
  title: Rocky IV
  type: movie
  year: 1985
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/rocky_iv_1985.jpg
  number: 203
  guid: 'https://decipherscifi.com/?p=10709'
  media:
    audio:
      audio/mpeg:
        content_length: 25245288
        duration: '30:02'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/decipherscifi_rocky_iv.mp3'

---
#### Forces

Height and reach advantage and impossible momentary punching force. Punching with horsepower vs just _straight up hitting people with horses_.

#### 'Murrica

James Brown and showboat Apollo. The Khaleesi naming scheme.

#### Sports Science

The rising tide of money in sports. The "big bang of body types." Steroids and other performance-enhancing drugs.

#### Brain Injury

The particular badness of boxing for brain damage. The possible safety increase in taking off the gloves in MMA. Just about _any_ sports injury is better than brain damage. The brain compared to Jello. The extra danger of rotational brain injury. Cumulative microconcussions.

#### Robots!

Surprisingly not a cynical product placement.

---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2019-12-17 07:45:02+00:00
description: 'Space antennas. Space elevators. Tunnels on Mars. Falling from space. Solar storms and coronal mass ejections. Finding the edge of the solar system. SETI.'
layout: podcast_post
permalink: /decipherscifi/ad-astra-nuclear-spaceships-the-edge-of-the-solar-system-and-space-monkeys
redirect_from:
  - /decipherscifi/221
slug: ad-astra-nuclear-spaceships-the-edge-of-the-solar-system-and-space-monkeys
title: 'Ad Astra:  nuclear spaceships, the edge of the solar system, and space monkeys'
tags:
  - coronal mass ejections
  - gravity
  - heliopause
  - moon base
  - nuclear propulsion
  - science
  - science fiction
  - seti
  - space
  - space elevators
  - space settlement
  - space travel

# Episode particulars
image:
  path: assets/imgs/media/movies/ad_astra_2019.jpg
  alt: 'Spaceman Brad Pitt on an artful soft green background'
links:
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/ad-astra/id1478332956?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Ad-Astra-Brad-Pitt/dp/B07XT8P2Y1'
  title: Ad Astra
  type: movie
  year: 2019
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/ad_astra_2019.jpg
  number: 221
  guid: 'https://decipherscifi.com/?p=10886'
  media:
    audio:
      audio/mpeg:
        content_length: 41372617
        duration: '49:09'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/ad_astra.final.mp3'

---
#### Science...?!

Some things are more art than science sometimes.

#### Structures anchored to geostationary orbit

"Space antennas" but what kind of signal would benefit from this design? X-Rays? Why would this not be better off in orbit? The structure sure looks a lot like a space elevator. Space elevators and geostationary orbital masses.

#### Falling from space

It's hard to steer without sufficient atmospheric density. Felix Baumgartner's [jump "from space."](https://en.wikipedia.org/wiki/Red_Bull_Stratos)

#### High energy blasting

Antimatter nonsense. Our increasing fragility in the face of an unlucky [coronal mass ejection](https://en.wikipedia.org/wiki/Coronal_mass_ejection). Protecting infrastructure both inside and increasingly _outside_ our atmosphere.

#### Moon settlement

Moon surface settlements vs moon gateways vs embarking to The Moon from low Earth orbit. Awesome moon buggy chases. Pirates!

#### Mars settlement

Communicating with and from Mars. Interplanetary distance and comms speed. Building settlements within Martian or Lunar lava tubes.

#### The heliopause

Finding the "edge" of our star system. Difference in concentration of high energy particles.

#### SETI

Looking for signs of life around other stars. Taking pictures of black holes. Planet-sized telescopes. The absurd distances from Earth to other stars. Vegemite in Australia. Absent of evidence is not evidence of absence.

#### Nuclear spaceships

[Project Orion](https://en.wikipedia.org/wiki/Project_Orion_(nuclear_propulsion)). Blowing up your spaceship because it doesn't have a nuclear bomb-ready push plate.

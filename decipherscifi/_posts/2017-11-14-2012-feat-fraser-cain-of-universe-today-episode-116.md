---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2017-11-14 07:45:25+00:00
description: "2012! How the world isn't ending for billions of years, geological warming, neutrinos, galactic alignments, the Yellowstone supervolcano, tsunami, more"
layout: podcast_post
permalink: /decipherscifi/2012-feat-fraser-cain-of-universe-today-episode-116
redirect_from:
  - /decipherscifi/116
slug: 2012-feat-fraser-cain-of-universe-today-episode-116
title: '2012: Mayan Calendar realities, galactic alignment, and ark ships w/ Fraser Cain'
tags:
  - science fiction
  - science

# Episode particulars
image:
  path: assets/imgs/media/movies/2012_2009.jpg
  alt: A Tibetan monk on a mountaintop oberserving massive global flooding
links:
  - text: 'Seveneves: impactors, guided evolution, and radiation shields w/ Daniel Mann'
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/50'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/2012/id342429302?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/2012-John-Cusack/dp/B0035FQ1UU'
  title: 2012
  type: movie
  year: 2009
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: fraser_cain
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/2012_2009.jpg
  number: 116y
  guid: 'https://decipherscifi.com/?p=4081'
  media:
    audio:
      audio/mpeg:
        content_length: 50440192
        duration: '01:00:02'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/2012_--_Fraser_Cain_--_decipherSciFi.mp3'
---
#### Mayan calendar

They had one! And so do we. They both end on some sort of the cycle and the world never ends because of our arbitrary selection of scope. Fraser is really tired of having to argue about this sort of thing with death-cultists. _The end_.

#### Galactic alignments

Neutrino detection.

#### Heating Earth

Earth's layers. Discovering new and different regions of the earth's internal structure via seismography. Rate of temperature increase. Effect on oceans. Heat retention and radioactivity.

#### Ark ships

Space settlement and Christopher's disappointment. [Seveneves]({% link decipherscifi/_posts/2016-08-09-seveneves-feat-daniel-mann-software-pro-episode-50.md %}) as the gold standard for how to relocate humanity to space in an emergency.

#### Perpetuation of specieseses and culture

Should we keep giraffes? Cows? Any larger fauna? Recognition of the complexity and energy-inefficiencies of the food chain. Surviving on crickets and phytoplankton.

#### Yellowstone

Yellowstone is no joke! Yellowstone supervolcano (more volcano conversation [in our Volcano episode]({% link decipherscifi/_posts/2017-11-07-volcano-feat-joe-ruppel-episode-115.md %})). Pyroclastic flow rates. [Lake Toba](https://en.wikipedia.org/wiki/Lake_Toba) (is larger).

#### Magnetosphere

The earth's internal metal. Pole-flipping!

#### Tsunami

Propagation of energy via wave through fluid and how it probably shouldn't roll the ship. Asteroid impacts, landslides, and misplaced fossils.

Anti-apocalypse rant

Fraser's got some stuff to say on this matter, tell-you-what.

---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2019-07-09 07:45:35+00:00
description: 'What should cyborg really mean? Cybernetics. Realizing that we really already are cyborgs. Photorealism. Using anime eyes to bypass the uncanny valley. Etc.'
layout: podcast_post
permalink: /decipherscifi/alita-battle-angel-cybernetic-cyborgs-photorealistic-cg-and-sports-plus-immortality
redirect_from:
  - /decipherscifi/201
slug: alita-battle-angel-cybernetic-cyborgs-photorealistic-cg-and-sports-plus-immortality
title: 'Alita Battle Angel: cybernetic cyborgs, photorealistic cg, and sports plus immortality'
tags:
  - anime
  - cybernetics
  - cyborg
  - photorealism
  - prosthetics
  - robots
  - science
  - science fiction
  - sports

# Episode particulars
image:
  path: assets/imgs/media/movies/alita_battle_angel_2019.jpg
  alt: 'A pile of heads and bodies of mismatched scale, with Alita at the fore'
links:
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/alita-battle-angel/id1451012705?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Alita-Battle-Angel-Rosa-Salazar/dp/B07NKSKS39'
  title: Alita Battle Angel
  type: movie
  year: 2019
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/alita_battle_angel_2019.jpg
  number: 201
  guid: 'https://decipherscifi.com/?p=10462'
  media:
    audio:
      audio/mpeg:
        content_length: 28571117
        duration: '34:00'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/alita_battle_angel_--_--_decipher_scifi.mp3'

---
#### Alita

"The movie with the cyborgs and the kicking and the punching." There are two kinds of film: people in a room talking and people in a room kicking and punching. Robert Rodriguez lands firmly within only one of these groups. Accidentally fitting Alita into the Robocop universe. Shooting and punching people in their robo-bits.

#### Cyborgs

Robots arms _everywhere_. But maybe not just for show? Cyborg arms as the cyborg future cliche. Replacing your limbs proactively for fun and profit. Replacing all of your joints, but _not_ just with hinges. Defining "cyborg" more precisely in consideration of the definition of "cybernetics."

> Cybernetics is a transdisciplinary approach for exploring regulatory systems—their structures, constraints, and possibilities.
>
> -[Wikipedia](https://en.wikipedia.org/wiki/Cybernetics)

#### "Real" cyborgs

We are already cyborgs, we just don't ourselves enough credit. Cyborgification as performance. Designing a new hearing aid to help us talk to bats.Imagining all the ways in which implanted and attached technologies would help improve the human experience. And what do we get? Always super-strong robot arms. Realizing you would need the whole-system upgrade to support the forces exerted upon/by your super-arm.

#### Doing it right

Don't be a cyborg bicep bro. Taking the short route to functional cyborg limbs by using existing neuromuscular wiring. The difficulty of inventing new appendages or body formats.

#### Photorealistic CG humans

Wow dude this was actually pretty great. Sidestepping the uncanny valley slightly by going with anime eyes. Aperture and catching more light. Legolas with dinner-plate eyeballs. [Neoteny](https://en.wikipedia.org/wiki/Neoteny).

#### Nonsensical murder sports

Future cybersports: more violent spectacle, less athleticism. Sports and functional immortality.

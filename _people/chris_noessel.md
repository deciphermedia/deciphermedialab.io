---
layout: person_page
person_data:
  name: Chris Noessel
  data_name: chris_noessel
  image_path: assets/imgs/people/chris_noessel.png
  title: Author, UX Expert
  bio: '20+ year UX veteran, author, and public speaker.'
  websites:
    - name: Sci-fi Interfaces
      url: 'https://scifiinterfaces.com/'
  social:
    - name: twitter
      url: https://www.twitter.com/chrisnoessel
  roles:
    - show: decipherscifi
      role: guest_cohost
    - show: decipherscifi
      role: roundtable_guest

---

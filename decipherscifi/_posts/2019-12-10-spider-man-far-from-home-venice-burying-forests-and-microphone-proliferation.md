---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2019-12-10 07:45:38+00:00
description: 'Spider-Man on a history tour! Civil engineering in ancient Venice. VPN security. Sea walls and climate change. Smart glasses. VFX!'
layout: podcast_post
permalink: /decipherscifi/spider-man-far-from-home-venice-burying-forests-and-microphone-proliferation
redirect_from:
  - /decipherscifi/220
slug: spider-man-far-from-home-venice-burying-forests-and-microphone-proliferation
title: 'Spider-Man Far From Home: Venice, burying forests, and microphone proliferation'
tags:
  - civil engineering
  - climate change
  - engineering
  - history
  - history
  - internet security
  - science
  - science fiction
  - smart glasses
  - vfx
  - virtual reality
  - wearables

# Episode particulars
image:
  path: assets/imgs/media/movies/spiderman_far_from_home_2019.jpg
  alt: 'Spider-Man and Mysterio, standing'
links:
  - text: 'This Video Is Sponsored By ███ VPN by Tom Scott'
    urls:
      - text: 'YouTube'
        url: 'https://www.youtube.com/watch?v=WVDQEoe6ZWY'
  - text: 'Spider-Man Into the Spider-Verse: particle colliders, art, and magnetic monopolies w/ Adrian Falcone'
    urls:
      - text: 'Decipher SciFi'
        url: 'https://decipherscifi.com/206'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/spider-man-into-the-spider-verse/id1444695454?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Spider-Man-Into-Spider-Verse-Liev-Schreiber/dp/B07L9YXWSW'
  title: 'Spider-Man: Far From Home'
  type: movie
  year: 2019
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/spiderman_far_from_home_2019.jpg
  number: 220
  guid: 'https://decipherscifi.com/?p=10879'
  media:
    audio:
      audio/mpeg:
        content_length: 32072869
        duration: '38:04'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/spiderman_far_from_home.final.mp3'

---
#### VPNs

MJ's VPN advice. VPN advertising on YouTube.

#### Venice

How to build a whole city on mud. Transplanting alder forests from _on_ some ground to _under_ some other ground. Hard clay is worse than bedrock. Measuring a sinking city's rate of subsidence with space-based radar and GPS. Climate change sure isn't helping any. What value should we place on artifacts of human civilization? Losing UNESCO world heritage sites. [The MOSE project](https://en.wikipedia.org/wiki/MOSE_Project). Climate change and rising sea levels. What will the world do?

#### VFX

Advanced holography with drones! Photoreal real-time rendering. LED walls. Projection mapping. _Lasers in your eyes_.

#### Smart glasses

Inward and outward-facing smart glass technologies. Wearable ubiquity. Evolution of "smart glasses" from Google Glass to Echo Frames. Magic Leap? Bone conduction and having discrete interactions with your personal assistant.

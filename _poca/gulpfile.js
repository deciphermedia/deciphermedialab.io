const gulp = require('gulp');
const concat = require('gulp-concat');

// ****************************
// Move JS Files to assets/js
// ****************************

gulp.task('js', function() {
  return gulp.src([
			'node_modules/flexsearch/dist/flexsearch.min.js',
		  'node_modules/@fortawesome/fontawesome-free/js/all.min.js',
      'node_modules/bootstrap/dist/js/bootstrap.min.js',
      'node_modules/imagesloaded/imagesloaded.pkgd.min.js',
      'node_modules/isotope-layout/dist/isotope.pkgd.min.js',
      'node_modules/jarallax/dist/jarallax-video.min.js',
      'node_modules/jarallax/dist/jarallax.min.js',
      'node_modules/jquery-waypoints/waypoints.min.js',
      'node_modules/jquery.easing/jquery.easing.min.js',
      'node_modules/jquery/dist/jquery.min.js',
      'node_modules/owl.carousel2/dist/owl.carousel.min.js',
      'node_modules/popper.js/dist/umd/popper.min.js',
      'node_modules/wowjs/dist/wow.min.js'
    ])
    .pipe(gulp.dest("assets/js"))
});

// ******************************
// Combine All js files into one
// ******************************

gulp.task('scripts', function() {
  return gulp.src([
    './assets/js/default-assets/audioplayer.js',
    './assets/js/default-assets/avoid.console.error.js',
    './assets/js/default-assets/classynav.js',
    './assets/js/default-assets/jquery.scrollup.min.js',
    'node_modules/imagesloaded/imagesloaded.pkgd.min.js',
    'node_modules/isotope-layout/dist/isotope.pkgd.min.js',
    'node_modules/jarallax/dist/jarallax-video.min.js',
    'node_modules/jarallax/dist/jarallax.min.js',
    'node_modules/jquery-waypoints/waypoints.min.js',
    'node_modules/jquery.easing/jquery.easing.min.js',
    'node_modules/owl.carousel2/dist/owl.carousel.min.js',
    'node_modules/wowjs/dist/wow.min.js',
  ])
    .pipe(concat('poca.bundle.js'))
    .pipe(gulp.dest('./assets/js/'))
});

// *************************************
// Move Font Awesome Fonts to assets/fonts
// *************************************

gulp.task('fafonts', function() {
  return gulp.src([
      'node_modules/@fortawesome/fontawesome-free/webfonts/*'
    ])
    .pipe(gulp.dest('assets/fonts'))
})

// *************************************
// Move Elegant Icons Fonts to assets/css
// *************************************

gulp.task('elefonts', function() {
  return gulp.src([
      'node_modules/elegant-icons/fonts/*'
    ])
    .pipe(gulp.dest('assets/css/fonts'))
})

// *******************************
// Move CSS to assets/css
// *******************************

gulp.task('css', function() {
  return gulp.src([
		  'node_modules/@fortawesome/fontawesome-free/css/all.min.css',
      'node_modules/bootstrap/dist/css/bootstrap.min.css',
      'node_modules/bootstrap/dist/css/bootstrap.min.css.map',
      'node_modules/elegant-icons/style.css',
      'node_modules/magnific-popup/dist/magnific-popup.css',
      'node_modules/owl.carousel2/dist/assets/owl.carousel.min.css',
      'node_modules/wowjs/css/libs/animate.css'
    ])
    .pipe(gulp.dest('assets/css'))
})

gulp.task('default', ['js', 'scripts', 'css', 'fafonts', 'elefonts']);

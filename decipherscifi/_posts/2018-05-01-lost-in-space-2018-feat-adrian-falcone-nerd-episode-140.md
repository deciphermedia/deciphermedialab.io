---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - tv
date: 2018-05-01 07:45:31+00:00
description: 'Lost in Space science! Just the tip of the space glacier: Adrian Falcone joins us to cover the first two episode of the new Lost in Space adaptation. Glacier lifecycles, superfreezing nonsense, space imaging. Video from Comet 67p. ASMR murderbots. What is a magnesium?'
layout: podcast_post
permalink: /decipherscifi/lost-in-space-2018-feat-adrian-falcone-nerd-episode-140
redirect_from:
  - /decipherscifi/140
slug: lost-in-space-2018-feat-adrian-falcone-nerd-episode-140
title: 'Lost in Space: supercooling, space imaging, and sexy robots w/ Adrian Falcone'
tags:
  - astronomy
  - fire
  - freezing
  - robots
  - science
  - science fiction
  - space settlement
  - space travel

# Episode particulars
image:
  path: assets/imgs/media/tv/lost_in_space_2018.jpg
  alt: 'A robot looming over a boy.... in spaaaaaaaaaace'
links:
  - text: '"Video" from the comet 67p'
    urls:
      - text: Twitter
        url: 'https://twitter.com/landru79/status/988490703075463168'
  - text: 'Lost in Space (1998 film): gifted children, future vlogging, and colonizing space'
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/10'
  - text: Which Are The Real Pictures of Space? What’s a Photo and What’s An Illustration? by Fraser Cain
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=Q41hLHcKyUM'
media:
  links:
    - text: Netflix
      url: 'https://www.netflix.com/title/80104198'
    - text: Amazon
      url: ''
  title: Lost in Space
  type: tv
  year: 2018
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: adrian_falcone
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/tv/lost_in_space_2018.jpg
  number: 140
  guid: 'https://decipherscifi.com/?p=6278'
  media:
    audio:
      audio/mpeg:
        content_length: 42162486
        duration: '50:10'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Lost_in_Space_2018_--_Adrian_Falcone_--_Decipher_SciFi.mp3'
---
#### Space travel and orienteering

Space maps. Star brightnesses and reference points. The [ESA Gaia Project](http://sci.esa.int/gaia/). Landing in glaciers. Glacier life cycles. Fjords! Callback to [Seveneves]({% link decipherscifi/_posts/2016-08-09-seveneves-feat-daniel-mann-software-pro-episode-50.md %}).

#### Space imaging

Dude, we have video from a comet! How various inputs are composited into attractive imagery. Public data and the opportunities for citizen science and science communication. Spectroscopy and impactor makeup-detection. Detecting invisible glass clouds by how they scatter light. Red shift and blue shift.

#### "Superfreezing"

Superfeeezing, supersaturation, etc. Booby-trapped microwaved water. Nucleation points. Slushy bear. Day/night cycles and weather around extreme freezing/thawing.

The "crew"

Dad's triage skills. Recognizing magnesium. Is it bigger than a breadbox?

#### Robots!

General Grievous gets the thigh ground. Fueling your nether regions with gasoline. Power-saving mode in the murder module.

#### "Fire seeds"

Giant redwood reproduction strategies. Living through wildfires. "Cleverness" in the unguided process of natural selection.

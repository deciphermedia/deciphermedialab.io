---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2016-02-23 05:45:42+00:00
description: 'We try to figure out if Idiocracy is mean or prescient. Also human evolution, dysgenics, design usability, and a step by step guide to saving the world.'
layout: podcast_post
permalink: /decipherscifi/idiocracy-adrian-falcone-movie-podcast-episode-26
redirect_from:
  - /decipherscifi/26
slug: idiocracy-adrian-falcone-movie-podcast-episode-26
title: 'Idiocracy: cynicism, user experience research, and dysgenics w/ Adrian Falcone'
tags:
  - science fiction
  - science
  - brainwashing
  - surveillance
  - intelligence
  - satire
  - user interface design
  - dystopia
  - capitalism
  - optimism
  - cynicism

# Episode particulars
image:
  path: assets/imgs/media/movies/idiocracy_2006.jpg
  alt: Idiocracy movie backdrop
links:
  - text: The Genius of Idiocracy
    urls:
      - text: Rolling Stone
        url: 'http://www.rollingstone.com/movies/features/the-genius-of-idiocracy-20141001'
  - text: Idiocracy is a Cruel Movie and You Should Be Ashamed for Liking It
    urls:
      - text: Gizmodo
        url: 'http://paleofuture.gizmodo.com/idiocracy-is-a-cruel-movie-and-you-should-be-ashamed-fo-1553344189'
  - text: The Design of Everyday Things by Donald Norman
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/the-design-of-everyday-things/id677234093?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'http://www.amazon.com/Design-Everyday-Things-Revised-Expanded-ebook/dp/B00E257T6C'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/idiocracy/id292230713?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'http://www.amazon.com/Idiocracy-Luke-Wilson/dp/B000LWBSDU'
  title: Idiocracy
  type: movie
  year: 2006
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: adrian_falcone
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: 
  number: 26
  guid: 'http://www.decipherscifi.com/?p=553'
  media:
    audio:
      audio/mpeg:
        content_length: '29335471'
        duration: '48:53'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Idiocracy_Adrian_Falcone_decipherSciFi.mp3'
---
#### Movie Production Issues

Lack of studio support. Poor audience test results. VERY small number of screens. Somehow, a cult hit after release.

#### Why Do People Love This Movie?

Has internet cult hit status. Why? Appealing to elitism and cynicism? Fart jokes? Love it or hate it.

#### Christopher Has a Problem With This Movie

It feels mean, basically. Also the extrapolations it makes are unreasonable as far as I'm concerned.

#### Dumbing Down Society

_Network_ covered similar ground in the 1970s.

#### Whose Fault is this Situation?

The yuppies for not reproducing more? The "dumb" people? The system? Corporatism, selfishness, etc?

#### Human Evolution

Defining terms because people use them in the wrong way too often. How humanity actually evolves. Dysgenics. Why the movie is wrong.

#### Bad Things

"Crab mentality."

{% youtube "https://www.youtube.com/watch?v=4q8Ob7noYys" %}

Self absorption. Laziness. Selfishness. Willful ignorance. Materialism.

#### Design for Idiots

Actually there are some interesting examples here. Simplification of UI. "Idiot-proofing." Automation.

#### How Do We Avoid This Future?

Each host gives their plan to save the world. Governor Adrian implements the ~mushroom~ marshmallow test.

{% youtube "https://www.youtube.com/watch?v=QX_oy9614HQ" %}

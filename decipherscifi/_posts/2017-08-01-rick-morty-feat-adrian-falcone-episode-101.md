---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - tv
date: 2017-08-01 06:19:03+00:00
description: "Rick & Morty and their multiverse, many worlds, Memeseeks, AI dangers, universes inside universes, schrödinger's cat and other thought experiments, more"
layout: podcast_post
permalink: /decipherscifi/rick-morty-feat-adrian-falcone-episode-101
redirect_from:
  - /decipherscifi/101
slug: rick-morty-feat-adrian-falcone-episode-101
title: 'Rick & Morty: Shrödinger, many worlds, and rules-lawyering AI w/ Adrian Falcone'
tags:
  - ai
  - many worlds
  - quantum physics
  - science
  - science fiction
  - shroedinger
  - universes

# Episode particulars
image:
  path: assets/imgs/media/tv/rick_and_morty_2013.jpg
  alt: "Rick holding open Morty's eyelids Rick & Morty backdrop"
links:
  - text: Community also by Darn Harmon
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/tv-season/community-season-1/id329363095?mt=4&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Pilot/dp/B002N7CTKM'
  - text: Rick and Morty - Finding Meaning in Life by Will Shoder
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=ez1rWBPznEc'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/tv-season/rick-and-morty-season-1-uncensored/id741096885?mt=4&at=1001l7hP'
    - text: YouTube
      url: 'https://www.youtube.com/playlist?list=ELlJwFeN0eZZI'
  title: Rick and Morty
  type: tv
  year: 2013
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: adrian_falcone
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/
  number: 101
  guid: 'https://decipherscifi.com/?p=2803'
  media:
    audio:
      audio/mpeg:
        content_length: 36958208
        duration: '43:59'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Rick_and_Morty_--_Adrian_Falcone_--_decipherSciFi.mp3'
---
#### Many Worlds

The interpretations of quantum physics that gives Rick & Morty the "multiverse." Probabilities and determinism.

#### Shrödinger's Cat

Shringer's thought experient, initially a _criticism_ of the Copenhagen Interpretation of quantum physics cum go-to example.

#### Universes

Organizing and labeling universes. The "central finite curve."

#### AI Foibles

Rules-lawyering with a genie, again. Elon Musk vs Mark Zuckerberg. [Kevin Kelly's take](https://www.wired.com/2017/04/the-myth-of-a-superhuman-ai/). Keeping them in the box.

#### Universes in Universes

Energy efficiency in multiverse batteries. Creating universes. the Kardashev scale.

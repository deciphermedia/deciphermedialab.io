---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2017-10-10 07:45:04+00:00
description: 'Mummy science! Egyptian religious context, economics, mummification, self-mummification, the history of Egyptomania, the Rosetta Stone, more'
layout: podcast_post
permalink: /decipherscifi/mummy-2017-feat-dominic-perry-history-egypt-podcast-episode-111
redirect_from:
  - /decipherscifi/111
slug: mummy-2017-feat-dominic-perry-history-egypt-podcast-episode-111
title: 'The Mummy (2017): the New Kingdom, Egyptomania, and chaotic neutral gods w/ Dominic Perry of the History of Egypt Podcast'
tags:
  - science fiction
  - science

# Episode particulars
image:
  path: assets/imgs/media/movies/the_mummy_2017.jpg
  alt: Tom Cruise in front of scary lady face
links:
  - text: The History of Egypt Podcast
    urls:
      - text: The History of Egypt Podcast
        url: 'https://egyptianhistorypodcast.com/'
  - text: The Japanese Art of Self-Preservation
    urls:
      - text: Damn Interesting
        url: 'https://www.damninteresting.com/sokushinbutsu-the-ancient-buddhist-mummies-of-japan/'
  - text: Diablo Postmortem 
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=VscdPA6sUkc'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/the-mummy-2017/id1238368178?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Mummy-Tom-Cruise/dp/B072C331FR'
  title: The Mummy
  type: movie
  year: 2017
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: dominic_perry
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/the_mummy_2017.jpg
  number: 111
  guid: 'https://decipherscifi.com/?p=3617'
  media:
    audio:
      audio/mpeg:
        content_length: 45006848
        duration: '53:34'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/The_Mummy_2017_--_Dominic_Perry_--_decipherSciFi.mp3'
---
#### Geographical Context

Traveling from ancient Egypt to Mespopotamia. Hiding evil things as far away as possible.

#### Deciphering the Mummy's historical period

Context clues pointing to Egypt's "New Kingdom," circa 1500BC-1200BC. Some touches that hinted: Ahmanet's [Nefertiti](https://en.wikipedia.org/wiki/Nefertiti)-inspired crown, her blue finger-paint as direct reference to [King Tutankhamun](https://en.wikipedia.org/wiki/Tutankhamun)'s golden finger- and toe-jewelry.

#### Speaking of King Tut...

His (lack of) actual historical importance. Public knowledge due to the simple coincidence of which things are best preserved and then discovered.

#### Egyptomania

From ancient Rome to Victorian England to King Tut in the mid-20th. It keeps coming back! The combination of modernity and exoticism of Ancient Egypt.

#### The Rosetta Stone

Napoleon's groundbreaking work in enabling the study of science and history while conquering things. The birth of Egyptology.

#### Egyptian book of the dead

Hollywood's chronic misunderstanding of the nature of The Book of the Dead. It's not evil! Egyptian beliefs re death and the afterlife.

#### Misunderstanding the ancient Egyptian gods

Chaotic neutral Seth and how he is totally not the god of death. Preservation of order against chaos in Egyptian mythology. Enumerating the actual gods of death and how they're not necessarily bad guys. The problem with mummies.

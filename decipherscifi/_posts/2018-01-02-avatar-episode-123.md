---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2018-01-02 06:26:30+00:00
description: 'Smurfs etc. Alpha Centauri, distance lightspeed and cryosleep. The moon Pandora. Ecology, flora and fauna. Planet-wide neural networks. Human bioluminescence.'
layout: podcast_post
permalink: /decipherscifi/avatar-episode-123
redirect_from:
  - /decipherscifi/123
slug: avatar-episode-123
title: 'Avatar: cryo sleep , terrestrial bioluminescence, and mooons vs planets'
tags:
  - alpha centauri
  - bioluminescence
  - cryo sleep
  - evolutin
  - science
  - science fiction

# Episode particulars
image:
  path: assets/imgs/media/movies/avatar_2009.jpg
  alt: Two blue cat people, Avatar movie backdrop
links:
  - text: The Science of Avatar by the Seattle Public Library
    urls:
      - text: SPL.org
        url: 'http://www.spl.org/library-collection/podcasts/2010-podcasts'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/avatar-2009/id354112018?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Avatar-Sam-Worthington/dp/B003EVWDR0'
  title: Avatar
  type: movie
  year: 2009
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/avatar_2009.jpg
  number: 123
  guid: 'https://decipherscifi.com/?p=4898'
  media:
    audio:
      audio/mpeg:
        content_length: 38277120
        duration: '45:33'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Avatar_--_--_decipherSciFi.mp3'
---
#### Alpha Centauri

Closest star systems to Earth. Sudden outbursts of James Cameron appreciation. Alpha Centauri {A,B}, Proxima Centauri. Sub-light space travel.

#### Cryo sleep

Continuation of consciousness. Perceptions of meatsackness.

#### Pandora the _moon_

Adaptive definitions of moons and planets. Learning about moons aside from our own. Disney World's Pandora.

{% youtube "https://www.youtube.com/watch?v=U2Di88nNS_k" %}

#### Bioluminescence

The utility of light in dark environs. Bioluminescence evolution on Earth. Useful wavelengths of produced light in water/air. Standard [human bioluminescence](http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0006256). Japanese gameshow science.

#### Pandoran flora and fauna

Megafauna and megaflora and lower gravity. Hexapodality.

#### Na'vi

Na'vi anatomy. Avatar differences, e.g. 5 fingers, eyebrows. Boobs? Dr. Manhattan.

#### The Pandoran internet

Neural networks. Brain-machine interface vs brain-everything interface. Na'vi adaptability to remote neural control. Data-transfer

#### Unobtanium

Superconductors. High-temperature superconductors! Floating "mountains."

---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2017-04-11 07:45:51+00:00
description: 'Nick Farmer of The Expanse joins us to talk about the zombie vampires in I Am Legend. Humanity in the post-apocalypse. Runaway viruses, loneliness, and dogs'
layout: podcast_post
permalink: /decipherscifi/i-am-legend-feat-nick-farmer-linguist-episode-85
redirect_from:
  - /decipherscifi/85
slug: i-am-legend-feat-nick-farmer-linguist-episode-85
title: 'I Am  Legend: zompires, fuel spoilage in the apocalypse, and loneliness w/ Nick Farmer'
tags:
  - science fiction
  - science
  - zombies
  - vampires
  - disease
  - evolution
  - undead
  - viruses

# Episode particulars
image:
  path: assets/imgs/media/movies/i_am_legend_2007.jpg
  alt: Will Smith walking with a good boy
links:
  - text: The Survivors by Nick Farmer
    urls:
      - text: Amazon
        url: 'https://www.amazon.com/Survivors-Novelette-Nick-Farmer-ebook/dp/B06XRL5SDV'
  - text: The Expanse
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/tv-season/the-expanse-season-1/id1059088673?mt=4&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Dulcinea/dp/B018BZ3SCM'
  - text: Mind Field Episode 1 - Isolation
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=iqKdEhx-dD4'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/i-am-legend-alternate-ending/id283064817?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/I-Am-Legend-Will-Smith/dp/B0017XD3OY'
  title: I Am Legend
  type: movie
  year: 2007
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: nick_farmer
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/i_am_legend_2007.jpg
  number: 85
  guid: 'http://www.decipherscifi.com/?p=1485'
  media:
    audio:
      audio/mpeg:
        content_length: 37036032
        duration: '44:04'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/I_Am_Legend_--_Nick_Farmer_--_decipherSciFi.mp3'
---
#### A book and so many adaptations

[I Am Legend (the book)](https://en.wikipedia.org/wiki/I_Am_Legend_(novel)) (1954), [The Last Man on Earth](https://en.wikipedia.org/wiki/The_Last_Man_on_Earth_(1964_film)) (1964), [The Omega Man](https://en.wikipedia.org/wiki/The_Omega_Man) (1971), [I Am Legend](https://en.wikipedia.org/wiki/I_Am_Legend_(film)) (2007), [I Am Omega](https://en.wikipedia.org/wiki/I_Am_Omega) (2007). This one (if you consider the alternative, better ending) might be the closest to getting the point of the book.

#### Zompires

Zombies vs vampires and the origin of this story in the pre-zombie era. The book inspired Night of the Living Dead!

#### Using viruses to our advantage

Viruses as workhorses to deliver medical payloads. Mutations, good and bad. Telomeres. Cancer, and the difficulties of biological immortality.

#### Loneliness

The post-apocalypse can be rough; it's difficult for social creatures. Talking to dogs and mannequins. Videos by

#### Zompire biology

What we observe presumably as the effects of their disease:

* Superquick respiration
* UV burns them to death
* Hairless
* Translucent skin
* Unhinged jaws
* Aggression
* Averbal
* Strong bones and muscle

#### Zompires vs Undead

The twist being that Will Smith is the _real_ monster! Depending on which ending you see.

#### Endings

The "alternative" was _so much better_. Who is the real monster? The meaning behind the phrase "I Am Legend" and how it is mostly lost in the films.

{% youtube "https://www.youtube.com/watch?v=kPSk30qzgFs" %}

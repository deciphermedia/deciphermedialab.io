---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2018-09-25 04:11:45+00:00
description: 'Human-technology integration. Brain-machine interfaces. Biohacking. Paralyzation workarounds, neural speed enhancement, and AI containment. Gun arms!'
layout: podcast_post
permalink: /decipherscifi/upgrade-biohacking-ai-containment-and-paralyzation-workarounds
redirect_from:
  - /decipherscifi/160
slug: upgrade-biohacking-ai-containment-and-paralyzation-workarounds
title: 'Upgrade: biohacking, AI containment, and paralyzation workarounds'
tags:
  - artificial intelligence
  - machine learning
  - paralyzation
  - prosthetics
  - science
  - science fiction

# Episode particulars
image:
  path: assets/imgs/media/movies/upgrade_2018.jpg
  alt: "March of Progress culminating in a chip"
links:
  - text: "Yudkowsky's AI box test"
    urls:
      - text: Yudkowski.net
        url: 'http://yudkowsky.net/singularity/aibox/'
  - text: 'Decipher RPG Pilot episode 1'
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/rpg1'
  - text:  'Decipher RPG Pilot episode 2'
    urls:
      - text: Decipher SciFi 
        url: 'https://deciphermedia.tv/decipherscifi/rpg2'
  - text:  'Decipher RPG Pilot episode 3'
    urls:
      - text: Decipher SciFi 
        url: 'https://deciphermedia.tv/decipherscifi/rpg3'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/upgrade/id1413558054?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Upgrade-Logan-Marshall-Green/dp/B07G7V98WZ'
  title: Upgrade
  type: movie
  year: 2018
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/upgrade_2018.jpg
  number: 160
  guid: 'https://decipherscifi.com/?p=7788'
  media:
    audio:
      audio/mpeg:
        content_length: 39747687
        duration: '47:18'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Upgrade_--_--_Decipher_SciFi.mp3'
---
#### Image enhancement

Naive vs machine-intelligent image enhancement. "[Photorealistic hallucination](https://developer.nvidia.com/gwmt)" and honest technical names. Reconsidering the absurdity of fictional image enhancement in the age of clever neural nets. Creative, artistic "enhancement" vs rigor.

#### Paralyzation workarounds

Well, breaking your spinal chord sure seems to suck. Spinal breaks and the autonomic nervous system. Bodily control via brain-machine interface.

#### Transhumanism

Extension of human functionality by implanted technology! Increasing neural communication speed, or shortening signal distance. Gun arms. _Gun arms_! Vocal device command by subvocalization.

#### The AI "box test"

Self-serving artificial intelligence. [Yudkowsky's AI  box test](http://yudkowsky.net/singularity/aibox/). Callback to [Ex Machina]({% link decipherscifi/_posts/2015-09-22-ex-machina-movie-podcast-episode-003.md %}). Additional stipulations. The true power of a goon with a wrench. How easy are humans to fool? Very.

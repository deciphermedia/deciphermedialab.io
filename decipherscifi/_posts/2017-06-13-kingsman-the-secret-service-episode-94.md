---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2017-06-13 07:45:47+00:00
description: 'Kingsman, where gentlemen spies fight with umbrellas. Prosthetic sword-legs, rebooting civilization, Gaia hypothesis, augmented reality futures, more'
layout: podcast_post
permalink: /decipherscifi/kingsman-the-secret-service-episode-94
redirect_from:
  - /decipherscifi/94
slug: kingsman-the-secret-service-episode-94
title: 'Kingsman The Secret Service: gaia hypothesis, rebooting civilization, and Oddjob with swordlegs'
tags:
  - augmented reality
  - gaia
  - prosthetic limbs
  - science
  - science fiction

# Episode particulars
image:
  path: assets/imgs/media/movies/kingsman_the_secret_service_2015.jpg
  alt: 
links:
  - text: 'Kingsman: not so secret effects'
    urls:
      - text: FXGuide
        url: 'https://www.fxguide.com/featured/kingsman-not-so-secret-effects/'
  - text: Arrival Science Fiction Film Podcast
    urls:
      - text: LSG Media
        url: 'https://www.libertystreetgeek.net/arrival/'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/kingsman-the-secret-service/id957056432?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Kingsman-Secret-Service-Colin-Firth/dp/B00TJYY1HQ'
  title: Kingsman
  type: movie
  year: 2015
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/kingsman_the_secret_service_2015.jpg
  number: 94
  guid: 'http://www.decipherscifi.com/?p=1587'
  media:
    audio:
      audio/mpeg:
        content_length: 31355496
        duration: '37:19'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Kingsman_decipherSciFi.mp3'
---
#### Gaia Hypothesis

The [idea](https://en.wikipedia.org/wiki/Gaia_hypothesis) that the biosphere self-regulates to support life. Does it [hold up?](https://www.newscientist.com/article/mg22029401-800-gaia-the-death-of-a-beautiful-idea/)

#### Civilization Reboot

Sim cards. Better ways to select the humans that will survive. Minimum populations.

#### Oddjob with swordlegs

Sword legs! Cleaving humans not just through but vertically.

#### Prosthetic feet/legs

Butts! And their utility for distance running. Hugh Herr and awesome climbing feet.

{% youtube "https://www.youtube.com/watch?v=CDsNZJTWw0w" %}

#### Augmented Reality

Versus virtual reality. Physical positioning and orientation fudging in virtual and augmented reality shared spaces. _Rainbows End_. HoloLens.

{% youtube "https://youtu.be/hglZb5CWzNQ?t=82" %}

#### Church Scene

It's so good! Also: long shots, real or otherwise.

{% youtube "https://www.youtube.com/watch?v=t1WWDBTda2Y" %}

{% youtube "https://www.youtube.com/watch?v=8q4X2vDRfRk" %}

#### The Culling

SIM card splodeyheads. Transcranial direct current stimulation. To cull, or not to cull.

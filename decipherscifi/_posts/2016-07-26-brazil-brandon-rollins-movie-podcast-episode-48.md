---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2016-07-26 07:20:15+00:00
description: 'Game designer Brandon Rollins comes on to help us face the dystopian bureaucracy with Brazil. Euphemism, scifi dystopias, complexity, security theater, more'
layout: podcast_post
permalink: /decipherscifi/brazil-brandon-rollins-movie-podcast-episode-48
redirect_from:
  - /decipherscifi/48
slug: brazil-brandon-rollins-movie-podcast-episode-48
title: 'Brazil: user interface satire, complexity, complexity, and ducting w/ Bradnon Rollins'
tags:
  - science fiction
  - science
  - satire
  - dystopia
  - government
  - british
  - police state
  - language

# Episode particulars
image:
  path: assets/imgs/media/movies/brazil_1985.jpg
  alt: Creepy baby torture mask Brazil movie backdrop
links:
  - text: War Co. Expandable Card Game
    urls:
      - text: WarCo
        url: 'http://warcothegame.com/'
  - text: The Egnineering Guy
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=hUhisi2FBuw'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/brazil-1985/id647918071?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'https://www.amazon.com/Brazil-Jonathan-Pryce/dp/B00D6C0D42'
  title: Brazil
  type: movie
  year: 1985
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: brandon_rollins
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/brazil_1985.jpg
  number: 48
  guid: 'http://www.decipherscifi.com/?p=916'
  media:
    audio:
      audio/mpeg:
        content_length: 29583655
        duration: '49:17'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Brazil_Brandon_Rollins_decipherSciFi.mp3'
---
#### Sci-Fi Dystopias

1984, Brazil, Farenheit 451, etc. Why do these count as scifi?

#### User Interfaces as analogy

Thanks to [Chris Noessel]({% link _people/chris_noessel.md %}) for pointing in the [Facebook group]({% link facebookgroup/index.html %}) the way the "badly designed" user interfaces are really incredibly well-designed, if the point is absurdism. :)

#### Complexity

In business, objects, life, gadgets, systems. Ducts! Creepin'.

#### Security Theater

Statistics, fear of skin cancer vs shark attack. Viscerality.

#### Euphemistic Language

Covering up reality. Making uncomfortable behaviour easier to swallow. Hiding reality.

#### Dreams and Reality

Cracking up. Dreams. Endings.

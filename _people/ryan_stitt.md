---
layout: person_page
person_data:
  name: 'Ryan Stitt'
  data_name: 'ryan_stitt'
  bio: 'Enthusiastic amateur historian, US Air Force serviceman, and host of the History of Ancient Greece podcast.'
  image_path: assets/imgs/people/ryan_stitt.jpg
  title: 'History Podcaster'
  websites:
    - name: 'The History of Ancient Greece Podcast'
      url: 'http://www.thehistoryofancientgreece.com/'
  social:
    - name: twitter
      url: greekhistorypod
  roles:
    - show: decipherhistory
      role: guest_cohost

---


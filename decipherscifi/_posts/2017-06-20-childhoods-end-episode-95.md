---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - tv
date: 2017-06-20 07:45:33+00:00
description: "Childhood's End, based on the classic Arthur C Clarke novel. First contact, the great filter, the Drake equation, baphomet, the end of homo sapiens, more"
layout: podcast_post
permalink: /decipherscifi/childhoods-end-episode-95
redirect_from:
  - /decipherscifi/95
slug: childhoods-end-episode-95
title: "Childhood's End: revealing aliens, Drake equation, and scientific inquiry"
tags:
  - aliens
  - drake equation
  - fermi paradox
  - first contact
  - great filter
  - science
  - science fiction
  - space
  - spaceships

# Episode particulars
image:
  path: assets/imgs/media/tv/childhoods_end_2015.jpg
  alt: A farmer on a tractors with a shotgun looking up at an Alien spaceship
links:
  - text: Childhood's End by Arthur C. Clarke
    urls:
      - text: iTunes
        url: 'https://books.apple.com/us/book/childhoods-end/id1439946022?mt=11&app=itunes&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Chldhoods-End-Arthur-C-Clarke/dp/B01CPSBYKY'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/tv-season/childhoods-end-season-1/id1059590227?mt=4&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/The-Overlords/dp/B018SX759E'
  title: Childhood's End
  type: tv
  year: 2015
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/tv/childhoods_end_2015.jpg
  number: 95
  guid: 'http://www.decipherscifi.com/?p=1598'
  media:
    audio:
      audio/mpeg:
        content_length: 43665093
        duration: '51:31'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Childhoods_End_--_--_decipherSciFi.mp3'
---
#### Arrival/First Contact

Aircraft shielding. Impressing upon humanity the futility of resistance.

#### Selecting Diplomats

Alien diplomatic moneyball.

#### They Know Us

Flawless colloquial earth languages. Cultural context. How to manipulate apes. Benevolent alien dictatorship.

#### Curbing Scientific Inquiry

Approaching utopia removing incentives for innovation. Alien limits on research. "The stars were not meant for man."

#### The Great Filter

Maybe these aliens in the show are the great filter. Filling in some slots of the Drake Equation. Maybe we're just terrible for the universe and they know it. :/

#### Butt Numbers

Directed evolution at a scale of tens to thousands of years.

#### Alien Appearance

This part is too spoilery for this page

#### Remember Us

Not too much to ask, really. We could have had a worse run.

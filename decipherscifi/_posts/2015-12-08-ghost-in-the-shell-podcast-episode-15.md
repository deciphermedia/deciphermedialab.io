---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: '2015-12-08 04:45:50+00:00'
description: 'With Ghost in the Shell on the podcast we talk anime, cybernetics, cyborgs, the soul, emergent AI, digital evolution, and more!'
layout: podcast_post
permalink: /decipherscifi/ghost-in-the-shell-podcast-episode-15
redirect_from:
  - /decipherscifi/15
slug: ghost-in-the-shell-podcast-episode-15
title: 'Ghost in the Shell: scifi anime, computer evolution, and cybernetics'
tags:
  - science fiction
  - science
  - artificial intelligence
  - 2d animation
  - anime
  - cyborg
  - man versu smachine
  - cyberpunk
  - android
  - robot
  - emergence
  - evolution
  - hacking

# Episode particulars
image:
  path: assets/imgs/media/movies/ghost_in_the_shell_1995.jpg
  alt: Ghost in the Shell 1995 anime movie backdrop
links:
  - text: Ghost in the Shell (original manga)
    urls:
      - text: Amazon
        url: 'http://www.amazon.com/Ghost-Shell-SAC/dp/1935429019'
  - text: Ghost in the Shell Standalone Complex
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/tv-season/ghost-in-shell-stand-alone/id275693606?at=1001l7hP&mt=4'
      - text: Amazon
        url: 'http://www.amazon.com/Ghost-Shell-Complex-Complete-Collection/dp/B001D265RQ'
  - text: The Matrix
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/movie/the-matrix/id271469518?at=1001l7hP&mt=6'
      - text: Amazon
        url: 'http://www.amazon.com/Matrix-Keanu-Reeves/dp/B000HAB4KS'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/ghost-in-the-shell/id368627419?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'http://www.amazon.com/Ghost-Shell-Richard-Epcar/dp/B000VTNLBU'
  title: 
  type: movie
  year: 
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: 
  number: 15
  guid: 'http://www.decipherscifi.com/?p=366'
  media:
    audio:
      audio/mpeg:
        content_length: 25481478
        duration: '35:23'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Ghost_in_the_Shell_decipherSciFi.mp3' 
---
#### Anime

We watched a lot of it in highschool. How this film transcends the norm. Timeline of anime and the cyberpunk aesthetic.

#### Influence

The influences that went into this movie. The huge influence this movie had on everything after.

#### Plot

For once we have to lay out the plot of the movie in detail, because it's kind of complicated!

#### The Opening Scene

Awesome. Exploding heads. Japanese pornography laws. Encryption as armament.

#### Cybernetics/Cyborgs

The spectrum on jacked-in-ness in the movie. Taxonomy of androids, cyborgs, and robots.

#### The Soul/Ghost

Mind-body dualism. The treatment of consciousness in the film.

#### Evolution in Binary

Emergent AI. What is life? Evolution of digital life forms.

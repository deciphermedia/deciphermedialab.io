---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2019-03-05 07:45:39+00:00
description: "Turning Superman's hair into a space elevator. Close-talking in space. Eyeballing orbital mechanics. Magic 'brick vision.' Reducing nuclear weapons. Etc."
layout: podcast_post
permalink: /decipherscifi/superman-iv-superman-hair-materials-science-denuclearization-and-pushing-the-moon-w-jolene-creighton
redirect_from:
  - /decipherscifi/183
slug: superman-iv-superman-hair-materials-science-denuclearization-and-pushing-the-moon-w-jolene-creighton
title: 'Superman IV: Superman hair materials science, denuclearization, and pushing the moon w/ Jolene Creighton'
tags:
  - comics
  - nuclear war
  - science
  - science fiction
  - superman
  - superpowers
  - the sun

# Episode particulars
image:
  path: assets/imgs/media/movies/superman_4_1987.jpg
  alt: 'Superman zooming with a nuclear missile'
links:
  - text: 'Nukemap'
    urls:
      - text: 'Nuclear Secrecy'
        url: 'https://nuclearsecrecy.com/nukemap/'
  - text: 'Superman IV deleted scene "Nuclear Man 1"'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=4ysPO7cWRng'
  - text: 'Jolene @ FoL'
    urls:
      - text: 'Future of Life'
        url: 'https://futureoflife.org/author/jolene/'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/superman-iv/id560076141?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Superman-IV-Quest-Christopher-Reeve/dp/B001EBV0M6'
  title: 'Superman IV'
  type: movie
  year: 1987
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: jolene_creighton
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/superman_4_1987.jpg
  number: 183
  guid: 'https://decipherscifi.com/?p=10248'
  media:
    audio:
      audio/mpeg:
        content_length: 37204187
        duration: '44:16'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Superman_4_--_Jolene_Creighton_--_Decipher_SciFi.mp3'

---
#### A review

This was the best worst thing we've ever seen. I'm sorry. You're welcome?

#### Space tricks

Leaving whole rocket stages to just kinda be in orbit and hit stuff. Talking in space by providing your own atmosphere thanks to your incredible, _super_ lung capacity. Also, how that doesn't work and Superman needs to be a close talker and hold conversations by vibrating his mouth directly on someone else's spacesuit helmet.

#### Superman bodily materials

The hair that can hold at least a half ton. How does Superman cut his hair? Introducing weird gnarly alternate Superman with really long hair and uncut fingernails. Donating Superman's bits to materials science. Making things out of hair. Totally realistic space elevator musings. Superman as a launch platform. Dissecting the Man of Steel for fun and profit. Using Superman for "free energy" ([Saturday Morning Breakfast Cereal](https://www.smbc-comics.com/comic/2011-07-13)).

#### Super-denuclearization

Superman's built-in orbital mechanics skills. The tiny bit of nothing that would be all of 1987's nuclear world arsenal against the power of the sun. The relative tiny-ness of the only nuclear weapons ever used in war. Proposals for nuclear de-proliferation. Going Dr Manhattan on the problem.

A bunch of stuff oh god this movie was silly

Nuclear Man's totally fab nails. The golden crotch-trim on his super suit. Plugging a volcano. Superman's "brick vision." Slowing things down, because the moon has "less gravity." It's obvious, really. The actual disasters that await us if Superman pushed the Moon away.

{% youtube "https://www.youtube.com/watch?v=4ysPO7cWRng" %}

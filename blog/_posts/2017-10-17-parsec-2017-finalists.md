---
# General post frontmatter
author: christopher_peterson
categories:
  - awards
date: 2017-10-17 13:14:20+00:00 
layout: post
slug: parsec-2017-finalists 
title: "We're 2017 Parsec Awards finalists!"
image:
  path: assets/imgs/misc/parsec_awards_2017_nomination.jpg
  alt: 'Parsec Awards nomination announcement'

---
Hey, neat!

We're finalists in the "Fact Behind the Fiction" category at the [2017 Parsec Awards](http://www.parsecawards.com/2017-parsec-awards/2017-finalists/)!

Thanks to a few different folks for nominating us some months ago and thanks to the Parsec people for putting us in a group with these other fine shows. We'll let you know how it goes sometime in November :)

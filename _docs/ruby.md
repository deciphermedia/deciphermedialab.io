Ruby
====

This document will outline how to get Ruby setup to happily install and use gems in the context of both your user and the project, and then install bundler which will manage the project gems.

Just run the code if you don't want to think about it and you'll *probably* get where you wanted to go... or read the comments for explanation.

# Ubuntu

```sh
# ruby
sudo apt-get install ruby
# tell gem where to install gems *for your user*
echo 'export gem_home="${home}/.gems"' >> "${home}/.bash_profile"
# tell your path where to look for executables from ruby gems. we'll need this to easily run the `jekyll` command itself.
echo 'export path="${home}/.gems/bin:${path}"' >> "${home}/.bash_profile"
# as bundler will prob always be used in the context of a project, tell it to use the relative directory `vendor`
# e.g. if the project is in `/src/deciphermedia`, bundler would install gems under the `/src/deciphermedia/vendor` directory
echo -e '---\nbundle_path: "vendor"' > .bundle/config

# install at the user level the one gem needed in order to do everything else
gem install bundler
```

# OSX

```sh
# brew something something and then prob most of the same stuff from above but ¯\_(ツ)_/¯
```

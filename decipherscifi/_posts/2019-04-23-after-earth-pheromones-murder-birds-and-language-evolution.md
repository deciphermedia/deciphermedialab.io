---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2019-04-23 07:45:18+00:00
description: 'Welcome to Earth! But, afterwards. Pheromones in animals. Evidence of human pheromone action. Shrike-modeled space monsters. Language over millennia. etc.'
layout: podcast_post
permalink: /decipherscifi/after-earth-pheromones-murder-birds-and-language-evolution
redirect_from:
  - /decipherscifi/190
slug: after-earth-pheromones-murder-birds-and-language-evolution
title: 'After Earth: pheromones, murder-birds, and language evolution'
tags:
  - atmosphere
  - evolution
  - language
  - pheromones
  - science
  - science fiction
  - space

# Episode particulars
image:
  path: assets/imgs/media/movies/after_earth_2013.jpg
  alt: 'Will Smith and little Will Smith doing Predator'
links:
  - text: 'Lion scent-marking with non-scientific audio'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=he2fgcO9uLQ'
  - text: 'Eddie Izzard "Babies on spikes" bit without any context'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=a41PNkrgYcg'
  - text: '"Butchering Bird" (the shrike)'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=okQYO10MT3c'
  - text: 'In Our Time - Pheromones'
    urls:
      - text: BBC
        url: 'https://www.bbc.co.uk/programmes/m0002mdl'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/after-earth/id682824463?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/After-Earth-Will-Smith/dp/B00EP0KBXM'
    - text: YouTube
      url: 'https://www.youtube.com/watch?v=PRln879ol50'
  title: After Earth
  type: movie
  year: 2013
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/after_earth_2013.jpg
  number: 190
  guid: 'https://decipherscifi.com/?p=10382'
  media:
    audio:
      audio/mpeg:
        content_length: 35543435
        duration: '42:18'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/after_earth_--_--_decipherscifi.mp3'

---
#### This movie

Not well reviewed. Will Smith's "[painful failure](https://variety.com/2015/film/news/will-smith-after-earth-comment-most-painful-failure-of-his-career-1201432773/)." The real twist is the lack of explanation for things. [Jaden Smith wisdom](https://www.buzzfeed.com/genamourbarrett/jaden-smith-tweets-that-will-make-you-say-huh).

#### Accent and language

The future "language" in the film. The difference between accent and dialect. how languages change over time, by example. Trying to understand original [spoken Shakespeare](https://www.youtube.com/watch?v=WeW1eV7Oc5A) and generally succeeding. Trying to understand original [spoken Beowulf](https://www.youtube.com/watch?v=CH-_GwoO4xI) and completely failing. The mutual intelligibility of Old Norse and Old English.

#### Creatures

Surprise! There are aliens and they want to kill us. Engineered animal-weapons (the "Ursa"). Improving the Ursa design: giving guns to blind animals for fun and profit. Komodo dragon-mode leeches. Evolving from parasite to hunter. The combat techniques of butt-spiking murderbirds (shrikes).

#### Pheromones

"Smelly handshakes." Mammary pheromone response in human infants. Getting used to stinky things. Tales of accidental moth pheromone pranks.

#### Quarantine earth

Surprising evolutionary directions in the absence of humanity. Even a thousand years later, baboons still don't appreciate getting hit with rocks.

#### Atmosphere

The dangers of low atmospheric oxygen. Using the top of Everest as marker for O2 danger. Climbing Everest in shorts.

---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2016-06-07 07:20:10+00:00
description: 'Adrian Falcone is back on the show for The Man From Earth: the biology and psychology of immortality, human evolution, human history and language, more'
layout: podcast_post
permalink: /decipherscifi/man-from-earth-adrian-falcone-movie-podcast-episode-41
redirect_from:
  - /decipherscifi/41
slug: man-from-earth-adrian-falcone-movie-podcast-episode-41
title: 'The Man From Earth: biological immortality, and what is a micromort w/ Adrian Falcone'
tags:
  - science fiction
  - science
  - immortality
  - language
  - religion
  - linguistics
  - history
  - fantasy

# Episode particulars
image:
  path: assets/imgs/media/movies/the_man_from_earth_2007.jpg
  alt: The Man From Earth figure in blinding light movie backdrop
links:
  - text: '"Good job, BRAIN!" by Good Job Brain'
    urls:
      - text: GoodJobBrain.com
        url: 'http://www.goodjobbrain.com/blog/2016/5/9/181-good-job-brain.html'
  - text: Risk. by VSauce
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=w-CK8VxMz9g'
  - text: 
    urls:
      - text: 
        url: ''
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/the-man-from-earth/id329255207?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'https://www.amazon.com/Man-Earth-David-Lee-Smith/dp/B001D0BI5W'
  title: The Man From Earth
  type: movie
  year: 2007
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: adrian_falcone
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/the_man_from_earth_2007.jpg
  number: 41
  guid: 'http://www.decipherscifi.com/?p=812'
  media:
    audio:
      audio/mpeg:
        content_length: 28956481
        duration: '48:15'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/The_Man_From_Earth_Adrian_Falcone_decipherSciFi.mp3'
---
#### Production

Jerome Bixby's final, lifelong work. A bittorrent success story. The Man From Earth: The Series

#### Why this movie is the best

A great mechanism for great conversation: a collection of domain experts exploring hypotheticals.

#### Human Evolution

Biological differences. Geography. Proto human language.

#### Memory and Learning

Plasticity. Long term memories. Retrospective fill-ins. Meta-memory.

#### Immortal Biology

Regeneration. Disease. Scarring and cancer.

#### Nasty, Brutish, and Short

Living through humanity's violent past. Adjusting to the current morality. Old fashioned ways.

#### Danger and Risk

Micromorts. Judging risk and how to live life when you're immortal.

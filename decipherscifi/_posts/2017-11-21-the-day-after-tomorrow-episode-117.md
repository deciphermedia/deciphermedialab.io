---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2017-11-21 07:45:55+00:00
description: 'The Day After Tomorrow! Questionable origins, climate science, paleoclimatology, ice cores, land hurricanes, thermohaline circulation, arctic vs antarctic'
layout: podcast_post
permalink: /decipherscifi/the-day-after-tomorrow-episode-117
redirect_from:
  - /decipherscifi/117
slug: the-day-after-tomorrow-episode-117
title: 'The Day After Tomorrow: paleoclimatology, thermohaline circulation, and climate modelers'
tags:
  - arctic
  - climate
  - climatology
  - geology
  - paleontology
  - science
  - science fiction

# Episode particulars
image:
  path: assets/imgs/media/movies/the_day_after_tomorrow_2004.jpg
  alt: A city that froze really quickly
links:
  - text: Inside The Giant American Freezer Filled With Polar Ice by tom Scott
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=DTRb6G9c9wU'
  - text: 13 Misconceptions About Global Warming
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=OWXoRSIxyIU'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/the-day-after-tomorrow/id271476626?mt=6'
    - text: Amazon
      url: 'https://www.amazon.com/Day-After-Tomorrow-Dennis-Quaid/dp/B000NDMRCS'
  title: The Day After Tomorrow
  type: movie
  year: 2004
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/the_day_after_tomorrow_2004.jpg
  number: 117
  guid: 'https://decipherscifi.com/?p=4135'
  media:
    audio:
      audio/mpeg:
        content_length: 25794560
        duration: '30:41'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/The_Day_After_Tomorrow_--_--_decipherSciFi.mp3'
---
#### Origins

Roland Emmerich. And from a book by Art Bell! ...of all people. Not known for hard scifi.

#### Climate science communication

Some examples are better than others.

#### Arctic vs Antarctic

They are... different! How they are different and why the movie might have chosen to go with the "wrong" one for the real-life event on which the opening scene based. [Larsen B Ice Shelf](https://en.wikipedia.org/wiki/Larsen_Ice_Shelf#Larson_B). Continental ice vs glacial ice.

{% responsive_image path: assets/imgs/misc/antarctic_ice_shelves.jpg alt: "Diagram of Antarctic ice shelves" caption: "Antarctic ice shelves" attr_text: "A. J. Cook and D. G. Vaughan CC-BY-3.0" attr_url: "https://www.the-cryosphere-discuss.net/3/579/2009/tcd-3-579-2009.pdf" %}

#### Thermohaline circulation

Arctic/antarctic events and affect on the [thermohaline circulation](https://en.wikipedia.org/wiki/Thermohaline_circulation). The Younger Dryas event as an irl historical example of _extreme_ climate shift which still took decades longer than the events of the film.

#### Paleoclimatology

Human records. Ice Cores! Meteoric ice. Tree rings! Water body beds! Caves!

#### Ice cores

Age and layers. Ash, gas, life, temperatures from oxygen isotope ratios.

#### Climate modeling

Colbert's insights from The Supercomputing Conference. The big data; it is very big.

#### "Landicanes"

... which are the name we've made up for the misnomered "hurricanes" that seem to form over land in the movie. Air flow. the Coriolis effect, thermodynamics and superfreezing in the center of the storm.

{% responsive_image path: assets/imgs/misc/hurricane_structure.jpg alt: "Diagram of the structure of a hurricane" caption: attr_text: "Kelvinsong@Wikimedia CC-BY-3.0" attr_url: "https://commons.wikimedia.org/wiki/File:Hurricane-en.svg" %}

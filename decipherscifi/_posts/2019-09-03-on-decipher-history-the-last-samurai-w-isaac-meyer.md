---
# General post frontmatter
categories:
  - episode
  - bonus
date: 2019-09-03 08:30:13+00:00
layout: podcast_post
permalink: /decipherscifi/on-decipher-history-the-last-samurai-w-isaac-meyer
redirect_from:
slug: on-decipher-history-the-last-samurai-w-isaac-meyer
title: 'On Decipher History: The Last Samurai w/ Isaac Meyer'
tags:
  - promo

# Episode particulars
image:
  path: assets/imgs/media/movies/the_last_samurai_2003.decipherhistory_promo.jpg
  alt: Tom Cruise at his most Japanese
links:
media:
people:
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/the_last_samurai_2003.decipherhistory_promo.jpg
  number: 
  guid: 'https://decipherscifi.com/?p=10768'
  itunes:
    episodetype: 'bonus'
  media:
    audio:
      audio/mpeg:
        content_length: 2984842
        duration: '03:27'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/the_last_samurai.dsf.promo.mp3'

---
A small taste of our new episode of Decipher History to remind you to subscribe to the new show. This week featuring our new friend Isaac Meyer from [The History of Japan Podcast]({% link decipherscifi/_posts/2019-09-03-on-decipher-history-the-last-samurai-w-isaac-meyer.md %} )!

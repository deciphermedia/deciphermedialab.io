---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - tv
date: 2019-10-29 07:45:41+00:00
description: "The Kazon taking up space until the Borg. The scale of the Star Trek &'universe.' The availability of water in the galaxy. Transporters and replicators. Etc."
layout: podcast_post
permalink: /decipherscifi/star-trek-voyager-water-galactic-scales-and-coffee-replicators
redirect_from:
  - /decipherscifi/214
slug: star-trek-voyager-water-galactic-scales-and-coffee-replicators
title: 'Star Trek Voyager: water, galactic scales, and coffee replicators'
tags:
  - aliens
  - coffee
  - evolution
  - galaxy
  - science
  - science fiction
  - space
  - space travel
  - star trek

# Episode particulars
image:
  path: assets/imgs/media/tv/star_trek_voyager_1995.jpg
  alt: 'Star Trek Voyager season 1 crew'
links:
  - text: 'The Orville'
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/tv-season/the-orville-seasons-1-2/id1454065074?mt=4&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Old-Wounds/dp/B074SY51TB'
      - text: Hulu
        url: 'https://geo.itunes.apple.com/us/tv-season/the-orville-seasons-1-2/id1454065074?mt=4&at=1001l7hP'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/tv-season/star-trek-voyager-the-complete-series/id1439413673?mt=4'
    - text: Amazon
      url: 'https://www.amazon.com/Caretaker/dp/B005HEVBGG'
  title: 'Star Trek: Voyager'
  type: movie
  year: 1995
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/tv/star_trek_voyager_1995.jpg
  number: 214
  guid: 'https://decipherscifi.com/?p=10831'
  media:
    audio:
      audio/mpeg:
        content_length: 21438615
        duration: '25:25'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/star_trek_voyager.mp3'

---
#### Setup

Cross-galaxy teleportation macguffins. The Delta quadrant. Where are the Borg!?

#### Species

The Kazon, aka less-good, less-organized Klingons. Were Klingons racist? Or the Ferengi? Or the Kazons?

#### Water

The availability of water in star systems. Exoplanet surveys and water-rich gas giants.

#### Voyager

Yay Janeway! Comparing with the original actress. Imagining a Star Trek/[Too Many Cooks](https://www.youtube.com/watch?v=QrGrOK8oZG8) crossover.

{% youtube "https://www.youtube.com/watch?v=8SIZcDWKyw0" %}

#### Replicators and teleporters

The most incredible Star Trek technology of all. Teleporter failure rates: very low, or actually 100%?

#### Scales

Galaxy Scale. "Quandrants" actually refer to "quarters of the galaxy" which blew Christopher's mind slightly ([like this guy](https://www.reddit.com/r/startrek/comments/4zarou/ive_been_watching_star_trek_for_21_years_and_i/)). And [here is a map](http://1.bp.blogspot.com/-y58dY7Qf8aE/UUIuRkFAfgI/AAAAAAAADAA/gPFtf3F07W4/s1600/Star_Trek_Unity_One_Map_WP_by_Joran_Belar.jpg). Putting speed limits on the universe.

---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2018-04-03 07:45:49+00:00
description: 'Stargate science with Dominic from The History of (ancient) Egypt Podcast! Swole Daniel Jackson, ancient Egyptian pyramid construction, alien conspiracy hypotheses, and meso-American pyramids. Stone vs copper vs bronze, relative stone hardness. Advantage of arsenical copper. Stargates & wormholes & singularities, more'
layout: podcast_post
permalink: /decipherscifi/stargate-feat-dominic-perry-of-the-history-of-egypt-podcast-episode-136
redirect_from:
  - /decipherscifi/136
slug: stargate-feat-dominic-perry-of-the-history-of-egypt-podcast-episode-136
title: 'Stargate: pyramid builders, ancient Egyptian writings, and swole Daniel Jackson  w/ Dominic Perry'
tags:
  - egyptology
  - exoplanets
  - history
  - language
  - linguistics
  - mummification
  - pyramids
  - robots
  - science
  - science fiction
  - stargates

# Episode particulars
image:
  path: assets/imgs/media/movies/stargate_1994.jpg
  alt: A stargate and a pyramid on a space background
links:
  - text: The History of Egypt Podcast
    urls:
      - text: Website
        url: 'https://egyptianhistorypodcast.com/'
  - text: The Trouble With Transporters by CGP Grey
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=nQHBAdShgYI'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/stargate/id251188802?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Stargate-Kurt-Russell/dp/B06XBQZDZ3'
  title: Stargate
  type: movie
  year: 1994
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: dominic_perry
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/stargate_1994.jpg
  number: 136
  guid: 'https://decipherscifi.com/?p=5931'
  media:
    audio:
      audio/mpeg:
        content_length: 50764464
        duration: '01:00:25'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Stargate_--_Dominic_Perry_--_Decipher_SciFi.mp3'
---
#### Pre-historic Egypt

The notability of the introduction of a massive metal space-portal 10,000 years ago, when Egyptian were still just barely getting down with agriculture.

#### Egypt and the pyramids

When was the Bronze age anyway? Pyramid time scales. Familial pyramid-building competitions. The progression from small-scale burial mounds to "great" pyramids. Egypt vs meso-American pyramid research. Seasonal construction recruitment.

#### Stone and copper tooling for stonework

Stone tools. Copper tools. Early Brone tools. Limestone vs granite. Arsenical copper advantages.

#### Pyramid-building alien hypotheses

"Chariots of the Gods" and the origin of alien pyramid conspiracies. Judging fringe theorists.

#### Ancient Egyptian language and writing

The origin of the writing systems of ancient Egypt. Accounting.

#### Daniel Jackson

Inspiring budding Egyptologists and getting suddenly swole.

#### Stargates

Stargate discovery. Deep time and the chances of happening upon ready-made technology after an extinction. Permeable wormhole membranes. Stargates vs black holes. EM transmission and accretion disks. Molecular deconstruction.

#### Probe robots

Bomb disposal space bot. Colonization.

#### Exoplanets

Stargates and our knowledge at the time.

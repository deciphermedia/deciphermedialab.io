---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2018-05-08 07:45:33+00:00
description: 'Zathura. Science words. Light pollution and Dark Sky Preserves. Ring systems: rocks vs ice, and lifecycles. Cryonic sleep. Gravity wells. Reptilian-ish aliens. Metabolic rates, evolution, and intelligence. Warm-blooded dinosaurs. You are made of meat. Kimchi in space. shooting stars. More!'
layout: podcast_post
permalink: /decipherscifi/zathura-episode-141
redirect_from:
  - /decipherscifi/141
slug: zathura-episode-141
title: 'Zathura: planetary ring lifecycles, science words, and light pollution'
tags:
  - aliens
  - astronomy
  - black holes
  - cryo sleep
  - gravity
  - science
  - science fiction
  - space
  - technobabble

# Episode particulars
image:
  path: assets/imgs/media/movies/zathura_2005.jpg
  alt: Two boys gazing wonder at outer space through their front door, Zathura movie backdrop
links:
  - text: If Earth had Rings by Artifexian
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=CItDiuBWP5I'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/zathura/id270815669?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Zathura-Josh-Hutcherson/dp/B000GT6L9Y'
  title: Zathura
  type: movie
  year: 2005
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/zathura_2005.jpg
  number: 141
  guid: 'https://decipherscifi.com/?p=6311'
  media:
    audio:
      audio/mpeg:
        content_length: 40646558
        duration: '48:22'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Zathura_--_--_Decipher_SciFi.mp3'
---
#### Science words

Jumanji in space. Card-by-card science words.

#### Light pollution

The night sky "never looked that close before." The majority of Americans being unfamiliar with a _real_ view of the night sky. Light pollution. [Dark sky preserves](https://en.wikipedia.org/wiki/Dark-sky_preserve). The [LA blackout and the milky way](https://www.reddit.com/r/AskHistorians/comments/3i3vzq/did_people_actually_call_911_or_an_observatory/). Light pollution and the effect on wildlife. And the effects on _us._

{% responsive_image path: assets/imgs/misc/light_pollution_skyglow.jpg alt: "False color visualization of Sky glow from urban light sources" caption: "Sky glow. Light pollution!" attr_text: "Royal Astronomical Society CC-BY" attr_url: "https://commons.wikimedia.org/wiki/File:Light_pollution_europe.jpg" %}

#### Planetary rings

100% ring rate on gas giants in our solar system. Ring size and albido. The lifecycle of a ring system. The "Roche Limit." Ring formation beginnings. Rocks vs ice in ring systems. [Super-large ring system discoveries](http://www.rochester.edu/newscenter/gigantic-ring-system-around-j1407b/).

{% responsive_image path: assets/imgs/misc/roche_limit.jpg alt: "A visualization of a body breaking up at its Roche Limit" caption: "The Roche limit" attr_text: "Theresa Knott CC-BY-SA-3.0" attr_url: "https://commons.wikimedia.org/wiki/File:Roche_limit_(ripped_sphere).svg" %}

#### Cryonic sleep

Cryonic sleep and galactic travel. Teenager sleep schedules.

#### Gravity Wells

Weightlessness in orbit. Orbit is falling.

#### Meeting aliens

As per Hawking: you might not want to!

#### Zorgons

Don't forget: people are meat too! Cold blooded vs warm-blooded intelligence. Metabolic rates and bloodedness. Dinosaurs [may have been mesotherms](http://phenomena.nationalgeographic.com/2014/06/12/dinosaurs-tuna-great-whites-echidnas/)!

> You are as meat as a cow!
>
> -Colbert

#### Space food

Tubes and bricks. Bad roomates and sardine tubes. Kimchi rocket ships in _spaaaaaace_. Eating your sandwich in the bathroom, just in case your microbiome can't handle the onslaught.

#### Shooting stars

IRL shooting stars. Rogue stars. Meteor shower disappointments.

---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2019-02-05 07:45:21+00:00
description: "The Arnold Experience, finally. Memory implantation and wiping. The separateness of 'muscle memory.' Tracking devices in your butt. Oxygen on Mars. Mummies. Etc."
layout: podcast_post
permalink: /decipherscifi/total-recall-top-notch-arnold-muscle-memory-and-how-not-to-settle-mars-w-josh-effengee
redirect_from:
  - /decipherscifi/179
slug: total-recall-top-notch-arnold-muscle-memory-and-how-not-to-settle-mars-w-josh-effengee
title: 'Total Recall - top-notch Arnold, muscle memory, and how not to settle Mars w/ Josh'
tags:
  - arnold schwarzenegger
  - dystopia
  - man meat
  - mars
  - mummification
  - science
  - science fiction
  - space resources
  - space settlement

# Episode particulars
image:
  path: assets/imgs/media/movies/total_recall_1990.jpg
  alt: 'Arnold in closeup being like ARGUGUGURURUAURUAA'
links:
  - text: "The X-Files Podcast (featuring Josh always)"
    urls:
      - text: LSG Media
        url: 'https://www.libertystreetgeek.net/subscribe-xfiles/'
  - text: 'Science Fiction Film Podcast (featuring Josh sometimes)'
    urls:
      - text: LSG Media
        url: 'https://www.libertystreetgeek.net/subscribe-sffp/'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/total-recall-mind-bending-edition/id539413297?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Total-Recall-Arnold-Schwarzenegger/dp/B000IHL52W'
  title: Total Recall
  type: movie
  year: 1990
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: josh_effengee
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/total_recall_1990.jpg
  number: 179
  guid: 'https://decipherscifi.com/?p=9979'
  media:
    audio:
      audio/mpeg:
        content_length: 42313568
        duration: '50:21'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Total_Recall_--_Josh_Effengee_--_Decipher_SciFi.mp3'

---
#### Man meat

Ahhhhnold! Your average Joe, working construction just like any other regular guy who is a giant bodybuilding monster-person. We really missed this in our coverage of [Total Recall 2012]({% link decipherscifi/_posts/2019-01-15-total-recall-2012-falling-through-the-planet-3d-scanning-and-lasers-in-your-eyes.md %}).

#### Memory

Memory "insertion" and memory "deletion" or "blocking" in mouse studies. The great distance from where we are now to trivial memory "vacations" a la Recall. Semantic memory, autobiographical memory, and muscle memory.

#### Tracking devices

Ideal tracking-implantation locations. The incredible utility of the anus for tracker storage, weighed against the small time window.

#### Mars

Building Martian settlements with as much radiation exposure as possible: glass! glass everywhere! Radiation exposure on the Martian surface. Burying your dead on Mars: a tasteful resource-wasting mausoleum indoors, a giant pile of bleached-out mummies outside the door, or arrange them in awesome poses?

#### Space Resources

Natural resources in space. Calculating mining cost vs cost to get it where it will be useful. Mining and transporting Helium-3 for fusion energy. Harvesting oxygen from frozen water and getting rocket fuel as a bonus. NASA's [Moxie](https://mars.nasa.gov/mars2020/mission/instruments/moxie/) and inventing trees for _spaaaaace_. Making your Martian habitat smell like a dirty fish tank, for _science_.

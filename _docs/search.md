Search
=============

We use [Flexsearch.js] to do client-side search on our [search page].

The main components of this system are:

* The search index: `search/indexjs`
* Flexsearch.js: `_poca/assets/js/flexsearch.min.js`
* Our search page: `search/index.html`
* Our custom search.js that implements search w/ Flexsearch.js: `_poca/assets/js/search.js`

# Excluding pages from search

Any pages with `search_exclude` set to `true` will be excluded from the search index.

[Flexsearch.js]: https://github.com/nextapps-de/flexsearch[
[search page]: https://deciphermedia.tv/search/

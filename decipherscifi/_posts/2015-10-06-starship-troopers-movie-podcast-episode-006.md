---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: '2015-10-06 03:45:08+00:00'
description: "With Starship Troopers on the podcast we talk Heinlein, Paul Verhoven's youth under fascism, satire, society in constant war, propaganda, othering, giant insectoid aliens"
layout: podcast_post
permalink: /decipherscifi/starship-troopers-movie-podcast-episode-006
redirect_from:
  - /decipherscifi/6
slug: starship-troopers-movie-podcast-episode-006
title: 'Starship Troopers: scifi satire, gender equity, and insectoid alien attack'
tags:
  - alien
  - asteroid
  - future
  - insectoid alien
  - military
  - satire
  - science fiction
  - space
  - space combat

# Episode particulars
image:
  path: assets/imgs/media/movies/starship_troopers_1997.jpg
  alt: 'Starship Troopers backdrop'
links:
  - text: Starship Troopers - by Robert Heinlein
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/starship-troopers/id407581409?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'http://www.amazon.com/Starship-Troopers-Robert-Heinlein-ebook/dp/B004EYTK2C'
  - text: 'The Commonwealth Saga - by Peter F Hamilton'
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/commonwealth-saga-2-book-bundle/id889685260?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'http://www.amazon.com/Commonwealth-Saga-2-Book-Bundle-Unchained-ebook/dp/B00JOESE0Q'
  - text: 'World War II Propaganda Cartoons'
    urls:
      - text: 'Walt Disney: On the Front Lines'
        url: 'http://www.amazon.com/Walt-Disney-Treasures-Front-Lines/dp/B0000BWVAH'
      - text: 'Looney Toons Assorted Propaganda Cartoons'
        url: 'https://www.youtube.com/results?search_type=&search_query=warner+brothers+ww2+propaganda'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/starship-troopers/id263878598?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'https://www.amazon.com/gp/video/detail/B000MF4O82'
  title: Starship Troopers
  type: movie
  year: 1997
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: joe_ruppel
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: 
  number: 6
  guid: 'http://www.decipherscifi.com/?p=245'
  media:
    audio:
      audio/mpeg:
        content_length: 23816954
        duration: '33:04'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Starship_Troopers_decipherSciFi.mp3'
---
---


author: Christopher Peterson
layout: podcast_post
wordpress_id: 245
categories:
- Guest Co-Host
- Joe Ruppel
- Podcast
tags:
- Action
- Adventure
- army
- asteroid
- buenos aires
- Casper Van Dien
- Christopher Peterson
- Denise Richards
- Dina Meyer
- drill instructor
- intelligence
- Jake Busey
- Lee Colbert
- liberation of prisoners
- moon
- Nieil Patrick Harriss
- Paul Verhoven
- Science Fiction
- soldiers
- space battle
- space marine
- space ship
- Thriller
- total destruction
---
#### The Movie is a Satire of the Book

Heinlein wrote what amounted to military propaganda. Picture Heinlein as the teacher with the missing arm.

#### Verhoven Dislikes Militarism and Fascism

Not a surprise for someone who grew up in Nazi-occupied Holland. He thought the book was disgusting and wouldn't finish reading it - had someone else tell him what happened.

#### Society at Constant War

Propaganda. Othering. Resources all in to the military-industrial complex. Militarism. Fascism.

#### Insectoid Aliens and Intelligence

Hive minds. Brain straws. Pheromone communication. Intelligent bugs. Higher-oxygen atmosphere.

#### Future Tech

Exo-suits, insterstellar travel, shooting projectiles across the galaxy. Drones? Where are the drones?

#### Gender Equality

Exo-suits, from the book, would close the gap for war purposes. Co-ed showers. Norway and Israeli conscripting women.

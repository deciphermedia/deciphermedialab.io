---
layout: person_page
person_data:
  name: Scott Wayne Indiana
  data_name: scott_wayne_indiana
  image_path: assets/imgs/people/scott_wayne_indiana.png
  title: Artist
  bio: 'A really good dude! And always creating art, of whatever variety.'
  websites:
    - name: 39forks
      url: 'http://39forks.com/'
    - name: Lotus Dimension
      url: 'http://lotusdimension.com/'
  social:
    - name: twitter
      url: https://www.twitter.com/39forks
    - name: instagram
      url: https://www.instagram.com/39forks
  roles:
    - show: decipherscifi
      role: guest_cohost
---

---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2016-11-29 07:54:53+00:00 
description: "Nick Farmer, the linguist from The Expanse, comes back on to help us tackle the language aspects of Ted Chiang's Arrival! Sapir-Whorf, alien language, more" 
layout: podcast_post
permalink: /decipherscifi/arrival-part-2-feat-nick-farmer-linguist-episode-66
redirect_from:
  - /decipherscifi/66
slug: arrival-part-2-feat-nick-farmer-linguist-episode-66 
title: 'Arrival Part 2: debunking Sapir-Whorf hypothesis, alien phonology, and conlang writing w/ Nick Farmer' 
tags:
  - science fiction
  - science
  - language
  - linguistics
  - universe
  - time travel
  - writing
  - ted chiang

# Episode particulars
image:
  path: assets/imgs/media/movies/arrival_2016_pt2.jpg
  alt: Drawing weird inky three-dimensional symbols in the mist Arrival movie backdrop
links:
  - text: Everything by Ted Chiang
    urls:
      - text: Amazon
        url: 'https://www.amazon.com/Ted-Chiang/e/B001HCZ6OA'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/arrival/id1166395905?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Arrival-Amy-Adams/dp/B01MDTS4VZ'
  title: Arrival
  type: movie
  year: 2016
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: nick_farmer
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/arrival_2016_pt2.jpg
  number: 66
  guid: 'http://www.decipherscifi.com/?p=1169'
  media:
    audio:
      audio/mpeg:
        content_length: 38814702
        duration: '53:54'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Arrival_part_2_Nick_Farmer_Linguist_decipherSciFi.mp3'
---
#### Nick Farmer

He's a linguist! And a conlanger, technically.

His website: [Nick Farmer Linguist](http://www.nickfarmerlinguist.com/)

His Twitter: [@nfarmerlinguist](https://twitter.com/nfarmerlinguist)

#### Sapir-Whorf

AKA linguistic relativity, and why it is a very very wrong thing. A warm and fuzzy idea that turns out, after facing science, not to reflect reality well. Nick really really wants to set the record straight on this matter.

#### Spoken Alien Language

Considering the possibilities. Different modalities. Language need not be sound-based! ASL. Frequencies of sound.

#### Written Language

Heptapod B! Bending over backwards to figure out a way for alien languages to break the linearity of time.

#### Alien Writing System

The complexity of deciphering alien writing systems. Extradimensionality. Stephen Wolfram wrote the software to create the logograms. Human language analogies. Mapping ideas and words and concepts is REALLY complicated.

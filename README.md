Decipher Media Network Site
===========================

<img src="assets/imgs/site/readme_logo.800x.jpg" width="400">

[This repository] is the website (content, code, and documentation), for Decipher Media - media about how and why.

It uses [Jekyll] which is a Ruby-based static site generator. Posts are written in [Kramdown markdown]. Details follow and are split into separate documents where lengthy.

# TOC
<!-- vim-markdown-toc GFM -->

* [Getting Started](#getting-started)
  * [Project Setup](#project-setup)
* [Build](#build)
  * [Test](#test)
  * [CI](#ci)
* [Website](#website)
  * [Theme](#theme)
  * [Subsites](#subsites)
  * [Podcast feeds](#podcast-feeds)
  * [Posts](#posts)
    * [Body](#body)
    * [Frontmatter](#frontmatter)
    * [Images](#images)
  * [People](#people)
  * [Search](#search)

<!-- vim-markdown-toc -->

# Getting Started

This section outlines how to set up your computing environment to be able to mess with and test the site.  
You have two options for getting started:

## Docker
The fastest and easiest way to get started is using Docker.  
Once you have [Docker installed](https://www.docker.com/get-started), just run the following command from the source directory to build the docker image:

```Dockerfile
docker build --tag deciphermedia .
docker run -p 8080:4000 deciphermedia
```
You can replace `deciphermedia` with your tag of choice, and `8080` with your port of choice if you need it running on a different port.  
You'll know it's ready when you see the text `Server running... press ctrl-c to stop.`
Then just head over to [http://localhost:8080/](http://localhost:8080/) and the website will be up and running.

It is recommended to work and make changes directly in the container, either connecting using `docker run -it deciphermedia /bin/bash` or using [Visual Studio Code's Docker extension](https://code.visualstudio.com/docs/remote/containers-tutorial) (or something similar).

## Manual Setup

### Prerequisites

* [Ruby and bundler](_docs/ruby.md) (as above)
* Git
* The ImageMagick C libraries (for the responsive-image Jekyll plugin)
	- OSX: ??
	- Ubuntu: `sudo apt-get install libmagickwand-dev`

### Setting Up
```sh
# Clone repo
git clone gitlab.com:deciphermedia/deciphermedia.gitlab.io.git
cd deciphermedia.gitlab.io.git
# Install ruby dependencies
bundle install

# This will run a test server @ http://localhost:4000 by default and output on any warnings or errors
bundle execute jekyll serve
# OR this will simply build the site and report any errors or warnings
bundle execute jekyll build
```

### Build

As a bare minimum, you can see if the site builds successfully:

```sh
bundle exec jekyll build
```

Or serve it live locally:

```sh
bundle exec jekyll serve
# will by default serve http://localhost:4000/
```

### Test

Our build stages are:

* `build`
* `test`
* `deploy`
* `clean`

Run`./build` with any number of these jobs listed as arguments. It will build the site into `./public/` and the `clean` job can tidy it up afterwards if you'd like.

Examples:

```sh
# Build the site and test it
./build build test

# Test the site after the build job has already created the site in `./public`
./build test

# Run the entire routine and clean up
./build all
```

# CI

We use gitlab-ci to build, test, and deploy the production site with the jobs [documented above](#test). The configuration for this is in `.gitlab-ci.yml`.

For simply testing the codebase, running the tests locally above is easier, BUT in case of diagnosing a failure that only occurs during the build on gitlab it could be useful to replicate their build environment locally.

So in order to run the gitlab-runner locally on *committed* code (uncommitted changes are ignored!), just like on gitlab, do the following:

*NOTE*: you'll need docker set up and your user allowed to operate the service. That is all beyond the scope of this document.

```sh
# sudo apt update && sudo apt install docker.io
# sudo systemctl start docker
# sudo usermod -aG docker $USER
cd /path/to/project/with/gitlab/ci
docker run --rm -t -i \
         --volume /var/run/docker.sock:/var/run/docker.sock \
         --volume $PWD:$PWD \
          --workdir $PWD \
         gitlab/gitlab-runner:latest \
         exec docker \
              --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
              --docker-pull-policy if-not-present \
              --env "HOSTNAME=$HOSTNAME" \
              --env "CI_PROJECT_NAMESPACE=$(basename $PWD)" \
              --env "CI_PROJECT_NAME=$(basename $PWD)" \
              stage::all
```

# Website

## Theme

We have adapted the [Poca] theme from Colorlib as a Jekyll gem-based theme.

Here is how you can rebuild its dependencies (fonts, javascript, scss, etc): [theme docs]

## Subsites

Our individual shows are referred to as "subsites" in the site code, and we could technically also host other "subsites" that do not serve any podcasts if we choose to in the future.

They are made visually distinct by their styling, as sourced from a collection of values in the `defaults` hash in `_config`. We do it here so that the values can be set hierarchically per site/subsite.

The values that can vary:

* `background`: the background color of the header area.
* `highlight`: the color of elements like body anchors or hover on navigation.
* `text`: the color of text in the header/nav area.
* `submenu`: the color of the submenu backgrounds, alterable as it must contrast against `background`.

## Podcast feeds

And since this is a site for podcasts, let's not forget those!

We wrote and use our own `jekyll-podcast` plugin to create our feeds, and they are intended to be served off of this very site.

[jekyll-podcast docs]

## Posts

Start a post by copying a template from `_docs/templates`:

* `_docs/templates/episode-decipherscifi.md` -> `decipherscifi/_posts/YYY-MM-DD-title-episode-number.md`
* `_docs/templates/episode-decipherhistory.md` -> `decipherhistory/_posts/YYY-MM-DD-title-episode-number.md`

### Body

Write the body in Markdown! A good guide: [Markdown].

And for Vim you should really also use [plasticboy/vim-markdown] because it does a ton to make markdown go more smoothly. But that's just a bonus.

### Frontmatter

This is alllllll of the stuff that goes between the two `---` lines at the top of the post. It specifies all of the metadata that defines a specific page/post/episode - post type, featured image, movie/etc/whatever title, people, etc. Jekyll's own [frontmatter] go into some more detail.

When creating a new post from our templates, everything necessary should probably already be there just waiting to be filled in.

### Images

[Images documentation]

Images on this site are, in general, expected to be included in a document with licensing and accessibility information. As per the docs above.

## People

People: we have those! Quite a few even, once you lump the guest co-hosts in with the actual hosts. Each person is defined by their metadata. The people metadata shows up in the show pages and also is used to dynamically generate the individual people pages.

[People documentation]

## Search

We provide client-side javascript search via flexsearch.js on our search page.

[Search documentation]


[Conventional Commits]: https://www.conventionalcommits.org/en/v1.0.0/#summary
[Images documentation]: _docs/images.md
[Jekyll]: https://jekyllrb.com/
[Kramdown markdown]: https://kramdown.gettalong.org/syntax.html
[Markdown]: https://guides.github.com/features/mastering-markdown/
[People documentation]: _docs/people.md
[Poca]: https://colorlib.com/wp/template/poca/
[Ruby]: _docs/ruby.md
[Search documentation]: _docs/search.md
[This repository]: https://gitlab.com/deciphermedia/deciphermedia.gitlab.io
[frontmatter]: https://jekyllrb.com/docs/front-matter/
[jekyll-podcast docs]: _docs/jekyll-podcast.md
[plasticboy/vim-markdown]: https://github.com/plasticboy/vim-markdown
[theme docs]: _docs/theme.md

---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - video game
date: 2020-02-04 07:45:27+00:00
description: "The importance of Halo in FPS history. Nostalgia. Ring worlds. Halo's answer to the Fermi paradox: a great filter. Power armor and super soldiers. Etc."
layout: podcast_post
permalink: /decipherscifi/halo-first-contact-power-armor-and-how-does-master-chief-poop-w-daniel-barker
redirect_from:
  - /decipherscifi/227
slug: halo-first-contact-power-armor-and-how-does-master-chief-poop-w-daniel-barker
title: 'Halo: first contact, power armor, and how does master chief poop? w/ Daniel Barker'
tags:
  - armor
  - drake equation
  - evolution
  - fermi paradox
  - first contact
  - kardashev
  - ring worlds
  - science
  - science fiction
  - weapons technology

# Episode particulars
image:
  path: assets/imgs/media/videogames/halo_2001.jpg
  alt: 'Master Chief looking pretty epic'
links:
  - text: 'Halo Foward Unto Dawn'
    urls:
      - text: 'Amazon'
        url: 'https://www.amazon.com/Halo-4-Forward-Unto-Dawn/dp/B07FJMQ15Q'
  - text: 'Uncertainty Principle'
    urls:
      - text: 'UncertaintyPrincipleThePodcast.com'
        url: 'https://uncertaintyprinciplethepodcast.com/'
media:
  links:
    - text: Steam
      url: 'https://store.steampowered.com/app/976730/Halo_The_Master_Chief_Collection/'
    - text: Amazon
      url: 'https://www.amazon.com/Halo-Master-Chief-Collection-Xbox-One/dp/B00KSQHX1K'
  title: Halo
  type: video_game
  year: 2001
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: daniel_barker
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/videogames/halo_2001.jpg
  number: 227
  guid: 'https://decipherscifi.com/?p=10923'
  media:
    audio:
      audio/mpeg:
        content_length: 40208552
        duration: '47:45'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/halo.output.mp3'

---
#### Importance

Nostalgia: wow Halo has really been around for a minute. Hundreds of hours of that one level of local multiplayer but never playing the story. Turning an RTS into a TPS and then an FPS and then getting that Microsoft money.

Figuring out the importance of Halo CE in the history of gaming. PC gaming master race. Tank-controlled console FPS games. Unseating Goldeneye. Establishing a timeline of FPS quality on consoles and PC.

#### First Contact

Humanity honing its warfare skills by just being jerks to other humans. Settling 800+ planets! “Glassing” enemy alien planets.

#### Put a ring on it

Ringworlds! But less a ring “world” and more a ring “weapons platofrm that also is fairly liveable.” The Drake Equation and the Fermi Paradox through the lens of Halo.

#### Armor

Master Chief vs Doom Guy. Measuring Hell energy. Mjolnir armor - a half ton and costs as much as [a destroyer](https://www.halopedia.org/UNSC_destroyer) to produce. “Smart” and “dumb” AI delineations. Neural exoskeleton integration. PRedictive algorithms. Relating Master Chief to [Upgrade]({% link decipherscifi/_posts/2018-09-25-upgrade-biohacking-ai-containment-and-paralyzation-workarounds.md %}). What about just putting an AI and some batteries inside of the Mjolnir armor? Falling from space.


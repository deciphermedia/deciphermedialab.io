---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2016-10-25 07:45:02+00:00
description: 'Cae and Ryan join us and teach us about how storytelling works. Thing tests. Antarctic research and exploration. Deep dive on nature of consciousness, more'
layout: podcast_post
permalink: /decipherscifi/carpenters-the-thing-feat-cae-ryan-episode-61
redirect_from:
  - /decipherscifi/61
slug: carpenters-the-thing-feat-cae-ryan-episode-61
title: 'The Thing: antarctic explorers, extreme doctoring, and figuring out aliens w/ Cae & Ryan'
tags:
  - science fiction
  - science
  - john carpenter
  - monsters
  - aliens
  - paranoia
  - medicine
  - antarctica

# Episode particulars
image:
  path: assets/imgs/media/movies/the_thing_1982.jpg
  alt: Snow-suited man with light exploding from face The Thing movie backdrop
links:
  - text: The Things by Peter Watts
    urls:
      - text: Clarksworld Podcast
        url: 'http://clarkesworldmagazine.com/audio_01_10/'
  - text: The Politics of Experience & The Divided Self by R.D. Laing
    urls:
      - text: Amazon
        url: 'https://www.amazon.com/R-D-Laing/e/B001IU2QRG'
  - text: Lee Hardcastle's The Thing Claymations
    urls:
      - text: Thingu @ YouTube
        url: 'https://www.youtube.com/watch?v=QAoONl2P8fw'
      - text: Claycat's The Thing @ YouTube
        url: 'https://www.youtube.com/watch?v=BG33zECv8dc'
      - text: Frozen blood test scene @ YouTube
        url: 'https://www.youtube.com/watch?v=dooAjI6yOhg'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/the-thing/id282249286?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'https://www.amazon.com/Thing-Kurt-Russell/dp/B002H0YRGO'
  title: The Thing
  type: movie
  year: 1982
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: ryan_bingham
    roles:
      - guest_cohost
  - data_name: caelum_rale
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/the_thing_1982.jpg
  number: 61
  guid: 'http://www.decipherscifi.com/?p=1076'
  media:
    audio:
      audio/mpeg:
        content_length: 48919357
        duration: '58:14'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/John_Carpenters_The_Thing_Caelum_Rale_Ryan_Bingham_decipherSciFi.mp3'
---
#### Movie origins

Who Goes There? short story. Previous adaptations: The Thing From Another World (1951) · Horror Express (1972) · The Thing (2011)

#### Antarctic Exploration

British and Norwegian missions. Cultures and approaches.

#### Antarctic Research

Conditions. Connectivity. Boredom. Whiskey, cigars, and shoe leather.

#### Antarctic Doctoring

You need a certain kinda Dr MacGyver badass for this job. Spinal self-diagnosis, etc. Wilford Brimley diabeetus.

#### John Carpenter cinematography and soundtracks and storytelling

Cae and Ryan understand these things, and explain them to us. We mostly get it.

#### The Thing

So many questions. Where did it come from? How does it work? The nature of the creature. Is it intelligent?

#### The nature of consciousness

If you were The Thing, could you know you were The Thing? Would The Thing know it was The Thing? Self interest in all cases. Group interests in all cases.

#### Infection

If I can't win, neither can you. The doctor was the only one acting rightly. Best plan: destroy the base and kill everyone, just in case. Sexual tests as alternatives to blood...

#### "The Things"

> I am being Blair. I escape out the back as the world comes in through the front.
> I am being Copper. I am rising from the dead. 
> I am being Childs. I am guarding the main entrance.
> The names don't matter. They are placeholders, nothing more; all biomass is interchangeable. What matters is that these are all that is left of me. The world has burned everything else.
> <cite>The Things by Peter Watts</cite>

---
layout: page
redirect_from:
  - /decipherscifi/thank-you-for-subscribing/
search_exclude: true
title: Thank you for subscribing!
---

We're really excited to have you in this special group of our most engaged listeners.

{% youtube "https://www.youtube.com/watch?v=g3rFNbSKpEE" %}

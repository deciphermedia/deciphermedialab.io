---
# General post frontmatter
categories:
  - episode
  - interview
  - topic
  - tv
date: 2017-01-27 06:16:50+00:00 
description: 'An interview with Cailin Munroe, VFX Producer on The Expanse: diaper butts, ship designs, physics FX, UI, matte paintings, 3D, the next form of the blue goo' 
layout: podcast_post
permalink: /decipherscifi/decipherscifi-is-people-01-the-expanse-vfx-w-cailin-munroe
redirect_from:
  - /decipherscifi/i1
slug: decipherscifi-is-people-01-the-expanse-vfx-w-cailin-munroe 
title: 'The Expanse VFX: an interview with Cailin Munroe, VFX Producer' 
tags:
  - science fiction
  - science
  - vfx
  - the expanse

# Episode particulars
image:
  path: assets/imgs/media/tv/the_expanse_2015_vfx.jpg
  alt: Cailin Munroe VFX supervisor superimposed over a scene from The Expanse
links:
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/tv-season/the-expanse-season-1/id1059088673?mt=4&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Dulcinea/dp/B018BZ3SCM'
  title: The Expanse
  type: tv
  year: 2015
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: cailin_munroe
    roles:
      - guest
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/tv/the_expanse_2015_vfx.jpg
  guid: 'http://www.decipherscifi.com/?p=1293'
  media:
    audio:
      audio/mpeg:
        content_length: 20930560
        duration: '29:03'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/The_Expanse_VFX_--_Cailin_Munroe_--_decipherSciFi.mp3'
---
# "Decipher SciFi is People"

We did an interview! Since it's not the normal format, we're releasing this as a bonus halfway between normal episodes.

We talk here with Cailin Munroe from The Expanse all about a bunch of stuff: the role of the producer, on-set work, diaper butts, ship designs, physics FX, device and environment UI, space hair, dogs barking, matte paintings, 3D animated sets, the next form of the blue goo ☠️

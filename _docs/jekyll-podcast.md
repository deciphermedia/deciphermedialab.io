Jekyll-Podcast
===============

(TODO: these docs. some notes follow.)

the cover image path is produced by an include so that I can get a thumbnail etc dynamically

_includes/podcast_episode_player_coverimage_path.html
default should be: {{ site.url }}{{ cover_image }}

You will need to set the 'show' key in the podcast options in _config AS WELL AS include the field in each episode's frontmatter. Recommended: do this with frontmatter defaults.
page must specify the audio format as well since you could have multiple of the same feed just with different formats

```yaml
# per post
excerpt: 
show: showname
chapters:
  - start: '00:00:00'
    title: intro

podcast:
  debug: 
  description: 
  episode:
    author: 
    block:
    cover_image: 
    guid: 
    itunes:
      episodetype: 
    media:
      audio:
        audio/mpeg:
          content_length: 
          duration: 
          explicit: 
          url: 
    number: 
    season: 
    subtitle: 
    title: 
    type: 
```


TODO

get rid of rawvoice stuff who cares
ensure deprecated itunes tags removed
ensure new 2019 itunes tags included
allow custom episode image. rn it just uses the shwo image
remove googleplay handling

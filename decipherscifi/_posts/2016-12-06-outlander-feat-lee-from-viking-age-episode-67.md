---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2016-12-06 07:45:35+00:00
description: 'Lee from Viking Age joins us for some viking scifi: Outlander! Alien colonies, viking language, ocular data input, warrior culture, more'
layout: podcast_post
permalink: /decipherscifi/outlander-feat-lee-from-viking-age-episode-67
redirect_from:
  - /decipherscifi/67
slug: outlander-feat-lee-from-viking-age-episode-67 
title: 'Outlander: viking history, the Prometheus problem, and bioluminescence w/ Lee Accommando'
tags:
  - science fiction
  - science
  - bioluminescence
  - whaling
  - warriors
  - aliens
  - vikings

# Episode particulars
image:
  path: assets/imgs/media/movies/outlander_2008.jpg
  alt: A roughly pyramidal viking pile Outlander movie backdrop
links:
  - text: Viking Age Podcast
    urls:
      - text: Apple Podcasts
        url: 'https://itunes.apple.com/us/podcast/viking-age-podcast/id1102929069'
      - text: RSS
        url: 'http://vikingagepod.libsyn.com/rss'
  - text: Beowulf the movie
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/movie/beowulf/id274059405?mt=6&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Beowulf-Ray-Winstone/dp/B0012SCHZK'
  - text: Beowulf translated by Seamus Heeney
    urls:
      - text: Amazon
        url: 'https://www.amazon.com/Beowulf-New-Verse-Translation-Bilingual/dp/0393320979'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/outlander/id308182427?mt=6'
    - text: Amazon
      url: 'https://www.amazon.com/Outlander-James-Caviezel/dp/B0093QIEZA'
  title: Outlander
  type: movie
  year: 2008
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: lee_accomando
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/outlander_2008.jpg
  number: 67
  guid: 'http://www.decipherscifi.com/?p=1189'
  media:
    audio:
      audio/mpeg:
        content_length: 29465706
        duration: '40:55'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Outlander_Lee_Accomando_decipherSciFi.mp3'
---




---
#### Alien Seed Colony

Human aliens in Norway c 709 A.D. The "Prometheus Problem."

#### Viking Language

Old norse and 12th century Icelandic. The Icelandic sagas.

#### Ocular Data Input

Brain implants, language learning, and nosebleeds.

#### Whaling

Whales are big!

#### Warrior Culture

Berserkers and their magic mushrooms. Specific training methodologies between vikings, space marines, and dragon-fighting.

#### The Monster

The movie turns into Predator. Elephant-lion-crocodiles. Apex predator population scales.

#### Bioluminescence

Frequency of bioluminescence. Adaptation. Wavelengths. Is bioluminescence expensive?

---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2016-09-20 07:45:48+00:00
description: "With Planet of the Apes we discuss Charlton Heston's jerkiness, that animals are more intelligent than they get credit for, time dilation, language and more"
layout: podcast_post
permalink: /decipherscifi/planet-apes-podcast-episode-56
redirect_from:
  - /decipherscifi/56
slug: planet-apes-podcast-episode-56
title: 'Planet of the Apes : Episode 56'
tags:
  - science fiction
  - science
  - evolution
  - primate intelligence
  - linguistics
  - language
  - time travel
  - time dilation
  - animal intelligence

# Episode particulars
image:
  path: assets/imgs/media/movies/planet_of_the_apes_1968.jpg
  alt: Humans enslaved by apes Planet of the Apes movie backdrop
links:
  - text: Sapiens by Yuval Noah Harari
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/sapiens/id819198260?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Sapiens-Humankind-Yuval-Noah-Harari-ebook/dp/B00ICN066A'
  - text: Are We Smart Enough to Know How Smart Animals Are? by Frans de Waal
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/are-we-smart-enough-to-know/id1047342176?mt=11&at=1001l7hP'
      - text: Amazon
      - url: 'https://www.amazon.com/Are-Smart-Enough-Know-Animals-ebook/dp/B016APOCRA'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/planet-of-the-apes-1968/id435011890?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'https://www.amazon.com/Planet-Apes-Charlton-Heston/dp/B0053EZWZQ'
  title: Planet of the Apes
  type: movie
  year: 1968
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/planet_of_the_apes_1968.jpg
  number: 56
  guid: 'http://www.decipherscifi.com/?p=1019'
  media:
    audio:
      audio/mpeg:
        content_length: 28463774
        duration: '47:25'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Planet_of_the_Apes_decipherSciFi.mp3'
---

{% responsive_image path: assets/imgs/selfies/planet_of_the_apes.jpg alt: "Decipher Media Hosts in Planet of the Apes" caption: "Just a normal day in the studio" %}

#### Relativity time travel

Jerky Charlton Heston. 99.999995% the speed of light. [Time dilation](https://en.wikipedia.org/wiki/Time_dilation).

#### The Mission

Settle? Explore? Three men and one women on a colonization mission. Charlton Heston is a bad person.

#### Moon on the brain

Exploding moons, broken moons, moon impacts. Callbacks: [Cowboy Bebop]({% link decipherscifi/_posts/2016-09-13-cowboy-bebop-movie-podcast-episode-55.md %}), [Seveneves]({% link decipherscifi/_posts/2016-08-09-seveneves-feat-daniel-mann-software-pro-episode-50.md %}), Deep Impact

#### Language

Biology of speech. Cognitive speech development. Language evolution.

{% youtube "https://www.youtube.com/watch?v=CJkWS4t4l0k" %}

#### Animal Intelligence

Octopuses, corvids, elephants, various apes. Mostly chimps. Tool use, language. Moving the goalposts.

{% youtube https://www.youtube.com/watch?v=xNDvp5FovhQ %}

---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2018-09-18 07:45:07+00:00
description: 'Black Panther science! Invisible cities. Remote vehicle piloting. Afrofuturism. Vibranium impactors and material peroperties. War rhinos. Click language.'
layout: podcast_post
permalink: /decipherscifi/black-panther-rhino-domestication-click-languages-and-killmonger-overheating
redirect_from:
  - /decipherscifi/159
slug: black-panther-rhino-domestication-click-languages-and-killmonger-overheating
title: 'Black Panther: rhino domestication, click languages, and Killmonger overheating'
tags:
  - afrofuturism
  - armor
  - invisiblity
  - language
  - linguistics
  - monarchy
  - science
  - science fiction
  - self-driving vehicles
  - teleportation

# Episode particulars
image:
  path: assets/imgs/media/movies/black_panther_2018.jpg
  alt: 'Black Panther, his arms wide.'
links:
  - text: 'Decipher RPG part 1'
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/rpg1'
  - text: 'Decipher RPG part 2'
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/rpg2'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/black-panther-2018/id1342065788?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Panther-Theatrical-Version-Chadwick-Boseman/dp/B079NKRK66'
  title: Black Panther
  type: movie
  year: 2018
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/black_panther_2018.jpg
  number: 159
  guid: 'https://decipherscifi.com/?p=7721'
  media:
    audio:
      audio/mpeg:
        content_length: 33976002
        duration: '40:26'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Black_Panther_--_--_Decipher_SciFi.mp3'
---
#### Afrofuturism

Wishing there were more of this in popular media.

#### Invisible cities

A review of invisibility technology: spatial, temporal, and spectral light cloaking. Vibranium to the rescue.

#### Vibranium

Space impactors. Resource

#### Armor

Body armor, stationary armor. Avoiding concussive force. Reactive armor in tanks: shaped explosives that explode at the other thing that's gonna explode! Molecular springs of carbon for storage of mechanical energy from kinetic. How not to turn into Black Panther jelly when the suit releases its energy (be superhuman).

#### Absolute monarchies

Bas Rutten for king. _Right hoooook_.

#### Scars and sweat

Killmonger's scarification and scar damage to the dermis. [Sweat patterns around scars](https://www.reddit.com/r/mildlyinteresting/comments/8ox6nr/my_scars_dont_seem_to_sweat/).

#### Telepiloting

Remote vehicle control. Light-speed communications delays. AI-assisted remote human driving.

#### War Rhinos

Domestication vs trainability. Extinct war elephants and why we can't have war rhinos. Dürer's rhinoceros, drawn from notes on the first rhino seen in Europe since Roman times.

{% responsive_image path: "assets/imgs/misc/durer_rhinoceros.jpg" alt: "Dürer's Rhinoceros" caption: "Dürer's Rhinoceros" attr_text: "CC-0" attr_url: "https://commons.wikimedia.org/wiki/File:Albrecht_D%C3%BCrer_-_The_Rhinoceros_(NGA_1964.8.697).jpg" %}

#### Click language

Xhosa! Bantu languages. The possibility of a connection to the earliest human languages.

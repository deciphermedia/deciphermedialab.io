---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2018-08-07 06:13:40+00:00
description: 'When movies give you cynicism, talk about: waste management, dumps vs landfills, plastic appreciation, and solving the plastic problem. And human evolution!'
layout: podcast_post
permalink: /decipherscifi/wall-e-biodegradable-plastics-waste-management-and-space-cattle
redirect_from:
  - /decipherscifi/154
slug: wall-e-biodegradable-plastics-waste-management-and-space-cattle
title: 'Wall-E: biodegradable plastics, waste management, and space cattle'
tags:
  - evolution
  - plastic
  - science
  - science fiction
  - waste management

# Episode particulars
image:
  path: assets/imgs/media/movies/wall_e_2008.jpg
  alt: 
links:
  - text: The Water Fountain You Pee In
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=6Ha5Rs2r4F0'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/wall-e/id286533539?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Wall-Fred-Willard/dp/B0094KTBBG'
  title: Wall-E
  type: movie
  year: 2008
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/wall_e_2008.jpg
  number: 154
  guid: 'https://decipherscifi.com/?p=7285'
  media:
    audio:
      audio/mpeg:
        content_length: 29419920
        duration: '35:00'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Wall-E_--_--_Decipher_SciFi.mp3'
---

#### Space settlement

The economics of leaving Earth. Settling the inner and outer solar system. [The Expanse]({% link decipherscifi/_posts/2017-06-06-the-expanse-season-2-feat-fraser-cain-episode-93.md %}). Space cattle and other space resources (water, mineral).

#### Garbage - in spaaace!

Garbage in space aka space junk. Cleaning up space junk. Giant nets as a major cleanup strategy.

#### Garbage - on Eaaaarth!

Modern waste management. What to do with municipal solid waste. Leachate. The difference between dumps and landfills and the interesting energy-reclamation technology involved in cutting edge WTE facilities.

#### Plastic

Massive plastic appreciation. How plastics ushered in the modern world. A lament for wasteful use of "disposable" plastic products. Microplastics in the water. Biodegradable plastics. The discovery of plastic-eating bacteria. Speeding up nature to digest plastic _now_. The coming plague of plastic-eating bacteria.

#### "De-evolution"

This isn't a thing! There's no end point that is being retreated from - life just adapts to pressures, ongoing.

#### Handling failure

Revisiting our past failures. Combating cynicism by just kind of ignoring it.

---
# General post frontmatter
author: christopher_peterson
layout: post
date: 2018-05-04 01:19:17+00:00
slug: gar-an-animation-with-batch-25-comics 
title: 'GAR - an animation with Batch 25 Comics' 

---
This is not scifi (though much of his work *is*), but I recently published an animation I did from a comic by our friend Andy P of [Batch 25 Comics](https://batch25comics.com/).

I found [the comic](https://batch25comics.com/3-stories/) touching and I hope you enjoy this video as much as I did making it. Here it is!

{% youtube "https://www.youtube.com/watch?v=TyWLLFf40XQ" %}

<br>
I hope you'll take a look at the comic too, and the rest of his work because I think it's all really neat. 😁

---
layout: person_page
person_data:
  name: 'Isaac Meyer'
  data_name: 'isaac_meyer'
  bio: 'Host of multiple history-centric podcasts and just generally cool guy'
  image_path: assets/imgs/people/isaac_meyer.png
  title: 'Historian, teacher, podcaster'
  websites:
    - name: Isaac Meyer
      url: 'http://isaacmeyer.net/'
    - name: History of Japan Podcast
      url: 'http://isaacmeyer.net/category/podcasts/history-of-japan-podcast/'
    - name: Criminal Records Podcast
      url: 'https://demetriaspinrad.com/criminal-records-podcast/'
  social:
    - name: twitter
      url: 'https://twitter.com/giantEnemy_Crab/'
  roles:
    - show: decipherhistory
      role: guest_cohost

---


---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - topic
date: 2020-01-07 07:45:11+00:00
description: "A decadal science review and a look at back at what we've learned on the show. Privacy (or lack thereof), cyberpunk, machine learning, space, physics, etc."
layout: podcast_post
permalink: /decipherscifi/decade-in-review-privacy-computing-everything-and-torture-toilets
redirect_from:
  - /decipherscifi/223
slug: decade-in-review-privacy-computing-everything-and-torture-toilets
title: 'Decade in Review: privacy, computing everything, and torture toilets'
tags:
  - artificial intellignece
  - cyberpunk
  - cyberpunk
  - evolution
  - free software
  - machine learning
  - physics
  - privacy
  - science
  - science fiction
  - technology
  - wearables

# Episode particulars
image:
  path: assets/imgs/media/misc/decadal_review.jpg
  alt: 'Decipher SciFi jar-brain science logo'
links:
media:
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/misc/decadal_review.jpg
  number: 223
  guid: 'https://decipherscifi.com/?p=10899'
  media:
    audio:
      audio/mpeg:
        content_length: 36713759
        duration: '43:36'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/decadal_review.output.mp3'

---
No spoilers this week! Just looking at all the scientific discoveries of the past decade and how that has or hasn't played into the scifi we've covered.

#### 1. Privacy & Cyberpunk

... and the illusion thereof. Snowden and the shattering of the _illusion_ of privacy in the digital age. The world becoming the less awesome-looking irl version of cyberpunk. Workers as a resources. Torture toilets.

#### 2. Machine learning

The biggest huge development in the pats decade which underlies _every other things_. Wowzers. Realizing the importance of the data going _in_, so you don't get [racist policing](https://theappeal.org/the-truth-about-predictive-policing-and-race-b87cf7c070b1/) or [image recognition](https://www.wired.com/story/when-it-comes-to-gorillas-google-photos-remains-blind/). The availability of open tools, compute time, and learning resources.

#### 3. Genetic editing

[CRISPR](https://en.wikipedia.org/wiki/Crispr)! What will eventually result from this one?

#### 4. Space

Water on Mars! So much more water than we thought. Greater accuracy in portrayals of space settlement. Increasing private industry in space.

#### 5. Physics

Hey, an update on Einstein: still correct! Also... Gravity waves! Higgs-Boson! Imaging _a black hole_. The contributions of computing and storage technologies.

#### 39. Human history

Confirmation of Sapiens and Neanderthal interbreeding in remaining modern humans. Discovering more extinct human varieties.

#### 53. Technology

What happened to AR glasses!? The state of wearables. Recognizing that there are senses to augment _other_ than our visual system.

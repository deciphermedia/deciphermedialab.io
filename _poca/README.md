Poca HTML5 Template by ColorLib
=========================================

This theme is a modification/adaptation of an existing HTML5 template for use with Jekyll.

The original: [Poca] by Colorlib.

[Poca]: https://colorlib.com/wp/template/poca/

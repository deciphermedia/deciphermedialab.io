---
# General post frontmatter
categories:
  - episode
  - bonus
date: 2019-10-22 04:36:59+00:00
layout: podcast_post
permalink: /decipherscifi/on-decipher-history-the-patriot
redirect_from:
slug: on-decipher-history-the-patriot
title: 'On Decipher History: The Patriot'
tags:
  - science fiction
  - science

# Episode particulars
image:
  path: assets/imgs/media/movies/the_patriot_2000.decipherhistory_promo.jpg
  alt: Mel Gibson being patriotic with History-Brain
links:
media:
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/the_patriot_2000.decipherhistory_promo.jpg
  number: 
  guid: 'https://decipherscifi.com/?p=10828'
  media:
    audio:
      audio/mpeg:
        content_length: 1450639
        duration: '01:43'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/the_patriot.dsf_promo.mp3'

---
The taste of another episode of Decipher History! Featuring some over-the-top colonial heroism, etc

[The Patriot: heroism, invisible slavery, and cutting-edge chair technology](https://deciphermedia.tv/decipherhistory/6)

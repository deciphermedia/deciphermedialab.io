---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2016-11-01 07:46:53+00:00
description: 'Joe Ruppel joins again to get into victorian medicine, vivisection, society in decline, Marlon Brando/Val Kilmer, transhumanism, "individual plasticity"' 
layout: podcast_post
permalink: /decipherscifi/island-of-dr-moreau-jeo-ruppel-movie-podcast-episode-62
redirect_from:
  - /decipherscifi/62
slug: island-of-dr-moreau-jeo-ruppel-movie-podcast-episode-62 
title: 'The Island of Dr Moreau: vivisection, individual plasticity, and the human animal w/ Joe Ruppel'
tags:
  - science fiction
  - science
  - hg wells
  - history
  - victorian period
  - transhumanism
  - medicine
  - gene splicing
  - animals
  - playing god

# Episode particulars
image:
  path: assets/imgs/media/movies/the_island_of_dr_moreau_1996.jpg
  alt: Marlon Brando and VBal Kilmer heads and another Marlon Brando in the middle but creepier and with a messed up aspect ratio
links:
  - text: 'Frankenstein: science fiction horror, reanimation, and victorian context w/ Joe Ruppel'
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/29'
  - text: The Limits of Individual Plasticity by H.G. Wells
    urls:
      - text: Google Books
        url: 'https://books.google.com/books?id=TQ8eByH9QXsC&pg=PA36&lpg=PA36&dq=hg+wells+limit+of+individual+plasticity&source=bl&ots=eZdkPFVrfV&sig=0DdRCXM06OZOiIv1F_iyw7CFKN4&hl=en&sa=X&ved=0ahUKEwionpyCgpjPAhUhzoMKHYvsCrUQ6AEIJTAB#v=onepage&q=hg%20wells%20limit%20of%20individual%20plasticity&f=false'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/the-island-of-dr.-moreau-1996/id718304628?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'https://www.amazon.com/Island-Moreau-Unrated-Directors-Blu-ray/dp/B007VI4TKQ'
  title: The Island of Dr. Moreau
  type: movie
  year: 
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: joe_ruppel
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/the_island_of_dr_moreau_1996.jpg
  number: 62
  guid: 'http://www.decipherscifi.com/?p=1105'
  media:
    audio:
      audio/mpeg:
        content_length: 35966424
        duration: '49:56'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/The_Island_of_Dr_Moreau_Joe_Ruppel_decipherSciFi.mp3'
---

{% responsive_image path: assets/imgs/selfies/the_island_of_dr_moreau.jpg alt: "Decipher SciFi co-hosts creepily being either turned into Marlon Brando or an animal-person" caption: "what" %}

#### Vivisection

Definitions. Victorian science and protest movements. John Hunter and the John Byrne Irish Giant. Behaviourist views of animal feelings.

#### Humans as an animal

Coming to terms with our place in nature in Wells' time. Natural selection vs selection by Moreau.

#### Individual plasticity and heredity

More vivisection. Human body modification and evolution.

#### Religious analogy

The "father" and the "laws."

#### Should we do _x_?

Chris plays the amoral scientist monster every time we ask this question.

#### Carnivore vs herbivore

Moral of the story: Moreau really should have bred a race of herbivores. Would have side-stepped a lot of issues.

#### Transhumanism

Strategies in evolving humanity and combining with our replacement life form.

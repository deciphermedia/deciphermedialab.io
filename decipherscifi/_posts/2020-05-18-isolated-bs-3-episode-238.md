---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - conversation
date: 2020-05-19T00:02:03-04:00
description: 'Coffee. Always. Ham nerds finding zombie satellites. The ultimate ruins of human civilization in space. Bad pranks. The upside of racoon attacks. Etc.'
layout: podcast_post
redirect_from:
  - /decipherscifi/238
slug: isolated-bs-ancient-tattoos-zombie-satellites-running-sandals
title: 'Isolated BS part 3: ancient tattoos, zombie satellites, and running sandals'
tags:
  - ancient civilization
  - coffee
  - food
  - satellites
  - science
  - science fiction
  - space
  - tattoos

# Episode particulars
image:
  path: assets/imgs/media/misc/isolated_bs_3.jpg
  alt: 'The Decipher SciFi and Decipher History brains as earth satellites. CC-BY. Based on CC-0 USAF VIRIN 150415-F-AS483-002.JPG'
links:
  - text: 'Long-Lost U.S. Military Satellite Found By Amateur Radio Operator'
    urls:
      - text: NPR
        url: 'https://www.npr.org/2020/04/24/843493304/long-lost-u-s-military-satellite-found-by-amateur-radio-operator'
  - text: 'The Wolrd Above Us - every active satellite orbiting Earth'
    urls:
      - text: QZ
        url: 'https://qz.com/296941/interactive-graphic-every-active-satellite-orbiting-earth/'
  - text: 'Siberian Princess reveals her 2,500 year old tattoos'
    urls:
      - text: The Siberian Times
        url: 'http://siberiantimes.com/culture/others/features/siberian-princess-reveals-her-2500-year-old-tattoos/'
media:
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/misc/isolated_bs_3.jpg
  number: 238
  guid: /decipherscifi/isolated-bs-ancient-tattoos-zombie-satellites-running-sandals
  media:
    audio:
      audio/mpeg:
        content_length: 26085548
        duration: '31:00'
        explicit: false
        url: 'https://traffic.libsyn.com/secure/decipherscifi/isolated_bs_3.final.mp3'

---
#### Prank'd!

Kids and aliens and not understanding how pranks work. Losing your arm to a racoon attack, but then turning into a cyborg so maybe it's okay.

{% youtube "https://www.youtube.com/watch?v=F_brnKz_2tI" %}

#### Zombie satellites!

Joe shared with us [this recent development](https://www.npr.org/2020/04/24/843493304/long-lost-u-s-military-satellite-found-by-amateur-radio-operator) in zombie satellite communications. The long-term orbital stability og geosynchronous orbit, and ssedpace "ruins" as the longest landing mark of an expired humanity.

#### Joy items

Huaraches! Coffe tools and supplies. Colbert's long lost dinosaur mug.

{% responsive_image_block %}
  path: 'assets/imgs/misc/running_sandals.jpg'
  alt: 'Feet wearing running sandals'
  caption: 'Running Sandals!'
  attr_text: 'CC-0'
  attr_url: 'https://libreshot.com/barefoot-childrens-sandals/'
{% endresponsive_image_block %}

#### Ancient tattoos

Ancient tatoo princess a[rt reconstruction](http://siberiantimes.com/culture/others/features/siberian-princess-reveals-her-2500-year-old-tattoos/). Mummification and skin art preservation.

---
layout: person_page
person_data:
  name: Jolene Creighton
  data_name: jolene_creighton
  image_path: assets/imgs/people/jolene_creighton.png
  title: Science Communicator
  bio: 'Founding Editor-in-Chief, Futurism. Co-founder, Quarks to Quasars. Mostly, she does science-type things. Is a magical unicorn.'
  websites:
    - name: Jolene Creighton @ Facebook
      url: 'https://www.facebook.com/jolene.creighton'
  social:
    - name: twitter
      url: https://www.twitter.com/sciencejolene
    - facebook: jolene.creighton
  roles:
    - show: decipherscifi
      role: guest_cohost
---

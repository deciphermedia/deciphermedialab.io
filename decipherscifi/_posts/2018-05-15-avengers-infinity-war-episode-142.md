---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2018-05-15 04:51:17+00:00
description: "Avengers: Infinity War science! Thanos and utilitarianism, coin purse vulnerabilities. Thanos' excel spreadsheets. Atmospheric pressure and hypoxia. Nuetron stars: density, magnetic fields, and general unpleasantness. Dyson spheres and Dyson beams. Vision based on movement (but not really)."
layout: podcast_post
permalink: /decipherscifi/avengers-infinity-war-episode-142
redirect_from:
  - /decipherscifi/142
slug: avengers-infinity-war-episode-142
title: 'Avengers Infinity War: space mapping, Dyson spheres, and the purple coin purse'
tags:
  - atmopshere
  - ecnomics
  - marvel
  - science
  - science fiction
  - stars
  - superheroes
  - vision

# Episode particulars
image:
  path: assets/imgs/media/movies/avengers_infinity_war_2018.jpg
  alt: A stack of Marvel heads, Infinity War movie backdrop
links:
  - text: 'Megastructures 1.1: Dyson Spheres by Isaac Arthur'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=HlmKejRSVd8'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/avengers-infinity-war/id1370224078?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Avengers-Infinity-Robert-Downey-Jr/dp/B07CKNQGLN'
  title: Avengers Infinity War
  type: movie
  year: 2018
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/avengers_infinity_war_2018.jpg
  number: 142
  guid: 'https://decipherscifi.com/?p=6444'
  media:
    audio:
      audio/mpeg:
        content_length: 38656720
        duration: '46:00'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Avengers_Infinity_War_--_--_Decipher_SciFi.mp3'
---
#### Thanos

Josh Brolan was so good! And the character itself too! Coin purse chin. The story of how a light flick to the coin purse could have saved the world.

#### Economic modeling

Thanos breaks out the Excel spreadsheets. Productively employing the bottom trillionty billion around the galaxy. Ultimate utilitarianism.

#### The upper atmosphere

Atmospheric pressure. Hypoxia.

#### Space mapping and orienteering

Visualizing three-dimensional routes as two-dimensional on Earth-maps. Visualizing four-dimensional routes as three three physical dimensions plus _time_.

#### Star forges

Forging in the heart of a neutron star vs heart-of-a-neutron-star-adjacent. Breaking assumptions. Neutron stars. Facts and figures and analogies. Converting teaspoons of neutron star matter into 5-Dollar Footlongs. Thor jelly vs Thor Jam and their relative whelmingness.

#### Dyson Spheres

Disovering that Dyson's [original conception](http://science.sciencemag.org/content/131/3414/1667) (1960) of the Dyson "sphere" actually better fits the modern description of a Dyson "swarm." Different Dyson sphere variants. Statites.

#### Vision based on movement

But it isn't! It's contrast-based! Toad vision feature detection. Jurassic Park confusion.

{% youtube "https://www.youtube.com/watch?v=l3Es9cNH7I8" %}

#### Iron Man

Nanotech. Programming nanobots to eat brains - or gloves!

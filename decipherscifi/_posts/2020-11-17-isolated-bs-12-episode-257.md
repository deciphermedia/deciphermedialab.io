---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - conversation
date: 2020-11-17T02:58:33-05:00
description: "Clouds, in spaaaace. The Oculus Quest 2 and the possible shifting of the VR market. Christopher's review. Experiencing unreality. Making jerky. Etc."
layout: podcast_post
redirect_from:
  - /decipherscifi/257
slug: vr-breakthorughs-space-clouds-earthworm-jerky
title: 'Isolated BS: VR breakthroughs, space clouds, and earthworm jerky'
tags:
  - food
  - food preservation
  - meat
  - science
  - science fiction
  - space communications
  - virtual reality

# Episode particulars
image:
  path: assets/imgs/media/misc/isolated_bs_12_vr.jpg
  alt: 'Jar brain and History Brain in futuristic 90s VR'
links:
  - text: About Chapulines
    urls:
      - text: Wikipedia
        url: 'https://en.wikipedia.org/wiki/Chapulines'
  - text: 'Zebra vs Horses: Animal Domestication by CGP Grey'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=wOmjnioNulo'
media:
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/misc/isolated_bs_12_vr.jpg
  number: 257
  guid: vr-breakthorughs-space-clouds-earthworm-jerky
  media:
    audio:
      audio/mpeg:
        content_length: 26557953
        duration: '31:28'
        explicit: false
        url: 'https://traffic.libsyn.com/secure/decipherscifi/bs12.final.mp3'

---
# Spaaaaace

Clouds in space...? Microsoft Azure Space and Azure Orbital. Making space cheaper and  more accessible with satcomm as a service.

# VR

The new Oculus Quest. Past [predictions]({% link decipherscifi/_posts/2020-09-22-isolated-bs-11-episode-253.md %}). Pulling the VR market in the direction of affordability? Pandemic winter VR. Hating Facebook while embracing their VR product. Getting better at not getting sick. Experiences of unreality.

# Conferencing

Virtual chewing noises. Missing irl conference-going. It's like a vacation for adult nerds!

# Food

Food preservation. Preserved dairy variations by latitude. Jerky! Appreciating controlled rot. Earthworm jerky. [Roasted grasshoppers](https://en.wikipedia.org/wiki/Chapulines)!

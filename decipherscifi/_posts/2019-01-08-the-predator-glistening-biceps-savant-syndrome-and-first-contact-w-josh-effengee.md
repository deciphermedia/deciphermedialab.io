---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2019-01-08 07:45:04+00:00
description: 'Appreciating the action heroes of yesteryear. Duke Nukem. Savantism and useful skills like calendar calculation. Chess vs computer club. Predator etymology.'
layout: podcast_post
permalink: /decipherscifi/the-predator-glistening-biceps-savant-syndrome-and-first-contact-w-josh-effengee
redirect_from:
  - /decipherscifi/175
slug: the-predator-glistening-biceps-savant-syndrome-and-first-contact-w-josh-effengee
title: 'The Predator: glistening biceps, savant syndrome, and first contact w/ Josh'
tags:
  - action heroes
  - aliens
  - evolution
  - man-meat
  - savantism
  - science
  - science fiction

# Episode particulars
image:
  path: assets/imgs/media/movies/the_predator_2018.jpg
  alt: A bad ass-looking Predator
links:
  - text: The X-Files Podcast
    urls:
      - text: LSG Media
        url: 'https://www.libertystreetgeek.net/x-files/'
  - text: Meet the Deadliest Cat on the Planet
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=nl8o9PsJPAQ'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/the-predator/id1434878646?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Predator-Boyd-Holbrook/dp/B07H7WXH79'
  title: The Predator
  type: movie
  year: 2018
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: josh_effengee
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/the_predator_2018.jpg
  number: 175
  guid: 'https://decipherscifi.com/?p=9662'
  media:
    audio:
      audio/mpeg:
        content_length: 38173675
        duration: '45:26'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/The_Predator_--_Josh_Effengee_--_Decipher_SciFi.mp3'
---
#### Action heroes

Glistening, rippling man-meat. Sweaty pecs. Aggressive handshakes with biceps the size of coconuts. Longing for the good old days. Feeling unjustified disappointment when our heroes aren't giant walking meat monsters. The inapplicability of the bodybuilder physique to actually getting hero stuff done. Bodylifting.

#### Savantism

"Savant syndrome is a condition in which someone with significant mental disabilities demonstrates certain abilities far in excess of average" -[Wiki](https://en.wikipedia.org/wiki/Savant_syndrome). As opposed to someone just simply being awesome at something. Josh's recent related coverage from [The X-Files Podcast @ LSG Media](https://www.libertystreetgeek.net/podcast-x-files-roland/). Acquired savantism. Aptitude for... alien user interfaces? Trans-cranial magnetic stimulation.

#### Nerddom

Chess club vs computer club. Afterschool D&D. Duke Nukem and Team Fortress LAN parties at school.

#### Predator

Predator vs prey. Linguistically, should they be called "predator" if they hunt mainly for sport? "Predator" as a label for a biological relationship in which one creatures hunts and eats another for sustenance. [The deadliest cat in the world](https://www.youtube.com/watch?v=nl8o9PsJPAQ). Humans as top "predator." How to judge top predator when the line is not so clear cut.

#### Alien contact

Survey of movie alien contact scenarios and what they reveal about us. Energy required for interstellar travel and the likelihood anyone will happen by by accident. Government secrecy. Zip tie budgets and how to keep an alien captive. irl tranquilizer dart use and the inherent dangers. Why not to shoot yourself with an alien tranquilizer dose.

#### Building the megapredator

Selecting desirable traits from around the cosmos. Leave enough room for my stomach. Spinal juice collection. Gene manipulation. Selecting for intellectual traits: competitive science. Interpretive flex-dancing to enable armor modes.

#### Predator-killer

Reasonable solutions to the predator problem. The fix for climate change (fusion?). Arnold Schwarzenegger. The Terminator integrated with The Predator, Aliens, and Blade Runner. How awesome was [the original predator]({% link decipherscifi/_posts/2018-07-17-predator-preposterone-light-manipulation-and-aliens-vs-predator-vs-blade-runner.md %})?

---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2019-04-02 07:45:00+00:00
description: 'Mammalian adaptations to life in the sea. Breathing pxygenated fluids. Free diving and decompression sickness. Domesticating sharks. Etc.'
layout: podcast_post
permalink: /decipherscifi/aquaman-cetaceans-riding-sea-animals-and-the-bends
redirect_from:
  - /decipherscifi/187
slug: aquaman-cetaceans-riding-sea-animals-and-the-bends
title: 'Aquaman: cetaceans, riding sea animals, and the bends'
tags:
  - animal communication
  - comic books
  - domestication
  - echolocation
  - marine biology
  - mythology
  - science
  - science fiction

# Episode particulars
image:
  path: assets/imgs/media/movies/aquaman_2018.jpg
  alt: 'Sexy golden-scaled Jason Mamoa'
links:
  - text: 'Zebra vs Horses: Animal Domestication by CGP Grey'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=wOmjnioNulo'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/aquaman-2018/id1444244278?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Aquaman-Jason-Momoa/dp/B07PRLG915'
    - text: YouTube
      url: 'https://www.youtube.com/watch?v=saCIVYoIfuA'
  title: Aquaman
  type: movie
  year: 2018
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/aquaman_2018.jpg
  number: 187
  guid: 'https://decipherscifi.com/?p=10363'
  media:
    audio:
      audio/mpeg:
        content_length: 29621416
        duration: '35:15'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Aquaman_--_--_Decipher_SciFi.mp3'

---
#### DCEU

Aquaman as a sign of possibility the of interesting things going forward in the DC Extended Universe. Sexy Jason Mamoa. Underwater Superman.

#### Atlantis

Unlimited power when everyone else thought the world was flat. Appreciating the innovation that was realizing the Earth was mostly spherical. Ancient Greek steam turbans. Eating raw fish and seaweed. the origins of sushi. The careful application of lava for removing parasites.

#### Sea mammals

Underwater echolocation. Communication by sound. The possibility of cetacean languages. Torpedo-shaped body plans and efficient insulation. Breathing fluids (but not sea water!).

#### Deep diving

Decompression sickness. Why you really don't want bubbles in your fluids. The differing breath-holding techniques of whales at different depths. Whales that get decompression sickness.

#### Domesticating sea animals

The list of traits that make for good domesticatiion: "Friendly, feedable, fecund, and family-friendly" and how hard it might be to find the full set of traits in the ocean. The ultimate futility of trying to domesticate sharks.

{% youtube "https://www.youtube.com/watch?v=iJ4T9CQA0UM" %}

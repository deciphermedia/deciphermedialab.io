---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2019-08-20 07:45:25+00:00
description: 'VFX and practical. Animatronics. Computing miniaturization. High-end 90s CG. Old-school sutntwork. Grey goo nanobots. Liquid nitrogen porkchop sandwiches.'
layout: podcast_post
permalink: /decipherscifi/terminator-2-advanced-puppetry-skynet-and-liquid-nitrogen-dangers
redirect_from:
  - /decipherscifi/207
slug: terminator-2-advanced-puppetry-skynet-and-liquid-nitrogen-dangers
title: 'Terminator 2: advanced puppetry, Skynet, and liquid nitrogen handling w/ Joe Ruppel'
tags:
  - archnol schwarzenegger
  - liquid nitrogen
  - man versus machine
  - science
  - science fiction
  - skynet
  - technology
  - terminator
  - time travel
  - vfx

# Episode particulars
image:
  path: assets/imgs/media/movies/terminator_2_1991.jpg
  alt: 'Scary-looking T-1000 and some foreshadowey molten metal'
links:
  - text: 'T2 Stunt Trailer by Peter Kent'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=fLZi-qqFJe8'
  - text: 'The Forgotten Art of Blockbuster Cinema by Mike Hill'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=rhG3PJPx4Sk'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/terminator-2-judgment-day/id214818692?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/TERMINATOR-JUDGMENT-DAY-Special-Extended/dp/B00ZRID4HG'
  title: Terminator 2
  type: movie
  year: 1991
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: joe_ruppel
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/terminator_2_1991.jpg
  number: 207
  guid: 'https://decipherscifi.com/?p=10744'
  media:
    audio:
      audio/mpeg:
        content_length: 43629430
        duration: '51:50'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/terminator_2.decipherscifi.mp3'

---
#### People

Love and appreciation for the humans involved. James Cameron is amazing. Arnold, always our favorite. Linda Hamilton playing one of the most iconic characters in film. Robert Patrick doing the robot. James Cameron doing everything himself.

#### VFX

VFX and CG and the high shot counts for the time. The expense of CG and the appropriate use thereof. Budgets. Incredible animatronix! The value of high-quality practical FX from the 90s and how they hold up.

#### Ict hot stuntaz

Motorcycle jumping. Harley's Fat Boy. Jumping into helicopters.

#### Technology

The Terminator series as a window into our concerns over technology in different decades. Computing miniaturization. _Quantum stuff_. Read-only Terminator modes, IRQ switching, and `ro` cassettes.

#### Terminators

Terminator models and designs. Possible Billy Idol T-1000, or Kyle Reese-bots. T-1000 shiny grey goo nanite flow robot.

#### Liquid Nitrogen

Pork chop sandwiches! Inert gas asphyxiation. Steam barriers, sugar work, and dipping your hand into hot lead.

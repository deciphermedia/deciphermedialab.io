---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2019-10-15 07:45:39+00:00
description: 'Godzilla titan science! Ancient civilizations. Tracking elephants by their acoustic signature. Moths vs butterflies. Moth evolution and bat avoidance. Etc.'
layout: podcast_post
permalink: /decipherscifi/godzilla-king-of-monsters-acoustic-wildlife-tracking-and-moth-superiority
redirect_from:
  - /decipherscifi/213
slug: godzilla-king-of-monsters-acoustic-wildlife-tracking-and-moth-superiority
title: 'Godzilla King of the Monsters: acoustic wildlife-tracking and moth superiority'
tags:
  - animals
  - earth
  - evolution
  - monsters
  - moths
  - radioactivity
  - science
  - science fiction
  - silurian hypothesis

# Episode particulars
image:
  path: assets/imgs/media/movies/godzilla_king_of_monsters_2019.jpg
  alt: 'Scary Godzilla facing multiple dragon-heads'
links:
  - text: 'Deep Learning With The Elephants by Planet Money'
    urls:
      - text: NPR.org
        url: 'https://www.npr.org/2019/08/09/749938354/episode-932-deep-learning-with-the-elephants'
  - text: 'Godzilla: postwar Japan, science, and regrets w/ Miles Greb'
    urls:
      - text: Decipher SciFi
        url: 'https://decipherscifi.com/47'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/godzilla-king-of-the-monsters-2019/id1462805716?mt=6'
    - text: Amazon
      url: 'https://www.amazon.com/Godzilla-King-Monsters-Kyle-Chandler/dp/B07VKQNJNW'
  title: 'Godzilla: King of the Monsters'
  type: movie
  year: 2019
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/godzilla_king_of_monsters_2019.jpg
  number: 213
  guid: 'https://decipherscifi.com/?p=10821'
  media:
    audio:
      audio/mpeg:
        content_length: 24415186
        duration: '28:57'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/godzilla_king_of_monsters.mp3'

---
#### Titans

Awaking dormant giant monsters from within the Earth. Bio-acoustic signatures. Giant balloons! [Tracking elephants by sound](https://www.npr.org/2019/08/09/749938354/episode-932-deep-learning-with-the-elephants).

#### Pre-civilization civilizations

[The Silurian hypothesis](https://en.wikipedia.org/wiki/Silurian_hypothesis) thought experiment, especially as part of our [Atlantis]({% link decipherscifi/_posts/2019-03-19-atlantis-the-silurian-hypothesis-ancient-language-and-updating-the-drake-equation.md %}) coverage. The increasing disappearability of the details of civilization as they raise their technological level.

#### Moths

Moths: way better than butterflies! Echolocation defense mechanisms. Corresponding bat appreciation in our [A Quiet Place episode]({% link decipherscifi/_posts/2018-07-03-a-quiet-place-echolocation-cochlear-implant-vs-hearing-aid-and-aliens.md %}). Echolocation signal jamming vs. aural camouflage. Giant moths and dedication to reproduction so _complete_ that they will never once eat as an adult and devote all of their stored energy to reproduction. How we harvest silk from moths.

#### Creature combos

Pterosaur-bird monster Rhodan. Inane pedantry. Pterasaurs vs dinosaurs! Freedom at the cutting creative edge of paleontological art.

---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - tv
date: 2017-01-17 07:52:14+00:00 
description: 'Liam Ginty joins us to look at the first half of Black Mirror Season 3: Nosedive, Playtest, and Shut Up and Dance. Social commentary AND science stuff!' 
layout: podcast_post
permalink: /decipherscifi/black-mirror-season-3-part-1-feat-liam-ginty-episode-73
redirect_from:
  - /decipherscifi/73
slug: black-mirror-season-3-part-1-feat-liam-ginty-episode-73 
title: 'Black Mirror Season 3, Part 1: meowmeow beans, neurological speed, and showing our butts w/ Liam Ginty' 
tags:
  - science fiction
  - science
  - social media
  - video games
  - virtual reality

# Episode particulars
image:
  path: assets/imgs/media/tv/black_mirror_2011_season_3_part_1.jpg
  alt: Black Mirror season 3 collage from episodes Nosedive, Playtest, and Shut Up and Dance
links:
media:
  links:
    - text: Netflix
      url: 'http://www.netflix.com/title/70264888'
  title: Black Mirror
  type: tv
  year: 2011
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: liam_ginty
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/tv/black_mirror_2011_season_3_part_1.jpg
  number: 73
  guid: 'http://www.decipherscifi.com/?p=1267'
  media:
    audio:
      audio/mpeg:
        content_length: 41732096
        duration: '57:57'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Black_Mirror_Season_3_Part_1_--_Liam_Ginty_--_decipherSciFi.mp3'
---
## Episodes 1-3

Nosedive, Playtest, and Shut Up and Dance

#### Nosedive

Slow motion train wreck.  Meow-meow beans. [Chinese social credit system](https://en.wikipedia.org/wiki/Social_Credit_System). Real life is this episode. Nuance and charity in judging other people.

{% youtube "https://www.youtube.com/watch?v=CI4kiPaKfAE" %}

#### Playtest

Backpacking fantasy. Christopher's beard jealousy. Working while traveling via the internet. [Hideo Kojima](https://en.wikipedia.org/wiki/Hideo_Kojima). Near-death experiences, dream experiences, and neurological speed. Death spike hallucinations. American Psycho Hideo Kojima.

{% responsive_image path: assets/imgs/misc/hideo_kojima_implant.jpg alt: "Patrick Bateman Hideo Kojima meme" %}

#### Shut Up and Dance

Integrity. Liam's butt. The worst of the worst. Anonymous.

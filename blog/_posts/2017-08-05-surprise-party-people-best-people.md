---
# General post frontmatter
author: christopher_peterson
categories:
  - irl
date: 2017-08-05 17:07:06+00:00 
layout: post
slug: surprise-party-people-best-people 
title: 'Surprise party! (Our people are the best people!)' 
tags:
  - selfie
image:
  path: assets/imgs/misc/meetup-100-lsc.jpg
  alt: 'Decipher SciFi 100-episode surprise meetup group selfie'

---
Wow! My wonderful wife and all these friends of the show threw us a surprise party for our 100th episode. You guys are the best!

We went to Liberty Science Center. It was a delightful trip and I can't wait to go back with the kids.  :-)

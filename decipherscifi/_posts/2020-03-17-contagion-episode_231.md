---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2020-03-17T02:42:00-04:00
description: 'Dealing with current events. Wildlife farming and overflow. Hoarding steak for the carnivore diet. Homeopathy. Social distancing. Etc!'
layout: podcast_post
redirect_from:
  - /decipherscifi/231
slug: 'contagion-epidemiology-snake-oil-and-curve-flattening'
permalink: '/decipherscifi/contagion-epidemiology-snake-oil-and-curve-flattening'
title: 'Contagion: epidemiology, snake oil, and curve-flattening'
tags:
  - epidemiology
  - medicine
  - politics
  - psychology
  - quackery
  - science
  - science fiction
  - snake oil

# Episode particulars
image:
  path: assets/imgs/media/movies/contagion_2011.jpg
  alt: "Composite image of the characters from 'Contagion' in various levels of didstress"
links:
  - text: 'Johns Hopkins Coronavirus Resource Center'
    urls:
      - text: JHU.edu
        url: 'https://coronavirus.jhu.edu'
  - text: 'Why new diseases keep appearing in China by Vox'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=TPpoJGYlW54'
  - text: 'Medlife Crisis'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/channel/UCgRBRE1DUP2w7HTH9j_L4OQ'
  - text: 'Planet Money Podcast'
    urls:
      - text: NPR.org
        url: 'https://www.npr.org/podcasts/510289/planet-money'
  - text: 'The Indicator from Planet Money'
    urls:
      - text: NPR.org
        url: 'https://www.npr.org/podcasts/510325/the-indicator-from-planet-money'
  - text: 'Self-experimentation in medicine'
    urls:
      - text: Wikipedia
        url: 'https://en.wikipedia.org/wiki/Self-experimentation_in_medicine'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/contagion/id479822231?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Contagion-Marion-Cotillard/dp/B085F2XBNB'
  title: Contagion
  type: movie
  year: 2011
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/contagion_2011.jpg
  number: 231
  guid: 'https://decipherscifi.com/?p=10952'
  media:
    audio:
      audio/mpeg:
        content_length: 52595269
        duration: '1:02:30'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/contagion.final.mp3'

---
#### Current events

Our world is on the brink of some possibly extreme unpleasantness. Known unknowns threats to civilization. Humanity is bad at planning for statistically-likely-but-not-definite disaster.

#### Bullshit

The anti-scientific garbage that starts to overflow in the face of a public health crisis. Goop. Alex Jones. Snake oil. Homeopathy. Price gouging. Levels of understanding and respect for the severity of the current pandemic across age groups.

#### Epidemiology

R0, or basic reproduction number. Vulnerable sub-populations. Prisons. Social distancing. Hoarding toilet paper and... meat? The nipah virus.

#### Human behaviour

Social distancing. Hoarding steak and toilet paper.

#### Epidemiology

Historical pandemics. Rate of spead and pandemic reach in the premodern and modern worlds. Propaganda and the "Spanish Flu" of 1918. Wildlife as natural disease reservoirs. Wildlife farming and infectious disease overflow. Disease-spread mitigation. Getting germs in your holes Wash your hands! 

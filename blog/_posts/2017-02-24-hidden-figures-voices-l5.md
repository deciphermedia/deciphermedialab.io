---
# General post frontmatter
author: christopher_peterson
categories:
  - guest appearance
date: 2017-02-24 13:56:08+00:00 
layout: post
slug: hidden-figures-voices-l5 
title: 'Hidden Figures! (Voices From L5 guest appearance)' 
image:
  path: 'assets/imgs/media/movies/hidden_figures_2016.jpg'
  alt: 'Hidden Figured movie backdrop'
---
I was a guest on our friend [Liam]({% link _people/liam_ginty.md %})'s show! And [here it is](https://www.patreon.com/posts/voices-from-l5-8141348). We talked about the movie Hidden figures. So if you haven't heard his show, please give this a listen and then subscribe to his show at [Voice From L5](https://www.patreon.com/VoicesFromL5/posts) for new and interesting takes on humanity in space. 🙂

(PS: after we were done recording the things Liam wanted to talk about, I realized there was a lot more here to say in _our_ wheelhouse. So keep an eye out for our own episode on the matter soon!)

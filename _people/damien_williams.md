---
layout: person_page
person_data:
  name: Damien Williams
  data_name: damien_williams
  image_path: assets/imgs/people/damien_williams.png
  title: Think-Read-Book-Man
  bio: 'Writes, talks, thinks, teaches, and learns about philosophy, comparative religion, magic, artificial intelligence, human physical and mental augmentation, pop culture, and how they all relate.'
  websites:
    - name: A Future Worth Thinking About
      url: 'http://www.afutureworththinkingabout.com/?page_id=4'
  social:
    - name: twitter
      url: 'https://twitter.com/Wolven'
  roles:
    - show: decipherscifi
      role: roundtable_guest

---


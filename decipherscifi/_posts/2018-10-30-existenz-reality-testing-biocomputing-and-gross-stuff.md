---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2018-10-30 07:45:28+00:00
description: 'Gross Cronenberg body horror technology! Meat guns. Biocomputing. Quantum supremacy. Neural interfaces. Reality testing. Sphincter vs meatus, etc.'
layout: podcast_post
permalink: /decipherscifi/existenz-reality-testing-biocomputing-and-gross-stuff
redirect_from:
  - /decipherscifi/165
slug: existenz-reality-testing-biocomputing-and-gross-stuff
title: 'eXistenZ: reality testing, biocomputing, and gross stuff'
tags:
  - biological computing
  - biology
  - cronenberg
  - neurology
  - retro gaming
  - science
  - science fiction
  - transhumanism
  - video games
  - weapons

# Episode particulars
image:
  path: assets/imgs/media/movies/existenz_1999.jpg
  alt: 'Sexy people on a bed but with yucky Cronenberg stuff, thus ruining it. eXistenZ movie backdrop.'
links:
  - text: War Stories by Ars Technica
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/playlist?list=PLKBPwuu3eCYkScmqpD9xE7UZsszweVO0n'
  - text: GDC Classic Game Postmortems
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/playlist?list=PL2e4mYbwSTbbiX2uwspn0xiYb8_P_cTAr'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/existenz/id432475554?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Existenz-Jennifer-Jason-Leigh/dp/B009ZQRS2I'
  title: eXistenZ
  type: movie
  year: 1999
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/existenz_1999.jpg
  number: 165
  guid: 'https://decipherscifi.com/?p=7954'
  media:
    audio:
      audio/mpeg:
        content_length: 31729394
        duration: '37:45'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/eXistenZ_--_--_Decipher_SciFi.mp3'
---
#### Cronenberg

Creepy body horror. Fingers in holes. This is kinda hot.

#### The video game industry

Gaming in film. Brainscan. The video game bubble. E.T. The Video Game [landfill debacle](https://en.wikipedia.org/wiki/Atari_video_game_burial). AAA Game budgets before people took the industry seriously. AAA Auteur creators of the past. Auteur creators in indie games now.

#### Meat guns

Wooden cannons! Wow! Harvesting ammunition from people's mouths. Bio-based squirt guns as a reasonable alternative. [Blood-squirting lizards](https://www.youtube.com/watch?v=hE_fZNZjJcQ).

#### Alternative computing

Biological computing: mechanical, electrical, and chemical. [Quantum supremacy](https://en.wikipedia.org/wiki/Quantum_supremacy). Nipple interfaces and umbilicals.

#### Neural interfaces

Neural interface placements. Spinal connections and how to interact with the sensory loop. Bodily orifice security defenses. Sphincters vs meatuses.

#### Reality

But how do we tell what is real anyway!? The power of storytelling, even when you know it's not real. Railroading. Milgram experiment the game.

---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2018-02-27 07:45:43+00:00
description: 'The Cloverfield Paradox meets science! Which it needed. The Cloverfield formula, attempts at simulating gravity, gyroscopes and space station orientation. 3D printing, particle collider design, Phineas Gage for the nuclear age, new strategies fighting bone and muscle loss in space. And Adrian Falcone is here!'
layout: podcast_post
permalink: /decipherscifi/the-cloverfield-paradox-feat-adrian-falcone-physics-nerd-episode-131
redirect_from:
  - /decipherscifi/131
slug: the-cloverfield-paradox-feat-adrian-falcone-physics-nerd-episode-131
title: 'The Cloverfield Paradox: NASA bone loss, simulated gravity, and gyroscopes w/ Adrian Falcone'
tags:
  - 3d printing
  - artificial gravity
  - freezing
  - gyroscopes
  - monsters
  - particle colliders
  - physics
  - science
  - science fiction
  - space settlement
  - space stations

# Episode particulars
image:
  path: assets/imgs/media/movies/the_cloverfield_paradox_2018.jpg
  alt: A worried person in a space helmet, Cloverfield Paradox movie backdrop
links:
  - text: 10 Cloverfield Lane
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/movie/10-cloverfield-lane/id1085930296?mt=6&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/10-Cloverfield-Lane-John-Goodman/dp/B01CUVU7DQ'
  - text: Burning Astronaut Pee - Smarter Every Day 149
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=05oOst9kZXQ'
  - text: Space Telescopes Maneuver like CATS - Smarter Every Day 59
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=7AR4yntqLsQ'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/10-cloverfield-lane/id1085930296?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/10-Cloverfield-Lane-John-Goodman/dp/B01CUVU7DQ'
  title: The Cloverfield Paradox
  type: movie
  year: 2018
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: adrian_falcone
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/the_cloverfield_paradox_2018.jpg
  number: 131
  guid: 'https://decipherscifi.com/?p=5205'
  media:
    audio:
      audio/mpeg:
        content_length: 44638208
        duration: '53:07'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/The_Cloverfield_Paradox_--_Adrian_Falcone_--_Decipher_SciFi.mp3'
---
#### The Cloverfield Formula

Cloverfield -> 10 Cloverfield Lane -> The Cloverfield Paradox -> Overlord

1. Find a script
2. Add monsters
3. ...
4. profit

#### Artifical gravity. With a twist!

Like, literally a twist. Balancing huge gyroscopes on a central stem. Spinning buckets. Bucket size

#### Combating bone and muscle atrophy with the ARED.

How the ARED works. The danger of jackedstronauts coming back from space to take over the planet.

{% youtube "https://www.youtube.com/watch?v=gzynkaHuHwY" %}

#### Particle colliders

[Anatoli Burgorski](https://en.wikipedia.org/wiki/Anatoli_Bugorski) took a proton bream to the back of the face. It went surprisingly well for him! Phineas Gage of the nuclear age. Linear vs circular particle accelerators. The Cloverfield Paradox.

#### 3D printing

Impressively unimpressive food printing. Printing plastic handguns and metal lower receivers. Maybe don't use a gun on a space station though that's pretty dangerous.

#### Gyroscopes

Chris' immense latissimus dorsi. Avoiding affecting sensitive space mechanisms by avoiding spewing mass into space around your station/telescope.

{% youtube "https://www.youtube.com/watch?v=7AR4yntqLsQ&t=424s" %}

#### Losing earth/losing a space station

Geocentric vs heliocentric orbits. How to tell if Earth is gone/how to tell if your space station is gone.

---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2018-10-16 07:45:28+00:00
description: 'Gravity science! Space dangers. EVA jet chairs then and now. Relative orbital velocities. Dodging sniper bullets. EVA suits without the atmosphere issues.'
layout: podcast_post
permalink: /decipherscifi/gravity-decompression-sickness-orbital-mechanics-and-jet-chairs
redirect_from:
  - /decipherscifi/163
slug: gravity-decompression-sickness-orbital-mechanics-and-jet-chairs
title: 'Gravity: decompression sickness, orbital mechanics, and jet chairs'
tags:
  - astronauts
  - international space station
  - nasa
  - science
  - science fiction
  - space
  - space health
  - space settlement

# Episode particulars
image:
  path: assets/imgs/media/movies/gravity_2013.jpg
  alt: 'Astronauts working on Hubble with no danger in sight, Gravity backdrop'
links:
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/gravity/id708338403?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Gravity-Sandra-Bullock/dp/B00HJ92QTI'
  title: Gravity
  type: movie
  year: 2013
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/gravity_2013.jpg
  number: 163
  guid: 'https://decipherscifi.com/?p=7863'
  media:
    audio:
      audio/mpeg:
        content_length: 37569132
        duration: '44:42'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Gravity_--_--_Decipher_SciFi.mp3'
---
#### Criticism

Sloppy orbital mechanics and the value of science-adjacent entertainment.

#### Ed Harris

Forever the voice of Mission Control.

#### Space is dangerous

Almost as scary as the ocean.

#### EVA jet chairs

NASA's [MMU](https://en.wikipedia.org/wiki/Manned_Maneuvering_Unit). The new tiny one: [SAFER](https://en.wikipedia.org/wiki/Simplified_Aid_For_EVA_Rescue)! Giant rectal rectangle jet chairs?

#### Where and how things orbit

Earth-orbital distances. Where different kinds of man-made satellites operation and how they are not likely to run into each other roughly ever. The utility of geostationary orbits.  [Earth orbits](https://upload.wikimedia.org/wikipedia/commons/b/b4/Comparison_satellite_navigation_orbits.svg).

#### Orbital impacts

Relative velocities. [Could you dodge a sniper bullet?](https://www.youtube.com/watch?v=2qY0DA7DG9s) IRL ISS escape procedures. Soyuz escape plans.

#### Deadspin

Glowing blood and possible alien infections. Human sensitivity to positive and negative g-forces. Trying to push more blood to your head for mental superpowers.

#### Orbital transfer

And impromptu deorbiting. How to crash a probe into mars (by accident).

#### Pressurization

Chinese space stations, then and now. Station entrance and exit procedures. Finally the Star Trek balloon analogy comes in handy! Decompression sickness/the bends during EVA. Portable life support systems. Sokol and ACES, and their insufficiency for extra-vehicular activities.

#### Fire extinguishers

...in spaaaaaace. Carbon dioxide. Water. Super soakers. Fire emergency procedures.

#### EVA Suits

Past designs and modern designs. Future EVA suit developments. Constrictive suits directly _against the skin_ rather than creating an atmosphere around the body: [the biosuit](https://www.space.com/27210-biosuit-skintight-spacesuit-concept-images.html). The NASA [Z Suit](https://en.wikipedia.org/wiki/Z_series_space_suits) that looks like Buzz Lightyear, and operating EVA suits at the same pressure as your space station.

#### Reentry

Soyuz reentry angles. Heat shielding. Trying to point in a particular direction.

#### Lifting

The complete obviousness that heavy compound weight lifting is the major solution to muscle and bone loss in space. Bro do you even lift? [Vacuum-based space squat rack](https://youtu.be/05oOst9kZXQ).

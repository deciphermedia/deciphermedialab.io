---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - conversation
date: 2020-06-30T03:50:39-04:00
description: 'Corvid rankings. Normalizing masks. Enjoying the enabling of introversion in lockdown. Colorectal updates. Testosterone and fitness. Our new website source repo! Good and very, very bad art restoration. Etc.'
layout: podcast_post
redirect_from:
  - /decipherscifi/244
slug: isolated-art-restoration-ahistorical-viking-grooming-preposterone
title: 'Isolated BS: art restoration, ahistorical viking grooming, and preposterone'
tags:
  - art
  - fitness
  - history
  - science
  - science fiction
  - virtual reality

# Episode particulars
image:
  path: assets/imgs/media/misc/isolated_bs_6_art_restoration.jpg
  alt: Mona Lisa and Prado Mona Lisa with horrifying Decipher Media cartoon brain faces
links:
  - text: 'Masking For a Friend'
    urls:
      - text: '99pi.org'
        url: 'https://99percentinvisible.org/episode/masking-for-a-friend/'
  - text: 'Our open source website code'
    urls:
      - text: Gitlab.com
        url: 'https://gitlab.com/deciphermedia/deciphermedia.gitlab.io'
  - text: 'The Life and Times of Mr C by Andy P and Emily RG'
    urls:
      - text: Batch25Comics
        url: 'https://batch25comics.com/the-life-and-times-of-mr-c/'
media:
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/misc/isolated_bs_6_art_restoration.jpg
  number: 244
  guid: isolated-art-restoration-ahistorical-viking-grooming-preposterone
  media:
    audio:
      audio/mpeg:
        content_length: 40354994
        duration: '47:59'
        explicit: false
        url: 'https://traffic.libsyn.com/secure/decipherscifi/isolated_bs_6.final.mp3'

---
# Millhouse isolation

Enabled introversion. Social anxiety and human avoidance: online ordering abd contactless food delivery.

# Masks

Hoping for normalization of mask-wearing to confine your illness ot yourself. The popularization of medical masks for the lay population in China around the "[Manchurian plague](https://en.wikipedia.org/wiki/Manchurian_plague)." Masks failing to combat person-recognition (or probably even facial recognition 😥).

# Viking grooming

That [one time]({% link decipherscifi/_posts/2016-12-06-outlander-feat-lee-from-viking-age-episode-67.md %}) we did a viking episode with [The other Lee]({% link _people/lee_accomando.md %}). Our best historical understanding of Viking grooming standards, and how that very much differs from out contemporary depictions. BUT! The contemporary depictions are really fun so it's ok. 👌

# Corvids

[The problem-solvingest birds](https://en.wikipedia.org/wiki/Corvidae#Intelligence). Ravens and other corvids and their respective quality as tattoo-subject candidates. Planning ravens on the chest/shoulders. Maybe a jackdaw sleeve?

# Nazis

For real, fuck those guys. Cultural appropriation.

# Butt stuff

Welcome to the Decipher Media colorectal new update. Christopher's incoming bidet. Fiber supplmentation and chasing the ghost poop dragon. Young men living on Mountain Dew and waffles.

> Demodex never poop and then explode with poop and die
>
> - [Christopher]({% link _people/christopher_peterson.md %})

# Fitness

Testosterone levels over time. Testosterone replacement therapy. Dancing - fun for the whole family! Strength nostalgia.

# VR Gaming

Again with the motion sickness.

# Our website

[The site is now open source](https://gitlab.com/deciphermedia/deciphermedia.gitlab.io)! Go take a look if you're into Jekyll or Ruby or Gitlab or generally just podcasting the hard way.

# Art restoration

The unqalified Spanish art resotrers are [at it again](https://www.theguardian.com/artanddesign/2020/jun/22/experts-call-for-regulation-after-latest-botched-art-restoration-in-spain)! And don't forget the OG: [Monkey Jesus](https://en.wikipedia.org/wiki/Ecce_Homo_(https://en.wikipedia.org/wiki/Ecce_Homo_%28Mart%C3%ADnez_and_Gim%C3%A9nez,_Borja%29). [Hanlon's razor](https://en.wikipedia.org/wiki/Hanlon%27s_razor).

# Upcoming

As per our [Colossus Roundtable]({% link decipherscifi/_posts/2019-08-27-roundtable-colossus-the-forbin-project-w-chris-noessel-damien-jonathon.md %}): watch [Person of Interest](https://www.netflix.com/title/70197042)!

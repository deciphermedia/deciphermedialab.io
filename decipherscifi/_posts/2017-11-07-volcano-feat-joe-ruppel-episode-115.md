---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2017-11-07 07:47:15+00:00
description: 'Volcano - hot sticky molten-rock science. Seriously, what is a volcano? Pyroclastic flow, Pompeii, prehistoric climate-altering eruptions, fault lines, more'
layout: podcast_post
permalink: /decipherscifi/volcano-feat-joe-ruppel-episode-115
redirect_from:
  - /decipherscifi/115
slug: volcano-feat-joe-ruppel-episode-115
title: 'Volcano: seismic detectors, tar pit geology, and dangers of volcanic ash w/ Joe Ruppel'
tags:
  - fault lines
  - geology
  - paleontology
  - pyroclastic flow
  - science
  - science fiction
  - volcanoes

# Episode particulars
image:
  path: assets/imgs/media/movies/volcano_1997.jpg
  alt: A Volcano sprouting over LA
links:
  - text: Experiencing a volcanic shockwave
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=BUREX8aFbMs'
  - text: 'From the movie: a human being melting directly and immediately into mere inches of lava'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=R01bex9Ejvg'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/volcano/id280175630?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Volcano-Tommy-Jones/dp/B000I9YXWE'
  title: Volcano
  type: movie
  year: 1997
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: joe_ruppel
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/volcano_1997.jpg
  number: 115
  guid: 'https://decipherscifi.com/?p=4020'
  media:
    audio:
      audio/mpeg:
        content_length: 32503808
        duration: '38:41'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Volcano_--_Joe_Ruppel_--_decipherSciFi.mp3'
---
#### What is a volcano?

No, seriously, what is a volcano? A lot of film critics at the time whiffed on this one and at least one or two of us were in the wrong too. Volcano:

> A vent or fissure on the surface of a planet (usually in a mountainous form) with a magma chamber attached to the mantle of a planet or moon, periodically erupting forth lava and volcanic gases onto the surface.
> 
> -[Wiktionary](https://en.wiktionary.org/wiki/volcano)

#### La Brea tar pits

Predator/prey capture ratios. Biological activity causing the bubbling and subsequent discovery of a bunch of different bacterai that live in there. Larger things we find fossilized there:
 	
* Sabretooth tigers
* Mammoths
* Giant feline
* looooooooads more

#### Fault lines


The different fault types and which of them tend to manifest volcanoes. Spoiler alert: LA has a transform fault which is the kind that is really really really unlikely to sprout a volcano under normal conditions. [Transform faults](https://commons.wikimedia.org/wiki/File:Continental-continental_conservative_plate_boundary_opposite_directions.svg)

#### Volcano detection

Major monitoring methods looking out for pending eruptions:

* Seisimic activity
* Gas!
* Groundswell - actual deformation of the landscape from the pressure

#### Pompeii/Herculaneum

[Pyroclastic flow](https://en.wikipedia.org/wiki/Pyroclastic_flow) vs lava. The [eruption of Mt Vesuvius in 79AD](https://en.wikipedia.org/wiki/Eruption_of_Mount_Vesuvius_in_79). Casting the cavities that surrounded trapped bodies. [Cast of a pompeii body](https://commons.wikimedia.org/wiki/File:Pompeii_excavations,_Napoli,_Italy_35_-_dead_body.JPG)

#### Volcanic dangers

Volcanic ash, which is often basically just teeny tiny glass for you to breathe in. Lava temperatures: between holy shit hot and _omg holy shit_ hot.  Lava weather systems. Gasses! [Lava fountains](https://en.wikipedia.org/wiki/Lava#Lava_fountains).

#### Measures

Dams! Backwards or otherwise. [The Decade List](https://www.wired.com/2009/04/the-decade-volcanoes/).

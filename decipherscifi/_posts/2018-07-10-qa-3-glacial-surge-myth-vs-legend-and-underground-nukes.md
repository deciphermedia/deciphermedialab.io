---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - qa
date: 2018-07-10 07:45:14+00:00
description: 'Feedback and addenda! H.P. Lovecraft, Godzilla, Lojban, glaciers, volcanoes. Going deeper on some things we missed in earlier episodes. Featuring Jeremy! (sorta)'
layout: podcast_post
permalink: /decipherscifi/qa-3-glacial-surge-myth-vs-legend-and-underground-nukes
redirect_from:
  - /decipherscifi/150
slug: qa-3-glacial-surge-myth-vs-legend-and-underground-nukes
title: 'Q&A #3: glacial surge, myth vs legend, and underground nukes'
tags:
  - alcohol
  - geology
  - language
  - military
  - nuclear weapons
  - science
  - science fiction
  - stargates

# Episode particulars
image:
  path: assets/imgs/media/misc/qa_03.jpg
  alt: "Q&A #3 word cloud"
links:
media:
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/misc/qa_03.jpg
  number: 150
  guid: 'https://decipherscifi.com/?p=6865'
  media:
    audio:
      audio/mpeg:
        content_length: 42300317
        duration: '50:20'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/qa03_--_--_Decipher_SciFi.mp3'
---
A collection of responses to feedback and addenda from [supporter]({% link supporttheshow/index.md %}) Jeremy. Basically, a review of some things we missed in a few episodes and an expansion on others. :-)

#### [Volcano]({% link decipherscifi/_posts/2017-11-07-volcano-feat-joe-ruppel-episode-115.md %})

Fault types review. Flood basalts as hot spot lava spam. The Deccan Traps. Vulcanism as a possible explanation for dinsoaur extinction ([Wargames]({% link decipherscifi/_posts/2017-02-07-wargames-feat-joe-ruppel-history-nerd-episode-76.md %})).

#### [The Last Jedi]({% link decipherscifi/_posts/2017-12-26-star-wars-the-last-jedi-episode-122.md %})

Progress in millitary tactics. The triumph of guerrilla tactics. Politeness in chess. Mt St Helens sound waves. High-energy events.

#### [Black Mirror Season 4 Part 1 - Arkangel]({% link decipherscifi/_posts/2018-01-16-black-mirror-season-4-part-1-feat-fraser-cain-of-universe-today-episode-125.md %})

Implantable devices. API development.

#### [Philip K Dick's Electric Dreams]({% link decipherscifi/_posts/2018-01-30-philip-k-dicks-electric-dreams-episode-127.md %})

Lojban. Constructed languages. Esperanto.

#### [Transformers the Last Knight]({% link decipherscifi/_posts/2018-02-13-transformers-the-last-knight-feat-jolene-creighton-from-futurism-episode-129.md %})

Unfavorable reviews. Myth vs legend vs fable as given to us by an actual folklorist. Colonialism and the lens of cultural study. [Swole Jesus]({% link decipherscifi/_posts/2017-08-22-alien-covenant-episode-104.md %}). Zeus's promiscuity. Anti-intellectualism.

#### [Stargate]({% link decipherscifi/_posts/2018-04-03-stargate-feat-dominic-perry-of-the-history-of-egypt-podcast-episode-136.md %})

"Gopher nukes." Tactical nuclear yields. Nuclear testing and [Godzilla]({% link decipherscifi/_posts/2016-07-19-godzilla-miles-greb-movie-podcast-episode-47.md %}).

#### [Men in Black]({% link decipherscifi/_posts/2018-04-17-men-in-black-feat-liam-ginty-nerd-episode-138.md %})

Alcohol! Low-alcohol beverages in history instead of water. [A History of the World in Six Glasses](https://www.amazon.com/History-World-6-Glasses-ebook/dp/B002STNBRK), again.

#### [Lost in Space]({% link decipherscifi/_posts/2018-05-01-lost-in-space-2018-feat-adrian-falcone-nerd-episode-140.md %})
Climate change and glacial surges.

#### [Annihilation]({% link decipherscifi/_posts/2018-05-22-annihilation-episode-143.md %})

The Color Out of Space - by HP Lovecraft: [iTunes](https://geo.itunes.apple.com/us/book/the-colour-out-of-space/id1005756759?mt=11&at=1001l7hP) | [Amazon](https://www.amazon.com/Colour-Out-Space-H-Lovecraft-ebook/dp/B003IHVZYM)

#### [Tron]({% link decipherscifi/_posts/2018-06-05-tron-episode-145.md %})

Programming languages, where to draw the line behind low and high level.

#### [A Quiet Place]({% link decipherscifi/_posts/2018-07-03-a-quiet-place-echolocation-cochlear-implant-vs-hearing-aid-and-aliens.md %})
Sound localization without pitch. [Inaudible High-Frequency Sounds Affect Brain Activity](https://www.physiology.org/doi/full/10.1152/jn.2000.83.6.3548).

---
layout: person_page
person_data:
  name: Lee Colbert
  data_name: lee_colbert
  image_path: assets/imgs/people/lee_colbert.png
  title: Podcast Person
  bio: 'Beardly podcast man with the nimbleness of a dancing bear'
  websites:
    - name: LeeColbert.com
      url: 'https://leecolbert.com'
  # social:
  #   - name: twitter
  #     url: https://www.twitter.com/
  #   - name: instagram
  #     url: https://www.instagram.com/
  roles:
    - show: decipherhistory
      role: host
    - show: decipherrpg
      role: guest
    - show: decipherscifi
      role: host
---

---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2020-03-10T02:45:05-04:00
description: 'How this was eventually a good movie. Alexander taking the reigns from his father, Philip. Court purges. Pederasty. Sources. Etc.'
layout: podcast_post
redirect_from:
  - /decipherhistory/7
slug: alexander-succeeding-philip-the-great-alexandria
title: "Alexander: succession, 'The Great,' and sooo many Alexandrias w/ Ryan Stitt"
tags:
  - ancient greece
  - ancient rome
  - classicism
  - history
  - meta history
  - war
  - weapons technology

# Episode particulars
image:
  path: assets/imgs/media/movies/alexander_2004.jpg
  alt: 'Heroic, helmeted Colin Farrell on a rearing steed'
links:
  - text: "Ryan's Show!"
    urls:
      - text: "The History of Ancient Greece Podcast"
        url: 'http://www.thehistoryofancientgreece.com/'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/alexander-revisited-the-final-cut-unrated/id408616247?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Alexander-Ultimate-Cut-Colin-Farrell/dp/B00PGAZ1XU'
  title: Alexander
  type: movie
  year: 2004
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: ryan_stitt
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/alexander_2004.jpg
  number: 7
  media:
    audio:
      audio/mpeg:
        content_length: 66035480
        duration: '1:18:31.42'
        explicit: false
        url: 'http://traffic.libsyn.com/decipherscifi/alexander.final.mp3'

---
#### Meta

Versions! So many versions. The best version of the film: The Ultimate Cut.

#### Setting the scene

Ancient Greece after [300](% link decipherhistory/_posts/2019-06-11-300-professional-soldiers-the-spartan-mirage-and-expert-poking-w-josh-effengee.md %). The Peloponnesian War (which Ryan is [in the middle of](http://www.thehistoryofancientgreece.com/2019/04/091-attrition-and-plague.html) on his show!). The rise of Philip. Olympias.

#### Philip

Putting Macedon at center stage in ancient Greece. Technical and logistical innovations. Planning the invasion of Persia *right* before his suspiciously untimely death.

#### The purge

Alexander and Olypmias. Purging royal competition in the ancient world. Plutarch and gossip about "powerful women" in antiquity.

#### Sources

The loss of contemporary sources and the reliability of what remains. Plutarch's gossip column.

#### "The Great"

What makes an historical figure "The Great," instead of "The Terrible" or just forgotten? *So many* Alexandrias! Conquest, culture, and Hellenization.

#### Manly love

"Homosexuality" in the ancient world and different norms of power and masulinity. Alexander and Hephaestion. Achilles and Patroclas.

#### Death

Heavy Macedonian drinking and Alexander's downward spiral. Conflicting reports and also modern interpretations of possible causes of death. Indeterminate succession. Alexander's body floating around for centuries.

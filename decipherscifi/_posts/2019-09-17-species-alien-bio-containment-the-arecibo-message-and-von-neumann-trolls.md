---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2019-09-17 07:45:45+00:00
description: 'Species science. Alien genetic combination. The 90s! Ptosis. Jerky aliens. The sparse content of the Arecibo message. Cocoon soup!'
layout: podcast_post
permalink: /decipherscifi/species-alien-bio-containment-the-arecibo-message-and-von-neumann-trolls
redirect_from:
  - /decipherscifi/210
slug: species-alien-bio-containment-the-arecibo-message-and-von-neumann-trolls
title: 'Species:  alien bio-containment, the Arecibo message, and Von Neumann trolls'
tags:
  - aliens
  - panspermia
  - ptosis
  - science
  - science fiction
  - seti
  - space
  - von neumann probes

# Episode particulars
image:
  path: assets/imgs/media/movies/species_1995.jpg
  alt: 'Natascha Henstridge, but Geiger. And shooting an alien light beam...?'
links:
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/species/id292619972?mt=6'
    - text: Amazon
      url: 'https://www.amazon.com/Species-Ben-Kingsley/dp/B083F5NC4P'
  title: Species
  type: movie
  year: 1995
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/species_1995.jpg
  number: 210
  guid: 'https://decipherscifi.com/?p=10783'
  media:
    audio:
      audio/mpeg:
        content_length: 25751508
        duration: '30:33'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/species_1995.mp3'

---
#### Species

Wow there some really good movies in 1995! Geiger alien designs. Not "Alien," but "alien." This time, with nipples.

#### Ptosis

Forrest Whitaker. Chris's history of eyelid laziness variability. Being either a dolphin or a flamingo. Eye farts?

#### The Arecibo Message

The limited utility of sending messages to aliens _25,000 light years_ away. [The Arecibo Message](https://en.wikipedia.org/wiki/Arecibo_message) sent in 1974 toward the M13 globular cluster. A cluster containiner _millions_ of stars. 1679 bits: not a lot of bits!

{% responsive_image_block %}
  path: assets/imgs/misc/arecibo_message.jpg
  alt: A colorized visualization of the 'Arecibo Message' sent in 1974
  caption: A colorized visualization of the 'Arecibo Message' sent in 1974
  attr_text: Arne Nordmann CC-BY-3.0
  attr_url: https://commons.wikimedia.org/wiki/File:Arecibo_message.svg
{% endresponsive_image_block %}

#### Bio-executable Von Neumann machines

Von Neumann trolls. Self-assembling bootstrapping biological systems. What if DNA is very common across life in the universe?

#### Containment

How to combine biological containment and prisoner containment. _Lots_ of "fire" buttons. Biocontainment protocols. Integrating what we've learned from prison-escape movies. Considering the Magneto-vault.

#### Cocoons

Vulvic wall-pods. What the hell even is up with cocoons anyway? Cocoons are basically alien material already. The "soup method" of metamorphosis.

#### Fat-bergs

{% youtube "https://www.youtube.com/watch?v=3i_axpk0a7Q" %}

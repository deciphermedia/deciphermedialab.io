---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - qa
date: 2019-02-19 07:45:15+00:00
description: 'You asked us things, and we answered! How we started, why we started. Podcasting advice. Deciding to promote open licenses with our productions. Etc.'
layout: podcast_post
permalink: /decipherscifi/qa-4-answering-your-questions-and-appreciating-our-friends
redirect_from:
  - /decipherscifi/181
slug: qa-4-answering-your-questions-and-appreciating-our-friends
title: 'Q&A #4: Answering your questions and appreciating our friends'
tags:
  - 'q&a'
  - creative commons
  - feedback
  - free software
  - history
  - linux
  - podcasting
  - science
  - science fiction
  - vim

# Episode particulars
image:
  path: assets/imgs/media/misc/qa_04.jpg
  alt: Cartoon Christopher and Colbert in interview arrangement
links:
media:
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/misc/qa_04.jpg
  number: 181
  guid: 'https://decipherscifi.com/?p=10074'
  media:
    audio:
      audio/mpeg:
        content_length: 37370544
        duration: '44:28'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/QA4_--_--_Decipher_SciFi.mp3'

---
You folks asked us a bunch of questions. And we gave a bunch of answers!

I'll leave the actual Questions and Answers to be listened to in the episode, but we did mention some resources etc which I'll link below...

#### Past History coverage

To whet your appetite if you're as excited as us about the possibility of Decipher History:

* [Outlander]({% link decipherscifi/_posts/2016-12-06-outlander-feat-lee-from-viking-age-episode-67.md %}) with [Lee]({% link _people/lee_accomando.md %}) from [Viking Age Podcast](http://vikingagepodcast.com/)
* [The Mummy]({% link decipherscifi/_posts/2017-10-10-mummy-2017-feat-dominic-perry-history-egypt-podcast-episode-111.md %}) and [Stargate]({% link decipherscifi/_posts/2018-04-03-stargate-feat-dominic-perry-of-the-history-of-egypt-podcast-episode-136.md %}) with [Dominic Perry]({% link _people/dominic_perry.md %}) from [History of Egypt Podcast](https://egyptianhistorypodcast.com/)
* And a bunch of things like [Conan the Barbarian]({% link decipherscifi/_posts/2016-12-27-conan-the-barbarian-82-feat-joe-ruppel-history-nerd-episode-70.md %})
[Hidden Figures]({% link decipherscifi/_posts/2017-03-21-hidden-figures-feat-joe-ruppel-history-nerd-episode-82.md %})
[The Time Machine]({% link decipherscifi/_posts/2017-09-12-the-time-machine-feat-joe-ruppel-episode-107.md %})
[War of the Worlds]({% link decipherscifi/_posts/2016-03-29-war-of-the-worlds-joe-ruppel-movie-podcast-episode-31.md %})
and a bunch more with [Joe Ruppel]({% link _people/joe_ruppel.md %})

#### Podcasting advice

"Just make stuff!" But then for technical details for other Linux podcasters, my dotfiles (application configurations) are [shared openly on Github](https://chrispeterson.info/here-is-my-home-directory/). Files and scripts relevant specifically to our podcasting are probably all mentioned [here](https://chrispeterson.info/how-we-podcast-on-linux-at-decipher-scifi/).

{% youtube "https://www.youtube.com/watch?v=UhRXn2NRiWI" %}

#### Free software and licenses

Creative Commons and free software and making the world better by helping more people make their stuff.

Episode image made using:
[A Nice Cartoon Guy Filming A Live Marketing Video](https://commons.wikimedia.org/wiki/File:A_Nice_Cartoon_Guy_Filming_A_Live_Marketing_Video.svg) by Free Clip Art, [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.en)
[Fake Studio](https://www.flickr.com/photos/hyku/389755875/) by Josh Hallett, [CC-BY 2.0](https://creativecommons.org/licenses/by/2.0/)

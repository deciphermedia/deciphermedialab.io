---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2019-10-01 01:45:51-04:00
description: 'Crime in 19th century New York. Building the Five Points. Irish influx. The real Bill the Butcher. Period dialects. Surprising living conditions. Draft Riots!'
layout: podcast_post
permalink: /decipherhistory/gangs-new-york-irish-famine-draft-riots
redirect_from:
  - /decipherhistory/5
slug: gangs-new-york-irish-famine-draft-riots
title: 'Gangs of New York: history of The Five Points, immigration, and archaic criminal lingo'
tags:
  - civil war
  - crime
  - dialect
  - gangs
  - historicity
  - history
  - immigration
  - ireland
  - irish
  - language
  - new york

# Episode particulars
image:
  path: assets/imgs/media/movies/gangs_of_new_york_2002.jpg
  alt: Bill the Butcher draped in an American flag, Gangs of New York movie backdrop
links:
  - text: The Gangs of New York by Herbert Asburt
    urls:
      - text: Internet Archive
        url: 'https://archive.org/details/GangsOfNewYork_201811'
  - text: The Rogue's Lexicon by George Matsell
    urls:
      - text: Project Gutenberg
        url: 'http://www.gutenberg.org/ebooks/52320'
  - text: Five Points and 19th Century New York
    urls:
      - text: YouTube
        url: 'https://youtu.be/0JG-qEyu4q0'
media:
  links:
    - text: Netflix
      url: 'https://www.netflix.com/title/60021787'
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/gangs-of-new-york-2002/id432475512?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Gangs-New-York-Leonardo-DiCaprio/dp/B006RXQ800'
  title: Gangs of New York
  type: movie
  year: 2002
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/gangs_of_new_york_2002.jpg
  number: 5
  media:
    audio:
      audio/mpeg:
        content_length: 47768795
        duration: '56:46.65'
        explicit: false
        url: 'http://traffic.libsyn.com/decipherscifi/gangs_of_new_york.mp3'
---
#### Language

Creating the dialects, idolects, and accents of the five points. Historical Language sleuthing. Daniel Day Lewis' vocal performance.

#### Historicity

"A drama, not a documentary." The overstatements of the loose source book "The Gangs of New York." Compressed timeframes. Framing the story of the "gangs of New York" through the lens of the Italian mobs of the early 20th century. 

#### Blight

"The Great Famine" and "The Potato Famine." Blight: not a fungus! Actually non-photosynthetic algae. Living on only potatoes: pretty doable! How the blight was delivered from America and was able to flourish under the seasoncal conditions in Ireland.

#### Coming to America

Migration of Irish into (mostly) New York City. Staying where you land because you lack the resources to move on. The conditions on the long boat ride over.

#### Old New York

New York: like now, but smaller and mostly woods and marshes etc. Draining and filling the stinky sewage lake to make a place for the five points. American growth and the spoilage of nature. 

#### The Five Points

Not as crimey as you heard! But, maybe just as *grimey*. Discerning quality of life in the five points from archaeology, correspondences, censuces, bank records, and other sources. Common people, eating meat; for every meal! Chain immigration. 

#### Corruption

Using the Irish vote. Competitive firefighting. Tweed's truly impressive corruption.

#### Gangs

Bill the Butcher. 19th century gang names. The dueling origin stories of the "Dead Rabbits." Inter-linguistic phono-semantic matching. 

#### Draft riot

History washing over local squabbles. Lynchings. Violently protesting the purchasability of draft vexceptions. Thge New York Times offices and machine guns and molten lead.

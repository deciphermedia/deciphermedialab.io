People
=====

People in the context of Jekyll are part of a [collection] for the sake of organization and serving pages automatically from data.

# Creating people

We have hosts and guests and interviewees and sometimes people can even be multiple. So how do we set up their data?

Please first place their square image in `assets/imgs/people/person_data_name.png`. Images are required for each "person" in `_people/`.

Then start from the template: `_docs/templates/person.md`

```sh
cp _docs/templates/person.md _people/new_person.md
```

And fill it out! Just please make sure the field `person_data::data_name` matches the base filename.

# Adding people to an episode

Anyone listed in an episode as a `person` will be automatically listed at the bottom with their image.

Just put them into the `people` hash in the episode [frontmatter](../README.md#frontmatter) with their role, like follows:

```yaml
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: other_person
    roles:
      - guest_cohost
```

[collection]: https://jekyllrb.com/docs/collections/

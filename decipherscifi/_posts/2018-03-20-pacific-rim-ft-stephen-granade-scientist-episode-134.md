---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2018-03-20 05:05:25+00:00
description: 'Pacific Rim science and engineering with scientist Stephen Granade. Square-cube laws, alien butt-brains, and neurocomputational offloading. Robot engineering, alien blood, long-term terraforming strategies, Carl Sagan love, and doing science Aperture Science style. More!'
layout: podcast_post
permalink: /decipherscifi/pacific-rim-ft-stephen-granade-scientist-episode-134
redirect_from:
  - /decipherscifi/134
slug: pacific-rim-ft-stephen-granade-scientist-episode-134
title: 'Pacific Rim: square cube law, giant death robot, and Earth terrafroming  w/ Stephen Granade'
tags:
  - brain-machine interfaces
  - giant robots
  - kaiju
  - monsters
  - neurology
  - nuclear power
  - science
  - science fiction
  - square-cube law
  - terraforming

# Episode particulars
image:
  path: assets/imgs/media/movies/pacific_rim_uprising_2018.jpg
  alt: Pacific Rim cast people standing before a collection of really really big robots
links:
  - text: Stephen Granade
    urls:
      - text: Website
        url: 'http://stephen.granades.com/'
      - text: Twitter
        url: 'https://twitter.com/sargent'
  - text: Stephen on Pacific Rim again but with more physics
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=-nFL6X2EA2c'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/pacific-rim/id694686367?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Pacific-Rim-Charlie-Hunnam/dp/B00FWLDHSY'
  title: Pacific Rim
  type: movie
  year: 2018
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: stephen_granade
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/pacific_rim_uprising_2018.jpg
  number: 134
  guid: 'https://decipherscifi.com/?p=5863'
  media:
    audio:
      audio/mpeg:
        content_length: 50911816
        duration: '01:00:35'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Pacific_Rim_--_Stephen_Granade_--_Decipher_SciFi.mp3'
---
#### Brain-controlled machines

Brain-machine interfaces. EEG. The advantage of the group over the individual. Humans in tandem with AI and the closing window of human cognitive superiority. Computational offloading. Wetware/hardware integration and explaining the "excessive" neural load of Jaeger drift.

#### Drifting

Localized neural "loops." Neural load and body mass. Chasing the rabbit. Multiple input control vectors. Meatsack adaptations and proprioception. Human biofeedback and ambulation vs "[Emergence of Locomotion Behaviours in Rich Environments](https://www.youtube.com/watch?v=hx_bgoTF7bs)"

{% youtube "https://www.youtube.com/watch?v=hx_bgoTF7bs" %}

#### Square-cube business

Materials and size scales and the inappropriateness of humanoid design for your _giiiiiiiiiiant_ robots. The square cube law. Don't forget [Godzilla]({% link decipherscifi/_posts/2016-07-19-godzilla-miles-greb-movie-podcast-episode-47.md %}) and [King Kong]({% link decipherscifi/_posts/2017-07-25-kong-skull-island-episode-100.md %})!

#### Kaiju

Neural loads and signal speed. Central pattern generators and "butt brains" and how the dinosaurs didn't have them. Blue ammonia heavy metal blood. Atmospheric pressures.

#### Jägers

Metallic construction. Irrational fear of alloys. Possible material engineering choices. Beryllium is _the worst_.  Graphene and spraying pencils. Incentive systems for robot design. Square cube again. Density.

#### Power sources

The subtlety of nuclear reactors vs nuclear bombs. Orion nuclear blast spacecraft. Carl Sagan. Energy density of fuel sources. Nuclear testing for fun and profit. Playing science fast and loose.

{% youtube "https://www.youtube.com/watch?v=8pkivjHnD_s" %}

#### Alien terraforming projects

Playing the looooooooong game. Statistical [Great Filter](https://en.wikipedia.org/wiki/Great_Filter) analyses. Earth is a fixer-upper. Jerk bully aliens.

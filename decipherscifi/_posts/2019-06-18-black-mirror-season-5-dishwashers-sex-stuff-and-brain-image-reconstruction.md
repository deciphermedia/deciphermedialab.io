---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - tv
date: 2019-06-18 07:45:05+00:00
description: 'Fun new frontiers for human sexuality. Polar bears. Gaming. Body bacteria. Posthumous virtual performances, vocal generation, and mocap. And dishwashers!'
layout: podcast_post
permalink: /decipherscifi/black-mirror-season-5-dishwashers-sex-stuff-and-brain-image-reconstruction
redirect_from:
  - /decipherscifi/198
slug: black-mirror-season-5-dishwashers-sex-stuff-and-brain-image-reconstruction
title: 'Black Mirror Season 5: dishwashers, sex stuff, and brain image reconstruction'
tags:
  - artificial intelligence
  - gaming
  - image reconstruction
  - machine learning
  - microbiomes
  - polar bears
  - science
  - science fiction
  - sexuality
  - virtual performers

# Episode particulars
image:
  path: assets/imgs/media/tv/black_mirror_2011_season_5.jpg
  alt: 'Black Mirror Season 5 collage'
links:
  - text: 'Head Like a Hole by Devo'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=LhL8Sl3dWl8'
  - text: 'Human brain mapping and brain decoding by Jack Gallant'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=Ecvv-EvOj8M'
  - text: 'Deep image reconstruction from human brain activity'
    urls:
      - text: PLOS
        url: 'https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1006633'
media:
  links:
    - text: Netflix
      url: 'https://www.netflix.com/title/70264888'
  title: Black Mirror
  type: tv
  year: 2011
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/tv/black_mirror_2011_season_5.jpg
  number: 198
  guid: 'https://decipherscifi.com/?p=10436'
  media:
    audio:
      audio/mpeg:
        content_length: 31766720
        duration: '37:48'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/black_mirror_season_5_--_--_decipher_scifi.mp3'

---
# Striking Vipers

#### Human sexuality

The bright and exciting future of who does what with whom and how. _Polar bears_. How we might have handles this VR technology as teenagers.

{% responsive_image_block %}
  path: assets/imgs/selfies/the_golden_compass.jpg
  alt: Colbert is the bear
  caption: Colbert is the bear
{% endresponsive_image_block %}

#### Gaming

Nostalgia for days-long gaming binges with Doritos and Easy Cheese. Ubiquitous full-spectrum VR experience. Neural mapping. Popular physics plugins , game engines, and the possibility that McCallister and Striking Vipers etc run on the same software.

#### Bodily bacteria

Dirt subscription boxes. Maintaining your skin microbiome. Showring? Soap vs detergent. Probiotic yogurt: "one for me, one for the undercarriage."

#### Dishwashers

Knife-down safety is actually important(!). Dish detergents and plate dirtiness. Clever dirtiness-measurement technologies.

# Smithereens

#### Driver Rating

"2 stars, car was clean"

# Rachel, Jack, and Ashley Too

#### Virtual performance

Virtualizing irl performers. [Pepper's Ghost](https://en.wikipedia.org/wiki/Pepper%27s_ghost). Lifelike real-time rendering technology. [Real-time raytracing.](https://www.youtube.com/watch?v=G9PmuDtwC-I) Motion capture cost and complexity drops.

{% youtube "https://www.youtube.com/watch?v=G9PmuDtwC-I" %}

{% youtube "https://www.youtube.com/watch?v=KJRZTkttgLw" %}

#### Brains etc

Machine learning vocal production. FMRI vs EEG vs whatever awesome thing they have in Black Mirror. [Deep image reconstruction from human brain activity](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1006633).

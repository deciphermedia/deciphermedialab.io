---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - tv
date: 2018-01-30 07:45:16+00:00
description: 'Philip K Dick. Electric Dreams, consciousness, artificial intelligence, Nick Bostrom, instrumental convergence. Apathetic skynet. Gaslighting. Invasion of the Body Snatchers, Stranger Things, and Home Alone booby-trap techniques. Parasitic consciousnesses and travel by space rock.'
layout: podcast_post
permalink: /decipherscifi/philip-k-dicks-electric-dreams-episode-127
redirect_from:
  - /decipherscifi/127
slug: philip-k-dicks-electric-dreams-episode-127
title: "Philip K Dick's Electric Dreams: self-replicating AI, the illusion of self, and parasites"
tags:
  - ai takeoff
  - artificial intelligence
  - parasites
  - philip k dick
  - science
  - science fiction
  - virtual reality

# Episode particulars
image:
  path: assets/imgs/media/tv/philip_k_dicks_electric_dreams_2017.jpg
  alt: "Philip K Dick's Electric Dreams backdrop collage"
links:
  - text: Universal Paperclips
    urls:
      - text: DecisionProblem.com
        url: 'http://www.decisionproblem.com/paperclips/index2.html'
  - text: 'Blade Runner: "androids," the AI control problem, and brain design'
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/105'
  - text: 'A Scanner Darkly: the surveillance state, drug abuse, and rotoscoping'
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/8'
  - text: 'Minority Report: Eliot Peper on freedom and algorithmic precognition'
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/65'
media:
  links:
    - text: Amazon
      url: 'https://www.amazon.com/Real-Life/dp/B075NV2XPJ'
  title: "Philip K Dick's Electric Dreams"
  type: tv
  year: 2017
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/tv/philip_k_dicks_electric_dreams_2017.jpg
  number: 127
  guid: 'https://decipherscifi.com/?p=5067'
  media:
    audio:
      audio/mpeg:
        content_length: 33810432
        duration: '40:14'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Philip_K_Dicks_Electric_Dreams_--_--_decipherSciFi.mp3'
---
#### Philip K Dick

Callback to [some]({% link decipherscifi/_posts/2017-08-29-blade-runner-episode-105.md %}).[of]({% link decipherscifi/_posts/2015-10-20-a-scanner-darkly-podcast-episode-008.md %}).[our]({% link decipherscifi/_posts/2016-11-22-minority-report-eliot-peper-author-episode-65.md %}).[episodes]({% link decipherscifi/_posts/2016-01-12-philip-k-dick-review-movie-podcast-episode-20.md %}) on PKD and his stories. Pulp scifi of the 50s and 60s. General appreciation for his ideas and the return of thoughtful anthologies.

#### Autofac

PKDpocalypse. Consumerism something something. Supreme dedication to a task. [The Paperclip Maximizer](https://en.wikipedia.org/wiki/Instrumental_convergence#Paperclip_maximizer) and the possible danger of a highly-capable AI dedicated to its task. [Universal Paperclips](http://www.decisionproblem.com/paperclips/index2.html). The unconscious, apathetic skynet.

#### Real Life

PKD was all about that hash tag life. The malleability of our perception of "reality." Am I Anna Paquin? Neurological explanations for Deja Vu. Gaslighting. Dick moves. Colbert's joke-telling disability.

> Convincing your girlfriend she's crazy or paranoid is called gaslighting, and it's a dick move.
> But convincing her she's a robot with artificial implanted human emotions is called bladerunning. It's a Phillip K. Dick move.
>
> [Nathan Anderson](https://twitter.com/nathanthesnake?lang=en)

#### The Father Thing

Invasion of the Body Snatchers and Stranger Things. Really really affordable spacecraft for extremophiles. Parasites cleaning up after themselves poorly. Sometimes you just gotta step up and try to murder someone. Home alone booby-trap techniques, poorly applied. Compressed parasite operating systems.

---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2016-09-13 05:00:24+00:00
description: 'Anime is on the podcast once more with Cowboy Bebop - space westerns, Firefly, space settlement, martian terraforming, biodomes, weather control, more'
layout: podcast_post
permalink: /decipherscifi/cowboy-bebop-movie-podcast-episode-55
redirect_from:
  - /decipherscifi/55
slug: cowboy-bebop-movie-podcast-episode-55
title: 'Cowboy Bebop: biodomes, scifi westerns, and weather control'
tags:
  - science fiction
  - science
  - anime
  - animation
  - comics
  - future
  - mars
  - space western
  - space settlement

# Episode particulars
image:
  path: assets/imgs/media/movies/cowboy_bebop_the_movie_2001.jpg
  alt: The Bebop crew assembled on a sidewalk
links:
  - text: Cowboy Bebop the Series
    urls:
      - text: Funimation
        url: 'https://www.funimation.com/shows/cowboy-bebop/?qid=a62b0be759c253c2'
  - text: Voices from L5
    urls:
      - text: Patreon
        url: 'https://www.patreon.com/VoicesFromL5'
media:
  links:
    - text: Amazon
      url: 'https://www.amazon.com/Cowboy-Bebop-Movie-David-Lucas/dp/B01JQ160O8'
  title: Cowboy Bebop The Movie
  type: movie
  year: 2001
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/cowboy_bebop_the_movie_2001.jpg
  number: 55
  guid: 'http://www.decipherscifi.com/?p=1012'
  media:
    audio:
      audio/mpeg:
        content_length: 23506772
        duration: '39:10'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Cowboy_Bebop_decipherSciFi.mp3'
---

{% responsive_image path: assets/imgs/selfies/cowboy_bebop.jpg template: _includes/image_inline.html alt: "Decipher SciFi hosts Christopher Peterson and Lee Colbert in the Cowboy Bebop lineup" %}

#### Meta

Cowboy Bebop the series, Yoko Kanno, style points.

#### Space Western

Shades of Firefly.

#### Gate Disaster

Shades of Seveneves and Deep Impact. Meteor impacts are becoming a recurring theme!

#### Settling Mars

Biodomes, [marscrete](https://arxiv.org/abs/1512.05461), martian soil, regolith, constructive impacts. Terraforming. Biodomes.

#### Weather Control

China! Weather control before and after the Beijing Olympics. Cold and warm cloud seeding. Chemicals. Super-frozen, uncrystallized. water.

---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2020-01-21 07:45:29+00:00
description: 'The simulatability of reality. Defining consciousness. Universal computational capacity.  The (low) odds of finding yourself in base reality. Etc.'
layout: podcast_post
permalink: /decipherscifi/the-thirteenth-floor-yo-dawg-i-heard-you-liked-the-simulation-hypothesis
redirect_from:
  - /decipherscifi/225
slug: the-thirteenth-floor-yo-dawg-i-heard-you-liked-the-simulation-hypothesis
title: 'The Thirteenth Floor: Yo dawg I heard you liked the simulation hypothesis'
tags:
  - science fiction
  - science
  - virtual reality
  - simulation hypothesis
  - simulation argument

# Episode particulars
image:
  path: assets/imgs/media/movies/the_thirteenth_floor_1999.jpg
  alt: "A man standing at the boundary between reality and the virtual and it's a really cool image hnonestly"
links:
  - text: 'Inception'
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/movie/inception/id400763833?mt=6&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Inception-Leonardo-DiCaprio/dp/B0047WJ11G'
  - text: 'The Matrix'
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/movie/the-matrix/id271469518?mt=6&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Matrix-Keanu-Reeves/dp/B001XVD2Z0'
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=qEXv-rVWAu8'
  - text: 'The Matrix - green phosphors, neuroplasticity, and rejecting utopia w/ Adrian Falcone'
    urls:
      - text: 'Decipher SciFi'
        url: 'https://decipherscifi.com/186'
  - text: 'The Truman Show - objects visible from space, dome megastructues, and reality testing'
    urls:
      - text: 'Decipher Scifi'
        url: 'https://decipherscifi.com/103'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/the-thirteenth-floor/id284571749?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Inception-Leonardo-DiCaprio/dp/B0047WJ11G'
  title: 'The Thirteenth Floor'
  type: movie
  year: 1999
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/the_thirteenth_floor_1999.jpg
  number: 225
  guid: 'https://decipherscifi.com/?p=10909'
  media:
    audio:
      audio/mpeg:
        content_length: 38301343
        duration: '45:29'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/the_thirteenth_floor.output.mp3'

---
#### This movie

Yo dog, I heard you liked simulations. Server-room LAN rave parties. choosing your simulated period for fun, profit, and lack of complications.

#### Simulation

Plato’s shadows on a cave wall. Maybe shadows all the way down, and turtles all the way up! The simulatability of our perceived reality.

#### The argument

[The simulation argument](https://www.simulation-argument.com/) made distinct from the simulation hypothesis. Considering possible “great filters” that could prevent technological progress reaching the point where a reality simulation is possible.

#### Difficulties

The possible impossibility of simulating a whole universe in what may itself be a finite universe. Video game engine-corner cutting analogies for the reality sim. [Kardashev scales](https://en.wikipedia.org/wiki/Kardashev_scale) and simulation ability.

#### Reality

But is it “real” and does it even matter? “I think therefore I am.” Mind uploading and downloading and VR “death.

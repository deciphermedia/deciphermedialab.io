---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2019-03-19 07:45:20+00:00
description: 'The Atlantis myth. Human civilization 10k years ago. Imagining the detectability of modern civilization at geologic time scales. Extrapolating into space. Etc.'
layout: podcast_post
permalink: /decipherscifi/atlantis-the-silurian-hypothesis-ancient-language-and-updating-the-drake-equation
redirect_from:
  - /decipherscifi/185
slug: atlantis-the-silurian-hypothesis-ancient-language-and-updating-the-drake-equation
title: 'Atlantis: the Silurian hypothesis, ancient language, and updating the drake equation'
tags:
  - civilization
  - conlang
  - language
  - linguistic
  - myth
  - proto-indo-european
  - science
  - science fiction
  - silurian hypothesis

# Episode particulars
image:
  path: assets/imgs/media/movies/atlantis_2001.jpg
  alt: 'A people-pile and a weird submarine Atlantis movie backdrop'
links:
  - text: 'The Silurian Hypothesis: Would it be possible to detect an industrial civilization in the geological record?'
    urls:
      - text: 'arXiv.org'
        url: 'https://arxiv.org/abs/1804.03748'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/atlantis-the-lost-empire/id351757020?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Atlantis-Empire-Michael-J-Fox/dp/B003QSO05E'
  title: Atlantis
  type: movie
  year: 2001
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/atlantis_2001.jpg
  number: 185
  guid: 'https://decipherscifi.com/?p=10337'
  media:
    audio:
      audio/mpeg:
        content_length: 28313211
        duration: '33:41'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Atlantis__--_--_Decipher_SciFi.mp3'

---

#### Myth and history

Getting past the nonsense. The reasonable idea that civilizations have in fact existed and then fallen, without notice by modern study. Rocking Like a Hurricane in the B.C. era.

#### Human history irl

Humanity circa 10,000 years ago. The beginnings of civilization. Early agriculture. Making beer and eventually bread. How not to make bread by putting beer in the oven. More Rocking Like a Hurricane. Just invent pants, beer, and bread and you're off to a really solid start. "[Why are pants,](https://www.nbc.com/saturday-night-live/video/outrageous-clown-squad-kickspit-dirt-festival/n12792)[ different than shirts?](https://vimeo.com/20002765)"

#### The Silurian Hypothesis

[The Silurian Hypothesis](https://en.wikipedia.org/wiki/Silurian_hypothesis). Imagining the difference in civilizational detectibility over hundreds of thousands or millions of years. Thinking like ze dolphin. Plastic permanence. Climate impacts written into geology. Radiological evidence.

#### Proto Indo European

The ["Atlantean" conlang](https://en.wikipedia.org/wiki/Atlantean_language). Marc  Okrand and Klingon. Language before language was written down. How we figure out _anything_ about a language from 6500 years ago that is no longer spoken and was never written down.

#### Links

Atlantis: [iTunes](https://geo.itunes.apple.com/us/movie/atlantis-the-lost-empire/id351757020?mt=6&at=1001l7hP) | [Amazon Prime Video](https://www.amazon.com/Atlantis-Empire-Michael-J-Fox/dp/B003QSO05E/ref=sr_1_3?keywords=atlantis&qid=1552936093&s=gateway&sr=8-3)
The Silurian Hypothesis: Would it be possible to detect an industrial civilization in the geological record?: [arXiv.org](https://arxiv.org/abs/1804.03748)

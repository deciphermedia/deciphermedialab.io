---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2017-10-24 07:45:48+00:00
description: 'Event Horizon! space science, settling Mars vs the moon, Lovecraft and cosmicism, wormholes and spacetime, deep time, hell as a mirror, more!'
layout: podcast_post
permalink: /decipherscifi/event-horizon-feat-caelum-rale-episode-113
redirect_from:
  - /decipherscifi/113
slug: event-horizon-feat-caelum-rale-episode-113
title: 'Event Horizon: cosmic deep time,lived-in space ships, and real hell w/ Caelum Rale'
tags:
  - black holes
  - deep time
  - horror
  - lovecraft
  - lovecraftian horror
  - science
  - science fiction
  - space
  - space settlement

# Episode particulars
image:
  path: assets/imgs/media/movies/event_horizon_1997.jpg
  alt: A scary looking space station before a planet
links:
  - text: Caelum Rale
    urls:
      - text: Instagram
        url: 'https://www.instagram.com/inclosedspaces/'
  - text: The public-domain works of H.P. Lovecraft
    urls:
      - text: Archive.org
        url: 'https://archive.org/details/TheCollectedWorksOfH.p.Lovecraft'
      - text: Amazon
        url: 'https://www.amazon.com/Complete-Fiction-H-P-Lovecraft-ebook/dp/B076GLJKDL'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/event-horizon/id306163996?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Event-Horizon-Laurence-Fishburne/dp/B007QIDH9A'
  title: Event Horizon
  type: movie
  year: 1997
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: caelum_rale
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/event_horizon_1997.jpg
  number: 113
  guid: 'https://decipherscifi.com/?p=3840'
  media:
    audio:
      audio/mpeg:
        content_length: 50593792
        duration: '01:00:13'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Event_Horizon_--_Caelum_Rale_--_decipherSciFi.mp3'
---
#### Settling nearby space

Mars vs the moon. Inconvenient solar exposure schedules breeding technological food solutions. Substitute engineered fungi for photosynthetic plants as a food source.

#### Exposition

Vs "show don't tell." Australian flags and separation from the United Kingdom.

#### Space ships

Lived-in ships and how they were exemplified in Alien or Star Wars. The "traditional" design of the Lewis & Clark, basically maintaining the naval analogy in space travel. BUT! The Event Horizon is much different.

#### The Event Horizon

How the Event Horizon is the _opposite_ of Joseph Campbell's "belly of the whale." [Radula](https://en.wikipedia.org/wiki/Radula) hallways and OSHA. Gothic architecture and religious imagery in interior and exterior design.

#### FTL

The Event Horizon and its gravity drive something-or-other. Spacetime-folding visual analogies. Using contained black holes to create wormholes. The Monty Hall problem, but with Hell instead of a prize.

#### The Solaris Effect

Completely foreign consciousnesses probing for something to communicate. Accidentally poking at raw human neuroses like a toothache. Other forms of consciousness, where consciousness is a form of information processing.

#### Hell

Is this Hell a place? Or a manifestation of our neuroses? Or a greater consciousness just rolling over on us by accident?

#### Lovecraft

[Cosmicism](https://en.wikipedia.org/wiki/Cosmicism). Humans facing life from [deep time](https://en.wikipedia.org/wiki/Deep_time). Human-scale irrelevance.

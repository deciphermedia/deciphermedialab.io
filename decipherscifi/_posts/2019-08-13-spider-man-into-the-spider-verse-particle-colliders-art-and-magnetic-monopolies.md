---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2019-08-13 07:45:50+00:00
description: 'Particle collider scales and purposes. The future of physics knowledge. The (not really) dangers of colliders, and OMG BEAUTIFUL art. Favorite Spider-Mans.'
layout: podcast_post
permalink: /decipherscifi/spider-man-into-the-spider-verse-particle-colliders-art-and-magnetic-monopolies
redirect_from:
  - /decipherscifi/206
slug: spider-man-into-the-spider-verse-particle-colliders-art-and-magnetic-monopolies
title: 'Spider-Man Into the Spider-Verse: particle colliders, art, and magnetic monopolies w/ Adrian Falcone'
tags:
  - art
  - comics
  - magnets
  - multiverse
  - particle colliders
  - quantum physics
  - science
  - science fiction
  - time travel

# Episode particulars
image:
  path: assets/imgs/media/movies/spiderman_into_the_spiderverse_2018.jpg
  alt: 'Miles Morales jumping out of a brilliant pop-art background'
links:
  - text: 'Exhalation by Ted Chiang'
    urls:
      - text: iTunes
        url: 'https://books.apple.com/us/book/exhalation/id1425691448?mt=11&app=itunes&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Exhalation-Stories-Ted-Chiang/dp/1101947888'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/spider-man-into-the-spider-verse/id1444695454?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Spider-Man-Into-Spider-Verse-Liev-Schreiber/dp/B07L9YXWSW'
  title: 'Spider-Man: Into the Spider-Verse'
  type: movie
  year: 2018
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: adrian_falcone
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/spiderman_into_the_spiderverse_2018.jpg
  number: 206
  guid: 'https://decipherscifi.com/?p=10735'
  media:
    audio:
      audio/mpeg:
        content_length: 31755067
        duration: '37:42'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/spiderman_into_the_spiderverse.mp3'

---
#### Colliders

Particle collider scales. Increasing energies. Finding the Higgs Boson. Different ways of smashing things. Straight colliders, ring colliders, and rings so large they kinda seem straight at a certain scale. Adrian's new area of expertise. "Natural experiments" in particle physics. Imagining future colliders and subsequent discoveries. Confirmation of supersymmetry. Probing the edges of the standard model.

#### Collider danger

Stragelets and black holes and other things that won't happen. [Anatoli Burgorski](https://en.wikipedia.org/wiki/Anatoli_Bugorski) taking a load in the eye. Magnetic monopoles and winning "Magnetic Monopoly."

#### Art!

OMG the art. Chromatic aberration. Offset printing errors. Smearing vs motion blur. Animating on "twos" or "threes" and the use of fluidity as a storytelling tool.

#### Spider-ones

Favorite Spiderman movie?

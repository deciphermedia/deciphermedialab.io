---
layout: person_page
person_data:
  name: ''
  data_name: ''
  bio: ''
  image_path: assets/imgs/people/
  title: ''
  websites:
    - name: 
      url: ''
  social:
    - name: twitter
      url: 
  roles:
    - show: decipherscifi
      role: guest_cohost
    - show: decipherhistory
      role: guest_cohost

---

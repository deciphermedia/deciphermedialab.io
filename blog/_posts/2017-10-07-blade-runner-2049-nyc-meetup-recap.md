---
# General post frontmatter
author: christopher_peterson
categories:
  - irl
date: 2017-10-07 14:01:07+00:00 
layout: post
slug: blade-runner-2049-nyc-meetup-recap
title: 'Blade Runner 2049 NYC Meetup Recap!'
image:
  path: assets/imgs/misc/meetup_blade_runner_2049.jpg
  alt: 'Blade Runner 2049 NYC meetup Decipher SciFi group selfie'

---

Thanks for coming out folks! [We saw Blade Runner with you]({% link blog/_posts/2017-10-05-blade-runner-2049-nyc-meetup.md %}) and we love seeing you irl and I love Blade Runner and life and you're awesome

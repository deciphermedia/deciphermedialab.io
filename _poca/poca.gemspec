# coding: utf-8

Gem::Specification.new do |spec|
  spec.name                    = "poca"
  spec.version                 = "0.1.0"
  spec.authors                 = ["Antonio Trento", "Decipher Media"]

  spec.summary                 = %q{Poca is a fantastic, mobile-ready and responsive free podcasters website template with a nice selection of features and assets.}
  spec.homepage                = "https://colorlib.com/wp/template/poca/"
  spec.licenses                = ["CC-BY 3.0"]

  spec.metadata["plugin_type"] = "theme"

  spec.files                   = `git ls-files -z`.split("\x0").select do |f|
    f.match(%r{^(assets|_(includes|layouts|posts)/|(LICENSE|README)((\.(txt|md|markdown)|$)))}i)
  end

  spec.add_runtime_dependency "jekyll", "~> 4.0"

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 12.0"
end

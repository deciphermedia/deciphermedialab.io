---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - tv
date: 2019-05-14 07:45:39+00:00
description: 'A review as we zero in on the finale. Ballistae and accuracy. Irregular seasons, axial precession, elliptical orbits, and biological climate movers. Etc.'
layout: podcast_post
permalink: /decipherscifi/game-of-thrones-medieval-weaponry-variable-seasons-and-final-season-anxiety
redirect_from:
  - /decipherscifi/193
slug: game-of-thrones-medieval-weaponry-variable-seasons-and-final-season-anxiety
title: 'Game of Thrones: medieval weaponry, variable winters, and final-season anxiety'
tags:
  - ancient rome
  - dark ages
  - dragons
  - fantasy
  - history
  - middle ages
  - science
  - science fiction
  - weapons technology

# Episode particulars
image:
  path: assets/imgs/media/tv/game_of_thrones_2011.jpg
  alt: 'Eddard Stark sitting solemn on the Iron throne'
links:
  - text: "Game of Fuckin' Thrones by Anamanaguchi"
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=25KABvPbq-U'
  - text: 'Nightflyers (SciFi!) & Other Stories by GRRM'
    urls:
      - text: iTunes
        url: 'https://books.apple.com/us/book/nightflyers-other-stories/id1326883290?mt=11&app=itunes&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Nightflyers-Illustrated-George-R-Martin/dp/0525619682'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/tv-season/game-of-thrones-the-complete-series/id1458816208?mt=4&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Winter-Is-Coming/dp/B007BVOEPI'
  title: Game of Thrones
  type: tv
  year: 2011
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/game_of_thrones_2011.jpg
  number: 193
  guid: 'https://decipherscifi.com/?p=10404'
  media:
    audio:
      audio/mpeg:
        content_length: 28319107
        duration: '33:42'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/game_of_thrones_--_--_decipher_scifi.mp3'

---
In which we discuss Game of Thrones right before the finale

#### The books

Appreciating GRRM. And appreciating him double and triple as the show goes on.

#### The setting

The middle ages. The "dark ages" and how much is that a misnomer? Magic and dragons stagnating technological progress. Volcanoes beat dragons in rock-paper-scissors. Air force exclusivity. Ancient artillery. Ballistae and Scorpions and the peak of human weaponry until the weaponization of gunpowder.

#### Winter is coming

Figuring out ways to make the seasons less predictable. Unsatisfying suggestions in axial precession and funny orbits. The possibility of the interplay of biology with climatological processes.

#### Disappointment and Predictons

Trying to understand, as we reach the conclusion, why the show feels so unsatisfying now. Why every turn of events feels so unearned. Predicting who will get the throne as of "[The Last of the Starks](https://en.wikipedia.org/wiki/The_Last_of_the_Starks)."

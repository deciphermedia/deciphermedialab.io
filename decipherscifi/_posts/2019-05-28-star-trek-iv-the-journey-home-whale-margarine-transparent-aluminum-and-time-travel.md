---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2019-05-28 07:45:50+00:00
description: 'Time travel anachronisms. The Mooreeffoc Effect. Unconsidered cosmic horror. Whaling history. Whale oil factory ships. Napoleon and whale margarine. Etc.'
layout: podcast_post
permalink: /decipherscifi/star-trek-iv-the-journey-home-whale-margarine-transparent-aluminum-and-time-travel
redirect_from:
  - /decipherscifi/195
slug: star-trek-iv-the-journey-home-whale-margarine-transparent-aluminum-and-time-travel
title: 'Star Trek IV - The Journey Home: whale margarine, transparent aluminum, and time travel'
tags:
  - cosmic horror
  - evolution
  - science
  - science fiction
  - sea mammals
  - star trek
  - time travel

# Episode particulars
image:
  path: assets/imgs/media/movies/star_trek_iv_1986.jpg
  alt: 'Kirk and Spock in the 80s'
links:
  - text: Whaling
    urls:
      - text: Wikipedia 
        url: 'https://en.wikipedia.org/wiki/Whaling'
  - text: Whale Oil
    urls:
      - text: Wikipedia 
        url: 'https://en.wikipedia.org/wiki/Whale_oil'
  - text: Margarine
    urls:
      - text: Wikipedia
        url: 'https://en.wikipedia.org/wiki/Margarine'
  - text: 'Moby Dick by Herman Melville'
    urls:
      - text: iTunes
        url: 'https://books.apple.com/us/book/moby-dick/id427802472?mt=11&app=itunes&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Moby-Wordsworth-Classics-Herman-Melville/dp/1853260088'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/star-trek-iv-the-voyage-home/id208978062?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Star-Trek-IV-Voyage-Home/dp/B000HZGNEU'
  title: Star Trek IV
  type: movie
  year: 1986
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/star_trek_iv_1986.jpg
  number: 195
  guid: 'https://decipherscifi.com/?p=10415'
  media:
    audio:
      audio/mpeg:
        content_length: 39915210
        duration: '47:30'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/star_trek_iv_--_--_decipher_scifi.mp3'

---
#### Star Trek

Best Star Trek movies. Liking things because they are familiar. The odd-even Star Trek movie rule. Mathematical proofs. Dismissing the reboots.

#### Cosmic horror

Giant cylinders and plumbuses. Finding out you are as an ant in the universe. Spreading across space and building giant robot suits to fight the larger incomprehensible creatures.

#### Time travel

Using time travel to solve problems despite the consequences. Time travel anachronism stories. Bad time-travel sales pitches. The [Mooreeffoc Effect](https://en.wikipedia.org/wiki/Mooreeffoc). Would _you_ go to the future? Only going forward because medicine in the past is always so bad. Time travel vaccinations.

#### Whales

The possibility of cetacean "language." "Save the whales" in the 80s. Whale oil being supplanted by kerosene as industrial fuel. Stinky whale candles. The Napoleonic origins of margarine and the economics of fuel and margarine. Industrialized whaling. "Factory ships." Realizing we are running out of whales. Continued industrial whaling, namely by Japan and some of the Nordic countries.
